using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Sqo;

namespace SiaqodbDemo
{
    public class DatabaseFactory
    {
        public static string siaoqodbPath;
        private static Siaqodb instance;


        public static Siaqodb GetInstance()
        {
            if (instance == null)
            {
               //put here your License Key
				SiaqodbConfigurator.SetLicense(@"awRH8HJyTnRgus6I4sKUprqfuUta9E3e7BLTEavVnNJR8x/bg3EHrPeShTN0KxH1H4WY0TrY3bPhYq+AwJ98SQ==");

				//if ANDROID:
				if (Application.platform == RuntimePlatform.Android) {
					siaoqodbPath = Application.persistentDataPath + Path.DirectorySeparatorChar + @"database";
					Debug.Log ("Android Path " + siaoqodbPath);
				}
				//if Windows or MAC
				else if (Application.platform == RuntimePlatform.OSXEditor || 
				         Application.platform == RuntimePlatform.WindowsEditor) {
					siaoqodbPath = Environment.CurrentDirectory + Path.DirectorySeparatorChar + @"database";
					Debug.Log ("Editor Path " + siaoqodbPath);
				}
				
				//if iOS (iPhone /iPad)
				else if (Application.platform == RuntimePlatform.OSXPlayer ||
				         Application.platform == RuntimePlatform.IPhonePlayer) {
					siaoqodbPath = Application.persistentDataPath + Path.DirectorySeparatorChar + @"database";
					//siaoqodbPath = Application.dataPath.Replace("Data", "Documents");
					Debug.Log ("IPad Path " + siaoqodbPath);
				}

				if (!Directory.Exists(siaoqodbPath))
                {
                    Directory.CreateDirectory(siaoqodbPath);
                }
                instance = new Siaqodb(siaoqodbPath);
            }

            return instance;
        }
        #if UNITY_EDITOR
		[UnityEditor.MenuItem ("FarofaStudios/Delete Project Database", false, 50)]		
		#endif
		public static void DeleteDataBase () {
			instance = GetInstance ();
			Debug.Log ("Deleted");
			if (Directory.Exists(siaoqodbPath)) {
				DirectoryInfo directory = new DirectoryInfo (siaoqodbPath);
                CloseDatabase();
				foreach(System.IO.FileInfo file in directory.GetFiles()) 
					file.Delete();
				foreach(System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) 
					subDirectory.Delete(true);
			}
			CloseDatabase ();
		}

		public static void OpenDatabase () {
			if (instance == null) {
				instance = GetInstance ();
			}
			instance.Open (siaoqodbPath);
		}

        public static void CloseDatabase()
        {
            if (instance != null)
            {
                instance.Close();
                instance = null;
            }
        }
    }
}
