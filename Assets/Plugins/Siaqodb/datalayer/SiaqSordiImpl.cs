﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using Sqo;

namespace Sqo.Attributes {
	public static class SiaqSordiImpl {
		public static void ValidTransaction (object entity) {
			ValidMaxLength (entity);
			ValidNullable (entity);
		}

		private static void ValidNullable (object entity) {
			foreach (PropertyInfo pi in entity.GetType ().GetProperties ()) {
				bool isNullable = false;
				foreach (object attr in pi.GetCustomAttributes (true))
					if (attr is Nullable)
						isNullable = true;

				if (!isNullable) {
					object value = pi.GetValue (entity, null);
					
					if (value == null) {
						throw new Exception ("Sqo Exception - Property " + pi.Name + " value is null at " + entity.GetType () + " object!");
					}
				}
			}
		}

		private static void ValidMaxLength (object entity) {
			foreach (PropertyInfo pi in entity.GetType ().GetProperties ()) {
				foreach (object attr in pi.GetCustomAttributes (true)) {
					if (attr is MaxLengthAttribute) {
						MaxLengthAttribute maxLength = attr as MaxLengthAttribute;
						object value = pi.GetValue (entity, null);
						if (value == null)
							continue;

						if (!(value is string)) {
							throw new Exception ("Sqo Exception - Property " + pi.Name + " is not a string and it cannot be declared MaxLengthAttribute at " + entity.GetType () + "object!");
						}

						string stringValue = value as string;
						if (stringValue.Length > maxLength.maxLength) {
							throw new Exception ("Sqo Exception - Property " + pi.Name + " exceed the max length at " + entity.GetType () + " object!");
						}
					}
				}
			}
		}
	}

	public class Nullable : System.Attribute{}
}
