﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class VistoriaDAO : GenericDAO<Vistoria, int> {
	public List<Vistoria> LoadByVistoriador(Vistoriador vistoriador)
	{
		List<Vistoria> result = null;
		
		Func <Vistoria, bool> predicate = d => d.vistoriador != null ? d.vistoriador.OID == vistoriador.OID : false;
		result = LoadByCriteria (predicate);
		
		return result;
	}

	/*public void Synchronize () {
		VistoriaDAO vistoriaDAO = new VistoriaDAO();
		VistoriaWS.VistoriaWSService vistoriaWS = new VistoriaWS.VistoriaWSService ();
		List<Vistoria> vistorias = vistoriaDAO.LoadAllIncludingDeleted ();
		
		foreach (Vistoria vistoria in vistorias) {
			//Delete
			if (vistoria != null && vistoria.IsDeleted && vistoria.id != 0) {
				vistoriaWS.delete (vistoria.GetJson ());
				continue;
			}
			
			//Save
			if (vistoria != null && vistoria.id == 0 && !vistoria.IsDeleted) {
				int id = 0;
				string json = vistoria.GetJson ();
				string result = vistoriaWS.save (json);
				if (int.TryParse (result, out id)) 
					vistoria.id = id;
				
				vistoriaDAO.UpdateWithoutSetAsUpdated (vistoria);
				continue;
			}
			
			//Update
			if (vistoria != null && vistoria.IsUpdated) {
				vistoriaWS.update (vistoria.GetJson ());
				vistoriaDAO.UpdateWithoutSetAsUpdated (vistoria);
				continue;
			}
			
		}
	}*/
}
