﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class ServicoDAO : GenericDAO<Servico, int> {

	public List<Servico> LoadByAmbiente(Ambiente ambiente)
	{
		List<Servico> result = null;
		
		Func <Servico, bool> predicate = d => (d.ambiente != null) ? d.ambiente.OID == ambiente.OID : false;
		result = LoadByCriteria (predicate);
		
		return result;
	}

}
