﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class AmbienteDAO : GenericDAO<Ambiente, int> {

	public List<Ambiente> LoadByVistoriaAndAmbientName(Vistoria vistoria, string ambName)
	{
		List<Ambiente> result = null;
		
		Func <Ambiente, bool> predicate = d => (d.vistoria != null) ? d.vistoria.OID == vistoria.OID && d.nome == ambName : false;
		result = LoadByCriteria (predicate);
		
		return result;
	}
}
