﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class VistoriadorDAO : GenericDAO<Vistoriador, int>
{
	public Vistoriador login (string user, string pass)
	{
		Vistoriador result = null;

		Func <Vistoriador, bool> predicate = v => v.usuario == user && v.senha == pass;
		List<Vistoriador> list = LoadByCriteria (predicate);

		if (list != null && list.Count > 0) {
			result = list [0];
		}

		return result;
	}

	/*public void Synchronize ()
	{
		VistoriadorDAO vistoriadorDAO = new VistoriadorDAO ();
		VistoriadorWS.VistoriadorWSService vistoriadorWS = new VistoriadorWS.VistoriadorWSService ();
		List<Vistoriador> vistoriadors = vistoriadorDAO.LoadAllIncludingDeleted ();
		
		foreach (Vistoriador vistoriador in vistoriadors) {
			//Delete
			if (vistoriador != null && vistoriador.IsDeleted && vistoriador.id != 0) {
				vistoriadorWS.delete (vistoriador.GetJson ());
				continue;
			}
			
			//Save
			if (vistoriador != null && vistoriador.id == 0 && !vistoriador.IsDeleted) {
				int id = 0;
				string json = vistoriador.GetJson ();
				string result = vistoriadorWS.save (json);
				if (int.TryParse (result, out id))
					vistoriador.id = id;
				
				vistoriadorDAO.UpdateWithoutSetAsUpdated (vistoriador);
				continue;
			}
			
			//Update
			if (vistoriador != null && vistoriador.IsUpdated) {
				vistoriadorWS.update (vistoriador.GetJson ());
				vistoriadorDAO.UpdateWithoutSetAsUpdated (vistoriador);
				continue;
			}
			
		}
	}*/
}
