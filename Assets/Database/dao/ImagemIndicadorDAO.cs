﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ImagemIndicadorDAO : GenericDAO<ImagemIndicador, int> {
	public List<ImagemIndicador> LoadByVistoria(Vistoria vistoria)
	{
		List<ImagemIndicador> result = null;
		
		Func <ImagemIndicador, bool> predicate = d => (d.indicador != null && 
		                                               d.indicador.servico != null && 
		                                               d.indicador.servico.ambiente != null &&
		                                               d.indicador.servico.ambiente.vistoria != null) ? d.indicador.servico.ambiente.vistoria.OID == vistoria.OID : false;
		result = LoadByCriteria (predicate);
		
		return result;
	}

	public List<ImagemIndicador> LoadByVistoriaAmbienteServico(Vistoria vistoria, string ambiente, string servico)
	{
		List<ImagemIndicador> result = null;
		
		Func <ImagemIndicador, bool> predicate = d => (d.indicador != null && 
		                                               d.indicador.servico != null && 
		                                               d.indicador.servico.ambiente != null &&
		                                               d.indicador.servico.ambiente.vistoria != null) ? 
												(d.indicador.servico.ambiente.vistoria.OID == vistoria.OID && 
												 d.indicador.servico.ambiente.nome == ambiente && 
												 d.indicador.servico.nome == servico) : false;

		result = LoadByCriteria (predicate);
		
		return result;
	}

	/*public void Synchronize () {
		ImagemIndicadorDAO imagemIndicadorDAO = new ImagemIndicadorDAO();
		ImagemIndicadorWS.ImagemIndicadorWSService imagemIndicadorWS = new ImagemIndicadorWS.ImagemIndicadorWSService ();
		List<ImagemIndicador> imagemIndicadors = imagemIndicadorDAO.LoadAllIncludingDeleted ();
		
		foreach (ImagemIndicador imagemIndicador in imagemIndicadors) {
			//Delete
			if (imagemIndicador != null && imagemIndicador.IsDeleted && imagemIndicador.id != 0) {
				imagemIndicadorWS.delete (imagemIndicador.GetJson ());
				continue;
			}
			
			//Save
			if (imagemIndicador != null && imagemIndicador.id == 0 && !imagemIndicador.IsDeleted) {
				int id = 0;
				string json = imagemIndicador.GetJson ();
				string result = imagemIndicadorWS.save (json);
				if (int.TryParse (result, out id)) 
					imagemIndicador.id = id;
				
				imagemIndicadorDAO.UpdateWithoutSetAsUpdated (imagemIndicador);
				continue;
			}
			
			//Update
			if (imagemIndicador != null && imagemIndicador.IsUpdated) {
				imagemIndicadorWS.update (imagemIndicador.GetJson ());
				imagemIndicadorDAO.UpdateWithoutSetAsUpdated (imagemIndicador);
				continue;
			}
			
		}
	}*/
}
