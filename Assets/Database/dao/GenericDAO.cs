using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Sqo;
using SiaqodbDemo;
using System.IO;

public abstract class GenericDAO <T, PK> where T : Entity<PK> {

	private static readonly int MAX_DATA = 100000;
	public Siaqodb siaqodb;

	public GenericDAO() {
		DatabaseFactory.OpenDatabase ();
		siaqodb = DatabaseFactory.GetInstance();
	}

	public void SaveOrUpdate(Entity<PK> se){
		Sqo.Attributes.SiaqSordiImpl.ValidTransaction (se);

		if (se != null) {
			if (se.OID == 0) {
				siaqodb.StoreObject (se);
			} else {
				if (se != null && !(0).Equals (se.GetId ()) && !("").Equals(se.GetId ())) 
					se.IsUpdated = true;
				siaqodb.StoreObject (se);
			}
		}
	}

	public void SaveList (List<T> tList) {
		foreach (T t in tList)
			SaveOrUpdate (t);
	}

	public void UpdateWithoutSetAsUpdated (Entity<PK> se) {
		if (se != null) {
			se.IsUpdated = false;
			siaqodb.StoreObject (se);
		}
	}

	public T LoadById (PK id) {
		T result = null;
		try {
			var query = (from T t in siaqodb select t);
			result = query.ToList<T> ().Where (t => t.GetId ().Equals(id)).ToList ()[0];
		} catch {
			return null;
		}
		return result;
	}

	public T LoadByOID (int oid) {
		var query = (from T t in siaqodb select t);
		return query.ToList<T> ().Where (t => t.OID == oid).ToList ()[0];
	}

	public T LoadLast () {
		List<T> tList = (from T t in siaqodb select t).ToList<T> ();
		if (tList != null && tList.Count > 0)
			return tList.Last<T> ();
		else
			return null;
	}

	/*virtual public List<T> LoadAll(){
		IEnumerable<T> query = (from T t in siaqodb select t).Take(MAX_DATA);

		return query.ToList<T> ();
	}*/

	public List<T> LoadAllIncludingDeleted(){
		IEnumerable<T> query = (from T t in siaqodb
		                        select t).Take(MAX_DATA);
		
		return query.ToList<T> ();
	}

	//public abstract void Delete(T entity);

	public void DeleteFile () {
		IEnumerable<T> query = (from T t in siaqodb
		                        select t).Take(100000);

		foreach (T t in query.ToList<T> ()) {
			siaqodb.Delete (t);
		}
	}

	public List <T> LoadByCriteria (Func<T, bool> predicate) {
		var query = (from T t in siaqodb 
		             #if !UNITY_IOS
		             orderby t.OID 
		             #endif
		             select t);
		return query.ToList<T> ().Where (predicate).ToList ();
	}

}
