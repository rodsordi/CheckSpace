﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class IndicadorDAO : GenericDAO<Indicador, int> {
	public List<Indicador> LoadByVistoria(Vistoria vistoria)
	{
		List<Indicador> result = null;
		
		Func <Indicador, bool> predicate = d => (d.servico != null && 
		                                         d.servico.ambiente != null &&
		                                         d.servico.ambiente.vistoria != null) ? d.servico.ambiente.vistoria.OID == vistoria.OID : false;
		result = LoadByCriteria (predicate);
		
		return result;
	}

	public List<Indicador> LoadByVistoriaAndAmbienteAndServico (Vistoria vistoria, string ambiente, string servico) {
		List<Indicador> result = null;

		Func <Indicador, bool> predicate = d => (d.servico != null && 
		                                          d.servico.ambiente != null &&
		                                          d.servico.ambiente.vistoria != null) ? (d.servico.ambiente.vistoria.OID == vistoria.OID &&
		                                        d.servico.ambiente.nome == ambiente && 
		                                        d.servico.nome == servico) : false;
		result = LoadByCriteria (predicate);
		//Debug.Log("resultados encontradoss = " + result.Count);
		return result;
	}

	public List<string> getNomeAmbientesByVistoria (Vistoria vistoria) {
		var query = (from Indicador i in siaqodb
		             group i by i.servico.ambiente.nome into result
		             select result.Key);
		return query.ToList();
	} 


	/*public void Synchronize () {
		IndicadorDAO indicadorDAO = new IndicadorDAO();
		IndicadorWS.IndicadorWSService indicadorWS = new IndicadorWS.IndicadorWSService ();
		List<Indicador> indicadors = indicadorDAO.LoadAllIncludingDeleted ();
		
		foreach (Indicador indicador in indicadors) {
			//Delete
			if (indicador != null && indicador.IsDeleted && indicador.id != 0) {
				indicadorWS.delete (indicador.GetJson ());
				continue;
			}
			
			//Save
			if (indicador != null && indicador.id == 0 && !indicador.IsDeleted) {
				int id = 0;
				string json = indicador.GetJson ();
				string result = indicadorWS.save (json);
				if (int.TryParse (result, out id)) 
					indicador.id = id;
				
				indicadorDAO.UpdateWithoutSetAsUpdated (indicador);
				continue;
			}
			
			//Update
			if (indicador != null && indicador.IsUpdated) {
				indicadorWS.update (indicador.GetJson ());
				indicadorDAO.UpdateWithoutSetAsUpdated (indicador);
				continue;
			}
			
		}
	}*/
}
