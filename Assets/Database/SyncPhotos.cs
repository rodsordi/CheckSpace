﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SyncPhotos {
	public delegate void SyncCompletedHandler ();

	public static void Sync (List<string> urls, int photosAmountPerTime, SyncPhotosGO.SyncPhotosGOCompletedHandler syncPhotosGOCompleted) {
		new SyncPhotosManager ().Sync (urls, photosAmountPerTime, syncPhotosGOCompleted);
	}
}

public class SyncPhotosManager {
	private int index;
	private List<List<string>> packages;
	private Dictionary<string, byte[]> allPackagesResult;
	private event SyncPhotosGO.SyncPhotosGOCompletedHandler syncPhotosGOCompleted;

	public SyncPhotosManager () {
		allPackagesResult = new Dictionary<string, byte[]> ();
	}

	public void Sync (List<string> urls, int photosAmountPerTime, SyncPhotosGO.SyncPhotosGOCompletedHandler syncPhotosGOCompleted) {
		this.syncPhotosGOCompleted = syncPhotosGOCompleted;

		//List validation
		if (urls != null && urls.Count > 0) {
			//Create packages of strings
			packages = new List<List<string>> ();
			List<string> package = null;
			for (int i = 0; i < urls.Count; i++) {
				string url = urls[i];

				if (i % photosAmountPerTime == 0) {
					package = new List<string> ();
					packages.Add (package);
				}

				package.Add (url);
			}

			CreateDownloadPackage(0);

			Debug.Log ("packages.Count: " + packages.Count);
			foreach (List<string> list in packages)
				Debug.Log ("list.Count: " + list.Count);

		} else {
			syncPhotosGOCompleted (null);
		}
	}

	private void CreateDownloadPackage (int i) {
		//Create game object to download package of images
		GameObject go = new GameObject ("SyncPhotos");
		SyncPhotosGO syncPhotosGO = go.AddComponent<SyncPhotosGO> ();
		syncPhotosGO.Sync (packages[i], SyncPackageCompleted);
	}

	public void SyncPackageCompleted (Dictionary<string, byte[]> result) {
		foreach (KeyValuePair<string, byte[]> keyPair in result) {
			if (!allPackagesResult.ContainsKey (keyPair.Key))
				allPackagesResult.Add (keyPair.Key, keyPair.Value);
		}

		Debug.Log ("index: " + index + " Package Completed reached");
		index++;
		if (index <= (packages.Count-1)) {
			CreateDownloadPackage (index);
		} else {
			syncPhotosGOCompleted (allPackagesResult);
		}
	}
}

public class SyncPhotosGO : MonoBehaviour {
	public List<string> urls;
	public int downloadIndex;

	private Dictionary<string, byte[]> result = new Dictionary<string, byte[]> ();

	public delegate void SyncPhotosGOCompletedHandler (Dictionary<string, byte[]> result);
	public event SyncPhotosGOCompletedHandler syncPhotosGOCompleted;

	public void Sync (List<string> urls, SyncPhotosGOCompletedHandler syncPhotosGOCompleted) {
		this.syncPhotosGOCompleted = syncPhotosGOCompleted;

		foreach (string url in urls)
			StartCoroutine (DownloadPhoto (url));

		StartCoroutine (DownloadChecker ());
	}

	private IEnumerator DownloadPhoto (string url) {
		WWW www = null;
		
		if (url != null && url != "" && url != "null")
			www = new WWW (url);
		
		downloadIndex++;
		
		yield return www;

		if (www != null) {
			if (www.error != null) {
				print (www.error);
				
			} else {
				if (!result.ContainsKey (url))
					result.Add (url, www.bytes);
			}
		}

		downloadIndex--;
	}
	
	private IEnumerator DownloadChecker () {
		while (downloadIndex != 0) {
			yield return new WaitForSeconds (0.3f);
		}
		yield return new WaitForSeconds (0.3f);
		syncPhotosGOCompleted (this.result);
		Destroy (this.gameObject);
	}

}