﻿using UnityEngine;
using System.Collections;
using Sqo;

public class Vistoriador : Entity<int> {
	[Json.Id]
	public int id { get; set; }
	[Sqo.Attributes.MaxLength(100)]
	public string nome { get; set; }
	[Sqo.Attributes.MaxLength(10)]
	public string usuario { get; set; }
	[Sqo.Attributes.MaxLength(10)]
	public string senha { get; set; }

	public Vistoriador ()
	{
	}

	public Vistoriador (int id, string nome, string usuario, string senha)
	{
		this.id = id;
		this.nome = nome;
		this.usuario = usuario;
		this.senha = senha;
	}
	
	public Vistoriador (string json)
	{
		SetJson (json);
	}

	public override string ToString ()
	{
		return string.Format (base.ToString () + "[Vistoriador: id={0}, nome={1}, usuario={2}, senha={3}]", id, nome, usuario, senha);
	}
	
}
