﻿using UnityEngine;
using System.Collections;

public class ImagemIndicador : Entity<int> {
	[Json.Id]
	public int id { get; set; }

	[Sqo.Attributes.MaxLength(255)]
	[Sqo.Attributes.Nullable]
	public string tag { get; set; }

	[Sqo.Attributes.MaxLength(255)]
	[Sqo.Attributes.Text]
	[Sqo.Attributes.Nullable]
	public string imagemUrl { get; set; }

	[Sqo.Attributes.MaxLength(255)]
	[Sqo.Attributes.Text]
	[Sqo.Attributes.Nullable]
	public string comentario { get; set; }

	public Indicador indicador {get; set;}

	[Json.Ignore]
	public byte[] imagem;



	public ImagemIndicador () {}

	public ImagemIndicador (string json) {
		SetJson (json);
	}
	
}
