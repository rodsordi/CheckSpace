﻿using UnityEngine;
using System;
using System.Collections;
using System.Reflection;
using SimpleJSON;
using System.Collections.Generic;
using System.Globalization;

public class JsonBehaviour {
	
	public string GetJson () {
		JSONClass clazz = new JSONClass ();
		
		foreach (PropertyInfo property in GetType().GetProperties()) {
			string name = property.Name;
			object value = property.GetValue (this, null);
			
			if (value != null) {
				if (property.IsDefined (typeof(Json.Ignore), true)) {
					continue;
					
				} else if (property.PropertyType.BaseType == typeof(JsonBehaviour) || property.PropertyType.BaseType.BaseType == typeof(JsonBehaviour)) {
					JsonBehaviour jbo = (JsonBehaviour) value;
					clazz [name] = this.GetId (jbo);
					
				} else if (property.PropertyType == typeof(DateTime)) {
					DateTime date = (DateTime) value;
					if (date != default (DateTime))
						clazz [name] = date.ToString ("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					
				} else if (property.PropertyType == typeof(int)) {
					clazz [name] = value.ToString ();
					
				} else if (property.PropertyType == typeof(string)) {
					if (value != null)
						clazz [name] = value.ToString ();
					
				} else if (property.PropertyType == typeof (char)) {
					if ((char) value != default (char)) 
						clazz [name] = value.ToString ();
					
				} else if (property.PropertyType == typeof (bool)) {
					clazz [name] = value.ToString ();
					
				} else if (property.PropertyType == typeof (double)) {
					clazz [name] = value.ToString ();

				}
			}
		}
		
		return clazz.ToString ();
	}
	
	protected void SetJson (string json) {
		if (json == null || json == "" || json == "{}")
			return;

		try {
			JSONNode node = JSONNode.Parse (json);
			
			if (node != null) {
				
				foreach (PropertyInfo property in GetType().GetProperties()) {
					string name = property.Name;
					
					if (node [name].Value != "") {
						if (property.IsDefined (typeof(Json.Ignore), true)) {
							continue;
							
						} else if (property.PropertyType.BaseType == typeof(JsonBehaviour) || property.PropertyType.BaseType.BaseType == typeof(JsonBehaviour)) {
							JsonBehaviour jbo = (JsonBehaviour)Activator.CreateInstance (property.PropertyType);
							SetId (jbo, node [name].Value);
							property.SetValue (this, jbo, null);
							
						} else if (property.PropertyType == typeof(DateTime)) {
							object value = DateTime.ParseExact (node [name].Value, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
							property.SetValue (this, value, null);
							
						} else if (property.PropertyType == typeof(int)) {
							object value = node [name].AsInt;
							property.SetValue (this, value, null);
							
						}  else if (property.PropertyType == typeof(char)) {
							object value = node [name].Value [0];
							property.SetValue (this, value, null);
							
						} else if (property.PropertyType == typeof(bool)) {
							object value = node [name].AsBool;
							property.SetValue (this, value, null);
							
						} else if (property.PropertyType == typeof (double)) {
							object value = node [name].AsDouble;
							property.SetValue (this, value, null);

						}
					} 

					if (property.PropertyType == typeof(string)) {
						object value = node [name].Value;
						property.SetValue (this, value, null);
						
					}
				}
			}
		} catch (Exception e) {
			Debug.Log ("Json Exception: " + json);
			throw e;
		}
	}

	public static string GetJsonFromList<T> (List<T> list) where T : JsonBehaviour {
		string result = null;

		if (list != null && list.Count > 0) {
			JSONClass jsonList = new JSONClass ();

			string max = (list.Count - 1).ToString ();
			for (int i = 0; i < list.Count; i++) {
				if (list[i] == null)
					continue;

				string index = i.ToString ();
				while (index.Length < max.Length)
					index = "0" + index;

				string key = typeof(T).ToString () + "-" + index;
				string json = list[i].GetJson ();
				JSONNode jsonObj = JSONNode.Parse (json);
				jsonList[key] = jsonObj;
			}

			result = jsonList.ToString ();
		}

		return result;
	}
	
	public static List<T> GetList<T> (string json) where T : JsonBehaviour {
		List<T> result = new List<T> ();
		if (json == null || json == "" || json == "{}")
			return result;
		
		JSONNode node = JSONNode.Parse (json);
		
		if (node != null) {
			for (int i = 0; i < node.Count; i++) {
				T t = (T)Activator.CreateInstance (typeof(T));
				t.SetJson (node [i].ToString ());
				result.Add (t);
			}
		}
		
		return result;
	}
	
	private string GetId(JsonBehaviour jbo) {
		foreach (PropertyInfo property in jbo.GetType ().GetProperties()) {
			object value = property.GetValue (jbo, null);
			if (property.IsDefined (typeof(Json.Id), true)) { 
				if (value != null) {
					return value.ToString ();
				}
			}
		}
		return "0";
	}
	
	private void SetId(JsonBehaviour jbo, string id) {
		foreach (PropertyInfo property in jbo.GetType ().GetProperties()) {
			if (property.IsDefined (typeof(Json.Id), true)) {
				if (property.PropertyType == typeof(int)) {
					property.SetValue (jbo, int.Parse (id), null);
					return;
				}else if (property.PropertyType == typeof(string)) {
					property.SetValue (jbo, id, null);
					return;
				}
			}
		}
	}
}

namespace Json {
	public class Id : System.Attribute {}
	
	public class Ignore : System.Attribute {}
}
