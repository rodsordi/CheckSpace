﻿using UnityEngine;
using System.Collections;
using System;
using Sqo;

public class Vistoria : Entity<int> {
	[Json.Id]
	public int id { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	public string proprietario { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	[Sqo.Attributes.Nullable]
	public string empreendimento { get; set; }

	[Sqo.Attributes.Nullable]
	public double valor { get; set; }

	[Sqo.Attributes.Nullable]
	public double metrosQuadrados { get; set; }

	[Sqo.Attributes.MaxLength(255)]
	[Sqo.Attributes.Text]
	[Sqo.Attributes.Nullable]
	public string endereco { get; set; }

	[Sqo.Attributes.MaxLength(10)]
	[Sqo.Attributes.Nullable]
	public string unidade { get; set; }

	[Sqo.Attributes.MaxLength(10)]
	[Sqo.Attributes.Nullable]
	public string bloco { get; set; }

	[Sqo.Attributes.Nullable]
	public DateTime dataCompra { get; set; }

	[Sqo.Attributes.Nullable]
	public double metragem { get; set; }

	[Sqo.Attributes.MaxLength(45)]
	[Sqo.Attributes.Nullable]
	public string padrao { get; set; }

	[Sqo.Attributes.Nullable]
	public DateTime dataVistoria { get; set; }

	[Sqo.Attributes.Nullable]
	public DateTime horarioAgendado { get; set; }

	[Sqo.Attributes.Nullable]
	public DateTime inicioVistoria { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	[Sqo.Attributes.Nullable]
	public string incorporadora { get; set; }

	[Sqo.Attributes.Nullable]
	public DateTime terminoVistoria { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	[Sqo.Attributes.Nullable]
	public string construtora { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	[Sqo.Attributes.Nullable]
	public string clima { get; set; }

	[Sqo.Attributes.MaxLength(255)]
	[Sqo.Attributes.Text]
	[Sqo.Attributes.Nullable]
	public string imagemPredioUrl { get; set; }

	public Vistoriador vistoriador { get; set; }

	[Json.Ignore]
	public byte[] imagem;

	public Vistoria ()
	{
	}

	public Vistoria (int id, string proprietario, string empreendimento, double valor, string endereco, string unidade, string bloco, DateTime dataCompra, double metragem, string padrao, DateTime dataVistoria, DateTime horarioAgendado, DateTime inicioVistoria, string incorporadora, DateTime terminoVistoria, string construtora, string clima, string imagemPredioUrl, Vistoriador vistoriador)
	{
		this.id = id;
		this.proprietario = proprietario;
		this.empreendimento = empreendimento;
		this.valor = valor;
		this.endereco = endereco;
		this.unidade = unidade;
		this.bloco = bloco;
		this.dataCompra = dataCompra;
		this.metragem = metragem;
		this.padrao = padrao;
		this.dataVistoria = dataVistoria;
		this.horarioAgendado = horarioAgendado;
		this.inicioVistoria = inicioVistoria;
		this.incorporadora = incorporadora;
		this.terminoVistoria = terminoVistoria;
		this.construtora = construtora;
		this.clima = clima;
		this.imagemPredioUrl = imagemPredioUrl;
		this.vistoriador = vistoriador;
	}
	

	public Vistoria (string json)
	{
		SetJson (json);
	}

	public override string ToString ()
	{
		return string.Format (base.ToString ()+"[Vistoria: id={0}, proprietario={1}, empreendimento={2}, valor={3}, metrosQuadrados={4}, endereco={5}, unidade={6}, bloco={7}, dataCompra={8}, metragem={9}, padrao={10}, dataVistoria={11}, horarioAgendado={12}, inicioVistoria={13}, incorporadora={14}, terminoVistoria={15}, construtora={16}, clima={17}, imagemPredioUrl={18}, vistoriador={19}]", 
		                      id, proprietario, empreendimento, valor, metrosQuadrados, endereco, unidade, bloco, dataCompra, metragem, padrao, dataVistoria, horarioAgendado, inicioVistoria, 
		                      incorporadora, terminoVistoria, construtora, clima, imagemPredioUrl, (vistoriador != null ? vistoriador.OID : 0));
	}
	
	
}
