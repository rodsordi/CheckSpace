﻿using UnityEngine;
using System.Collections;

public class Indicador : Entity<int> {
	[Json.Id]
	public int id { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	public string nome { get; set; }

	[Sqo.Attributes.MaxLength(45)]
	public string grupo { get; set; }

	[Sqo.Attributes.MaxLength(45)]
	public string subGrupo { get; set; }

	public int indicador { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	[Sqo.Attributes.Nullable]
	public string tag { get; set; }

	[Sqo.Attributes.MaxLength(255)]
	[Sqo.Attributes.Text]
	[Sqo.Attributes.Nullable]
	public string comentario { get; set; }

	public Servico servico { get; set; }



	public Indicador () {}

	public Indicador (string json)
	{
		SetJson (json);
	}

	public override string ToString ()
	{
		return string.Format (base.ToString() + "[Indicador: id={0}, nome={1}, grupo={2}, subGrupo={3}, indicador={4}, tag={5}, comentario={6}, servico={7}]", 
		                      id, nome, grupo, subGrupo, indicador, tag, comentario, (servico != null) ? servico.OID : 0);
	}
	
}
