﻿using UnityEngine;
using System.Collections;

public class Servico : Entity<int> {
	[Json.Id]
	public int id { get; set; }
	
	[Sqo.Attributes.MaxLength(100)]
	public string nome { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	[Sqo.Attributes.Nullable]
	public string tag { get; set; }

	[Sqo.Attributes.MaxLength(255)]
	[Sqo.Attributes.Text]
	[Sqo.Attributes.Nullable]
	public string comentario { get; set; }
	
	public Ambiente ambiente { get; set; }



	public Servico () {}

	public Servico (string json) {
		SetJson (json);
	}

	public override string ToString ()
	{
		return string.Format (base.ToString() + "[Servico: id={0}, nome={1}, tag={2}, comentario={3}, ambiente={4}]", id, nome, tag, comentario, (ambiente != null) ? ambiente.OID : 0);
	}
	
}
