﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;

public class Entity<PK> : JsonBehaviour {
	[Json.Ignore]
	public int OID { get; set; }
	[Json.Ignore]
	public bool IsUpdated { get; set; }
	[Json.Ignore]
	public bool IsDeleted { get; set; }

	//Retorna o valor da variável assinada com o attribute [Id]
	public PK GetId () {
		foreach (PropertyInfo property in this.GetType().GetProperties()) {
			object value = property.GetValue (this, null);
			if (property.IsDefined (typeof(Json.Id), true)) { 
				if (value != null) {
					return (PK) value;
				}
			}
		}
		return default(PK);
	}
	
	public override string ToString ()
	{
		return string.Format ("[Entity: OID={0}, IsUpdated={1}, IsDeleted={2}] ", OID, IsUpdated, IsDeleted);
	}
}
	
