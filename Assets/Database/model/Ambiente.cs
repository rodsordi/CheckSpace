﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Ambiente : Entity<int> {
	[Json.Id]
	public int id { get; set; }

	[Sqo.Attributes.MaxLength(100)]
	public string nome { get; set; }

	[Sqo.Attributes.Nullable]
	public double peDireito { get; set; }
	
	[Sqo.Attributes.Nullable]
	public double area { get; set; }

	[Sqo.Attributes.Nullable]
	public double acusticoAberto { get; set; }
	
	[Sqo.Attributes.Nullable]
	public double acusticoFechado { get; set; }
	
	[Sqo.Attributes.Nullable]
	public double termoAberto { get; set; }
	
	[Sqo.Attributes.Nullable]
	public double termoFechado { get; set; }

	[Sqo.Attributes.Nullable]
	public string acusticoAbertoComRes { get; set; }
	
	[Sqo.Attributes.Nullable]
	public string acusticoFechadoComRes { get; set; }
	
	[Sqo.Attributes.Nullable]
	public string termoAbertoComRes { get; set; }
	
	[Sqo.Attributes.Nullable]
	public string termoFechadoComRes { get; set; }

	public Vistoria vistoria { get; set; }



	public Ambiente () {}

	public Ambiente (string json) {
		SetJson (json);
	}
	public override string ToString ()
	{
		return string.Format (base.ToString() + "[Ambiente: id={0}, nome={1}, peDireito={2}, area={3}, acusticoAberto={4}, acusticoFechado={5}, termoAberto={6}, termoFechado={7}, acusticoAbertoComRes={8}, acusticoFechadoComRes={9}, termoAbertoComRes={10}, termoFechadoComRes={11}, vistoria={12}]", 
		                      id, nome, peDireito, area, acusticoAberto, acusticoFechado, termoAberto, termoFechado, acusticoAbertoComRes, acusticoFechadoComRes, termoAbertoComRes, termoFechadoComRes, (vistoria != null) ? vistoria.OID : 0
		                      );
	}
	
}
