﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.IO;

public class ConnectionManager : MonoBehaviour {

	void Start()
	{
		DontDestroyOnLoad(gameObject);
	}
	
	public static bool IsConnected (bool spawnMessage) {
		WebClient client = null;
		Stream stream = null;
		try
		{
			client = new System.Net.WebClient();
		
			stream = client.OpenRead("http://www.google.com");
			stream.ReadTimeout = 30000;

			return true;
		}
		catch (System.Exception e)
		{
			Debug.Log (e);
			if(spawnMessage)
			{
				OpenConnectionModal();
			}
			return false;
		}
		finally
		{
			if (client != null) {client.Dispose();}
			if (stream != null) {stream.Dispose();}
		}
	}
	
	public static void OpenConnectionModal()
	{
		GameObject connectionModal;
		string resource = "";
		#if UNITY_ANDROID
		resource = "ConnectionModal";
		#elif UNITY_IOS
		resource = "ConnectionModalIos";
		#endif
		GameObject UIRootObj = GameObject.FindGameObjectWithTag("UIRoot");
		connectionModal = NGUITools.AddChild(UIRootObj, Resources.Load(resource) as GameObject);	
		connectionModal.name = "Panel Connection";			
	}
}
