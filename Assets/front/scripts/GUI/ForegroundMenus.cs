﻿using UnityEngine;
using System.Collections;

public class ForegroundMenus : MonoBehaviour {

	public GameObject menuArrow;
	public GameObject ambientsArrow;
	public UILabel menuTitle;
	public UILabel bottomTitle;
	public static ForegroundMenus instace;
	
	void Start()
	{
		instace = this;
	}
	
	public void SetBottomTitle(string text)
	{
		bottomTitle.text = text;
	}
	
	public void SetMenuTitle(string text)
	{
		menuTitle.text = text;
	}
	
	public void ShowMenuTitle(bool val)
	{
		menuTitle.gameObject.SetActive(val);
	}
	
	public void ShowBottomTitle(bool val)
	{
		bottomTitle.gameObject.SetActive(val);
	}
	
	public void ShowMenuArrow(bool val)
	{
		menuArrow.SetActive(val);
	}
	
	public void ShowAmbientsArrow(bool val)
	{
		ambientsArrow.SetActive(val);
	}
}
