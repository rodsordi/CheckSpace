﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommentsWindowSmall : MonoBehaviour
{
	public UILabel commentLabel;
	public UIInput commentLabelInputTags;
	public UILabel commentLabelTags;
	public CheckSpaceConstants.CheckSpaceCategories category;
	public UIScrollView scrollComments;


	public void Close ()
	{
		gameObject.SetActive (false);	
		commentLabel.text = "";
		commentLabelTags.text = "";
	}

	public void SetComment (string text, string tags)
	{
		commentLabel.text = text;
		commentLabelInputTags.value = text;
		commentLabelTags.text = tags;
		gameObject.SetActive (true);
		scrollComments.ResetPosition ();
	}

	public void OnChangeComment ()
	{
		switch (category) {
		case CheckSpaceConstants.CheckSpaceCategories.Ceramic:
			CheckSpaceManager.instance.selectedAmbient.ceramicInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.CeramicWall:
			CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Eletrics:
			CheckSpaceManager.instance.selectedAmbient.eletricsInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Frames:
			CheckSpaceManager.instance.selectedAmbient.framesInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Hidraulics:
			CheckSpaceManager.instance.selectedAmbient.hidraulicsInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingCeiling:
			CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingWall:
			CheckSpaceManager.instance.selectedAmbient.paintingWallInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Sills:
			CheckSpaceManager.instance.selectedAmbient.sillsInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Peitoris:
			CheckSpaceManager.instance.selectedAmbient.peitorisInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.TentosBaguetes:
			CheckSpaceManager.instance.selectedAmbient.tentosBaguetesInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.WoodenFloor:
			CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalCeiling:
			CheckSpaceManager.instance.selectedAmbient.functionalCeilingInformation.service.comentario = commentLabel.text;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalWall:
			CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.service.comentario = commentLabel.text;
			break;

		}
	}
}
