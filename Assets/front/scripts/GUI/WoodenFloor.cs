﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WoodenFloor : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject rowPrefabI;
	
	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	public Ambient selectedAmbients;

	UIToggle toggleVerify;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.WoodenFloor;
	}

	public override void UpdateComments ()
	{
		string comment = "";
		
		for (int i = 0; i < rows.Count; i++) {
			
			foreach (var item in rows [i].GetComponent <AspectRow> ().indicators) {
				if (item.indicador == 2) {
					if (comment != "")
						comment += ", ";
					comment += item.nome;
				}
			}
		}

		CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.service.tag = comment;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		rows.Clear ();
		
		ForegroundMenus.instace.SetBottomTitle ("Piso Madeira");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = category;

		GameObject header2 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header2.GetComponent<LabelName> ().SetName ("I - Revestimento Madeira");
		
		GameObject goI = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabI);
		goI.name = CheckSpaceManager.instance.selectedAmbient.name + " Revestimento";

		rows.Add (goI);

		for (int i = 0; i < rows.Count; i++) {
			Debug.Log (CheckSpaceManager.instance.selectedAmbient.name);
			rows [i].GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.indicatorsList [i];
			rows [i].GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.evidencias [i];
			rows [i].GetComponent <AspectRow> ().ambiente = CheckSpaceManager.instance.selectedAmbient.name;
			rows [i].GetComponent <AspectRow> ().servico = CheckSpaceConstants.CheckSpaceCategories.WoodenFloor.ToString ();
			rows [i].GetComponent <AspectRow> ().categoryObject = this;
		}

		serviceTags = CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.verified = toggleVerify.value;
		int save = 0;
		if (toggleVerify.value)
			save = 1;
		PlayerPrefs.SetInt (CheckSpaceConstants.WoodenFloorVerified + StaticData.vistoriaAtual.id.ToString (), save);
		Debug.Log (CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.verified);
	}
}
