﻿using UnityEngine;
using System.Collections;

public class CategoryButton : MonoBehaviour
{

	public string spriteName;
	public CheckSpaceConstants.CheckSpaceCategories category;
	public UILabel labelName;
	public UISprite spriteVerified;

	void Start ()
	{
		labelName.text = gameObject.name;
	}

	public void OnClick ()
	{
		if (labelName.text != "Dados Gerais e Ambientes") {
			if (CheckSpaceManager.instance.createdAmbients.Count > 0) {
				HorizontalScroller.Instance.AdvanceScrollPosition ();
				CheckSpaceManager.instance.selectedCategory = category;
				CheckSpaceManager.instance.ambientMenu.gameObject.SetActive (true);
				CheckSpaceManager.instance.ambientMenu.FillAmbientList ();
				ForegroundMenus.instace.SetMenuTitle (labelName.text);
				GameObject.FindGameObjectWithTag ("AmbientsMenu").GetComponent<AmbientsMenu> ().currentTitle = labelName.text;
				ForegroundMenus.instace.ShowMenuArrow (true);
			}
		} else {
			HorizontalScroller.Instance.MoveToMiddleScrollPosition ();
			CheckSpaceManager.instance.selectedCategory = CheckSpaceConstants.CheckSpaceCategories.Home;
			CheckSpaceManager.instance.ShowHome ();
		}

		VerticalScroller.Instance.ResetScrollPosition ();
	}
	
}
