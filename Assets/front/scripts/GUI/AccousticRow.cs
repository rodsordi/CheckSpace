﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class AccousticRow : MonoBehaviour
{

	public double openAccoustic;
	public double closedAccoustic;
	public double openTermic;
	public double closedTermic;

	int openResCom = 1;
	int closedResCom = 1;

	public UILabel labelOpenAccoustic;
	public UILabel labelClosedAccoustic;
	public UILabel labelOpenTermic;
	public UILabel labelClosedTermicc;
	public UIToggle toggleOpenRes;
	public UIToggle toggleOpenCom;
	public UIToggle toggleClosedRes;
	public UIToggle toggleClosedCom;

	public UIButton commentButtonPe;
	public UIButton commentButtonArea;
	CommentsWindow commentsPanel;
	string tagsPe;
	string tagsArea;
	string commentArea;
	
	public List<string> allString = new List<string> ();

	void Start ()
	{
		commentsPanel = CheckSpaceManager.instance.commentsWindow;
	}

	public void OpenAccousticChanged ()
	{
		double.TryParse (labelOpenAccoustic.text, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out openAccoustic);
		CheckSpaceManager.instance.selectedAmbient.ambient.acusticoAberto = openAccoustic;
	}

	public void ClosedAccousticChanged ()
	{
		double.TryParse (labelClosedAccoustic.text, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out closedAccoustic);
		CheckSpaceManager.instance.selectedAmbient.ambient.acusticoFechado = closedAccoustic;
	}

	public void OpenTermicChanged ()
	{
		double.TryParse (labelOpenTermic.text, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out openTermic);
		CheckSpaceManager.instance.selectedAmbient.ambient.termoAberto = openTermic;
	}

	public void ClosedTermicChanged ()
	{
		double.TryParse (labelClosedTermicc.text, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out closedTermic);
		CheckSpaceManager.instance.selectedAmbient.ambient.termoFechado = closedTermic;
	}

	public void OpenResChanged ()
	{
		if (toggleOpenRes.value) {
			openResCom = 1;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoAbertoComRes = "Res";
		} else {
			openResCom = 0;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoAbertoComRes = "Com";
		}

	}

	public void OpenComChanged ()
	{
		if (toggleOpenCom.value) {
			openResCom = 1;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoAbertoComRes = "Com";
		} else {
			openResCom = 0;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoAbertoComRes = "Res";
		}

	}

	public void ClosedResChanged ()
	{
		if (toggleClosedRes.value) {
			closedResCom = 1;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoFechadoComRes = "Res";
		} else {
			closedResCom = 0;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoFechadoComRes = "Com";
		}
	}

	public void ClosedComChanged ()
	{
		if (toggleClosedCom.value) {
			closedResCom = 1;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoFechadoComRes = "Com";
		} else {
			closedResCom = 0;
			CheckSpaceManager.instance.selectedAmbient.ambient.acusticoFechadoComRes = "Res";
		}

	}

}
