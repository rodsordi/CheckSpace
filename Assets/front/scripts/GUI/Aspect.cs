﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Aspect : MonoBehaviour
{

	public string name = "";
	public Indicador indicator;
	public UIToggle toggleApproved;
	public UIToggle toggleRepproved;
	public UIToggle toggleNA;
	public UILabel labelName;
	public GUICategory categoryObject;
	public ImagemIndicador evidency;
	
	public void ChangeIndicator ()
	{
		if (toggleApproved.value)
		{
			indicator.indicador = 1;
			categoryObject.UpdateComments();
		}
		else if (toggleRepproved.value)
		{
			indicator.indicador = 2;
			categoryObject.UpdateComments();
		}
		else if (toggleNA.value)
		{
			indicator.indicador = 3;
			categoryObject.UpdateComments();
		}
	}

	public void PhotoEvidencyClick()
	{

		CheckSpaceManager.instance.photosWindow.Open();
		CheckSpaceManager.instance.photosWindow.FillComments(categoryObject.serviceTags);
	
		CheckSpaceManager.instance.photosWindow.SetEvidence(evidency);
		CheckSpaceManager.instance.photosWindow.LoadText();
		CheckSpaceManager.instance.photosWindow.LoadTags();
		CheckSpaceManager.instance.photosWindow.ClickPhotoButton();

	}

}
