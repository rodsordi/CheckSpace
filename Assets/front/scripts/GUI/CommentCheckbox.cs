﻿using UnityEngine;
using System.Collections;

public class CommentCheckbox : MonoBehaviour {

	public UILabel commentLabel;
	public string comment;
	public PhotosWindow commentsWindow;
	public UIToggle toggle;

	public void SetComment(string cmt)
	{
		comment = cmt;
		commentLabel.text = comment;
	}

	void Start()
	{
		if(toggle == null)
			toggle = GetComponent<UIToggle>();
	}

	public void SetToggleChange()
	{
		toggle = GetComponent<UIToggle>();
		EventDelegate toggleDelegate = new EventDelegate (commentsWindow, "CommentToggleChange");
		EventDelegate.Parameter parameter = new EventDelegate.Parameter(this);
		toggleDelegate.parameters[0] = parameter;
		toggle.onChange.Add(toggleDelegate);
	}
}
