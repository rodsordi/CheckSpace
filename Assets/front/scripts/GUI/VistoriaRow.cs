﻿using UnityEngine;
using System.Collections;

public class VistoriaRow : MonoBehaviour
{
	public Vistoria vistoria;
	public UILabel client;
	public UILabel data;

	public bool online = false;
	void Start ()
	{
		client.text = vistoria.proprietario;
		data.text = vistoria.dataVistoria.ToString ("dd/MM/yyyy");
	}

	void OnClick ()
	{
		StaticData.vistoriaAtual = vistoria;
		StaticData.OnlineVistoria = online;
		GameObject.Find("Login").GetComponent<Login>().carregando.SetActive(true);
		Application.LoadLevel ("RelatorioVistoria");

	}
}
