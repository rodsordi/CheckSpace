﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dimensions : MonoBehaviour
{
	
	public GameObject ambientButtonPrefab;
	public GameObject tableHeaderPrefab;
	public GameObject tableFooterPrefab;

	public UITable tableAmbients;
	public OverviewReportScreen overViewScreen;

	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	
	public CheckSpaceManager checkspaceManager;

	UIToggle toggleVerify;

	public void UpdateAmbientList ()
	{

		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		rows.Clear ();
		CreateVerificationToggle ();
		ForegroundMenus.instace.SetBottomTitle ("Medidas In Loco");
		GameObject go = NGUITools.AddChild (tableAmbients.gameObject, ambientButtonPrefab);
		go.name = checkspaceManager.selectedAmbient.name;
		DimensionRow dRow = go.GetComponent<DimensionRow> ();
		dRow.labelArea.text = CheckSpaceManager.instance.selectedAmbient.ambient.area.ToString ();
		dRow.labelRightFoot.text = CheckSpaceManager.instance.selectedAmbient.ambient.peDireito.ToString ();
//		dRow.labelComment.text = CheckSpaceManager.instance.selectedAmbient.dimensionsInformation.dimensao.comentario;
		rows.Add (go);
		//footer = NGUITools.AddChild(tableAmbients.gameObject, tableFooterPrefab);
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.dimensionsInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.dimensionsInformation.verified = toggleVerify.value;
	}


}
