﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommentsWindow : MonoBehaviour
{
	public UILabel commentLabel;
	public UIInput commentInput;
	public UILabel tagsLabel;
	public UITable tableCommentCheckboxes;
	public List<string> availableComments;
	public GameObject prefabComments;

	public int commentCount = 0;
	public string completeComment;
	public string [] result;
	public EventDelegate callback;

	public UIScrollView scrollComments;
	public UIScrollView scrollTags;
	public UIScrollView scrollCheckboxes;

	void Start ()
	{
		//FillComments (availableComments.ToArray ());
		tagsLabel.text = string.Empty;
	}

	public void FillComments (string[] allComments)
	{
		foreach (var item in tableCommentCheckboxes.GetChildList()) {
			Destroy (item.gameObject);
		}

		scrollCheckboxes.ResetPosition();
		tableCommentCheckboxes.Reposition ();

		for (int i = 0; i < allComments.Length; i++) {
			GameObject checkbox = NGUITools.AddChild (tableCommentCheckboxes.gameObject, prefabComments);
			CommentCheckbox commentCheckbox = checkbox.GetComponent<CommentCheckbox> ();
			commentCheckbox.SetComment (allComments [i]);
//			commentCheckbox.commentsWindow = this;
			commentCheckbox.SetToggleChange ();
		}

		tableCommentCheckboxes.Reposition ();
		scrollCheckboxes.ResetPosition();
		scrollComments.ResetPosition();
		scrollTags.ResetPosition();
	}

	public void LoadText()
	{
		commentInput.value = commentLabel.text;
		commentCount = 0;
	}

	public void LoadTags()
	{
		completeComment = tagsLabel.text;
		commentCount = tagsLabel.text.Split(',').Length;
	}

	public void Close ()
	{
		gameObject.SetActive (false);	
		result = new string[] {commentLabel.text, completeComment};
		callback.Execute();
		commentLabel.text = "";
		completeComment = "";
	}

	public void CommentToggleChange (CommentCheckbox checkBox)
	{
		if (checkBox.toggle.value)
			AddWord (checkBox.comment);
		else {
			if (commentCount > 0)
				RemoveWord (checkBox.comment);
		}
		tagsLabel.text = completeComment;
	}

	public void AddWord (string word)
	{
		if (commentCount > 0)
			completeComment += ", " + word;
		else
			completeComment = word;
		commentCount++;
	}

	public void RemoveWord (string word)
	{
		if (tagsLabel.text.Contains (word)) {
			string searchString;
			if (commentCount > 1)
				searchString = ", " + word;
			else
				searchString = word;

			int index = tagsLabel.text.IndexOf (searchString);

			if (index != -1) {
				completeComment = completeComment.Replace (searchString, string.Empty);
			} else {
				if (commentCount > 1)
					completeComment = completeComment.Remove (0, word.Length + 2);
				else
					completeComment = completeComment.Remove (0, word.Length);
			}
			commentCount--;
		}
	}
}
