﻿using UnityEngine;
using System.Collections;

public class SaveBox : MonoBehaviour
{

	public UILabel lblMessage;

	public void SetMessage (string text)
	{
		lblMessage.text = text;
	}

}
