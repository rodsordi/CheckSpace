﻿using UnityEngine;
using System.Collections;

public class AmbientCheckbox : MonoBehaviour {

	public UILabel labelName;
	string ambName = "";
	public OverviewReportScreen overviewScreen;

	public void SetName (string nm) {
		labelName.text = nm;
		ambName = nm;
	}

	public void OnCheckValueChange()
	{
		if(GetComponent<UIToggle>().value)
		{
			overviewScreen.AddCheckedAmbient(ambName);
		}
		else
		{
			overviewScreen.RemoveCheckedAmbient(ambName);
		}
	}
}
