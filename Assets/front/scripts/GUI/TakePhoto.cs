﻿using UnityEngine;
using System.Collections;
using System.Web;

public class TakePhoto : MonoBehaviour
{
	public string photoName;
	public UITexture texture;

	void Start ()
	{

	}

	public void AddEventListeners ()
	{
		EtceteraAndroidManager.photoChooserSucceededEvent += OnPhotoSucceed;
		EtceteraAndroidManager.photoChooserCancelledEvent += OnPhotoCancel;
	}

	public void RemoveEventListeners ()
	{
		EtceteraAndroidManager.photoChooserSucceededEvent -= OnPhotoSucceed;
		EtceteraAndroidManager.photoChooserCancelledEvent -= OnPhotoCancel;
	}

	void OnClick ()
	{
		EtceteraAndroid.promptToTakePhoto (photoName);

	}

	public void OnPhotoSucceed (string pName)
	{
		EtceteraAndroid.scaleImageAtPath (pName, 0.5f);
		texture.mainTexture = EtceteraAndroid.textureFromFileAtPath (pName);
	
		Texture2D tex2d = (Texture2D)texture.mainTexture;
		byte[] imgBytes = tex2d.EncodeToJPG ();

		StaticData.vistoriaAtual.imagem = imgBytes;
		string url = "";

		if (StaticData.OnlineVistoria) {
			string photoName = "photoPredio";
			photoName += StaticData.vistoriaAtual.id + "_" + StaticData.vistoriaAtual.proprietario;
			string encoding = HttpUtility.UrlEncode (imgBytes);

			url = CheckSpaceManager.instance.server.saveImage ("Vistoria_" + StaticData.vistoriaAtual.id.ToString () + ";" + photoName + ";" + encoding);
			StaticData.vistoriaAtual.imagemPredioUrl = url;
		} else {
			CheckSpaceManager.instance.SaveVistoriaLocal ();
		}
	}

	public void OnPhotoCancel ()
	{

	}

	public void OnDestroy ()
	{
		RemoveEventListeners ();
	}
}
