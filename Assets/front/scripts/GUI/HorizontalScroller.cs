﻿using UnityEngine;
using System.Collections;

public class HorizontalScroller : MonoBehaviour {

	public static HorizontalScroller Instance;
	UIScrollView scrollView;
	const float scrollMidPos = 0.5031693f;

	void Start () {
		Instance = this;
		scrollView = GetComponent<UIScrollView>();
	}

	public void ResetScrollPosition()
	{
		StartCoroutine(ScrollToBeggining());
	}

	public void AdvanceScrollPosition()
	{
		StartCoroutine(ScrollToEnd());
	}

	public void MoveToMiddleScrollPosition()
	{
		StartCoroutine(ScrollToMid());
	}

	IEnumerator ScrollToBeggining()
	{
		while (scrollView.horizontalScrollBar.value > 0)
		{
			scrollView.horizontalScrollBar.value -= 2 * Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		if(scrollView.horizontalScrollBar.value < 0)
			scrollView.horizontalScrollBar.value = 0;
	}

	IEnumerator ScrollToMid()
	{
		while (scrollView.horizontalScrollBar.value < scrollMidPos)
		{
			scrollView.horizontalScrollBar.value += 2 * Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		
		if(scrollView.horizontalScrollBar.value > scrollMidPos)
			scrollView.horizontalScrollBar.value = scrollMidPos;
	}

	IEnumerator ScrollToEnd()
	{
		while (scrollView.horizontalScrollBar.value < 1)
		{
			scrollView.horizontalScrollBar.value += 2 * Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		
		if(scrollView.horizontalScrollBar.value > 1)
			scrollView.horizontalScrollBar.value = 1;
	}

	void OnDestroy()
	{
		Instance = null;
	}

}
