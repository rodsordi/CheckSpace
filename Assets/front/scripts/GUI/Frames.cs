﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Frames : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject rowPrefabI;
	public GameObject rowPrefabII;
	public GameObject rowPrefabIII;
	public GameObject rowPrefabIV;
	public GameObject rowPrefabV;
	
	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	public Ambient selectedAmbients;

	UIToggle toggleVerify;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.Frames;
	}

	public override void UpdateComments ()
	{
		string comment = "";
		
		for (int i = 0; i < rows.Count; i++) {
			
			foreach (var item in rows [i].GetComponent <AspectRow> ().indicators) {
				if (item.indicador == 2) {
					if (comment != "")
						comment += ", ";
					comment += item.nome;
				}
			}
		}

		CheckSpaceManager.instance.selectedAmbient.framesInformation.service.tag = comment;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		rows.Clear ();
		
		ForegroundMenus.instace.SetBottomTitle ("Esquadrias");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = category;

		GameObject header2 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header2.GetComponent<LabelName> ().SetName ("I - Esquadrias");
		
		GameObject goI = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabI);
		goI.name = CheckSpaceManager.instance.selectedAmbient.name + " Esquadrias";
		
		GameObject header3 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header3.GetComponent<LabelName> ().SetName ("II - Ferragens");
		
		GameObject goII = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabII);
		goII.name = CheckSpaceManager.instance.selectedAmbient.name + " Ferragens";
		
		GameObject header4 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header4.GetComponent<LabelName> ().SetName ("III - Funcionamento");
		
		GameObject goIII = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabIII);
		goIII.name = CheckSpaceManager.instance.selectedAmbient.name + " Funcionamento";
		
		GameObject header5 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header5.GetComponent<LabelName> ().SetName ("IV - Pintura");
		
		GameObject goIV = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabIV);
		goIV.name = CheckSpaceManager.instance.selectedAmbient.name + " Pintura";
		
		GameObject header6 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header6.GetComponent<LabelName> ().SetName ("VI - Vidros");
		
		GameObject goV = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabV);
		goV.name = CheckSpaceManager.instance.selectedAmbient.name + " Vidros";

		rows.Add (goI);
		rows.Add (goII);
		rows.Add (goIII);
		rows.Add (goIV);
		rows.Add (goV);

		for (int i = 0; i < rows.Count; i++) {
			rows [i].GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.framesInformation.indicatorsList [i];
			rows [i].GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.framesInformation.evidencias [i];
			rows [i].GetComponent <AspectRow> ().ambiente = CheckSpaceManager.instance.selectedAmbient.name;
			rows [i].GetComponent <AspectRow> ().servico = CheckSpaceConstants.CheckSpaceCategories.Frames.ToString ();
			rows [i].GetComponent <AspectRow> ().categoryObject = this;
		}

		serviceTags = CheckSpaceManager.instance.selectedAmbient.framesInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}


	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.framesInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.framesInformation.verified = toggleVerify.value;
		Debug.Log (CheckSpaceManager.instance.selectedAmbient.framesInformation.verified);
	}
}
