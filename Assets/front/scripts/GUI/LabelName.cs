﻿using UnityEngine;
using System.Collections;

public class LabelName : MonoBehaviour {

	public UILabel labelName;

	public void SetName(string name)
	{
		labelName.text = name;
	}
}
