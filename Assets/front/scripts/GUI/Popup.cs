﻿using UnityEngine;
using System.Collections;

public class Popup : MonoBehaviour
{

	public UILabel title;
	public UILabel message;
	public UILabel cancelButton;
	public UILabel okButton;
	public EventDelegate StandardFunction;
	bool okSet = false;
	bool cancelSet = false;

	public void Awake ()
	{
		StandardFunction = new EventDelegate (this, "DestroySelf");
	}

	public void Start ()
	{
		if (!cancelSet)
			cancelButton.gameObject.SetActive (false);
		if (!okSet)
			okButton.gameObject.SetActive (false);
		

	}

	public void SetTitle (string desiredTitle)
	{
		title.text = desiredTitle;
	}

	public void SetMessage (string desiredMessage)
	{
		message.text = desiredMessage;
	}

	public void SetCancelButton (EventDelegate cancelFunction)
	{
		cancelButton.gameObject.SetActive (true);
		cancelButton.GetComponent<UIButton> ().onClick.Add (cancelFunction);
		cancelSet = true;
	}

	public void SetOkButton (EventDelegate okFunction)
	{
		okButton.gameObject.SetActive (true);
		okButton.GetComponent<UIButton> ().onClick.Add (okFunction);
		okSet = true;

	}

	public void DestroySelf ()
	{
		Destroy (gameObject);
	}
		
}

