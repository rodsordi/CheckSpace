﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupsManager : MonoBehaviour
{

	public static PopupsManager Instance;
	public List<GameObject> openPopups;

	void Start ()
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);
	}

	public void ShowPopupMessage (EventDelegate cancelFunction, EventDelegate confirmFunction, string title = "", string message = "")
	{
		GameObject popup = NGUITools.AddChild (gameObject, Resources.Load <GameObject> ("Popup"));
		Popup pUp = popup.GetComponent <Popup> ();
		pUp.SetCancelButton (cancelFunction);
		pUp.SetOkButton (confirmFunction);
		pUp.SetTitle (title);
		pUp.SetMessage (message);
		openPopups.Add (popup);
	}

	public void ShowPopupMessage (EventDelegate confirmFunction, string title = "", string message = "")
	{
		GameObject popup = NGUITools.AddChild (gameObject, Resources.Load <GameObject> ("Popup"));
		Popup pUp = popup.GetComponent <Popup> ();
		pUp.SetOkButton (confirmFunction);
		pUp.SetTitle (title);
		pUp.SetMessage (message);
		openPopups.Add (popup);
	}

	public void ShowPopupMessage (string title = "", string message = "")
	{
		GameObject popup = NGUITools.AddChild (gameObject, Resources.Load <GameObject> ("Popup"));
		Popup pUp = popup.GetComponent <Popup> ();
		pUp.SetOkButton (pUp.StandardFunction);
		pUp.SetTitle (title);
		pUp.SetMessage (message);
		openPopups.Add (popup);
	}


}
