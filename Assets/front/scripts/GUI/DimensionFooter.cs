﻿using UnityEngine;
using System.Collections;

public class DimensionFooter : MonoBehaviour {

	public UILabel labelTotal;

	public void SetTotal(float total)
	{
		labelTotal.text = total.ToString();
	}
}
