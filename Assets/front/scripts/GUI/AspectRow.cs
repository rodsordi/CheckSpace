﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AspectRow : MonoBehaviour
{
	public UITable tableAspects;
	public GameObject aspectPrefab;
	public GameObject aspectHeaderPrefab;

	public GUICategory categoryObject;

	public List<AspectList> aspectsList;
	public List<ImagemIndicador> evidenceList;
	[HideInInspector]
	public List<Aspect> aspects = new List<Aspect> ();

	[HideInInspector]
	public List<Indicador> indicators;

	public string ambiente;
	public string servico;

	public void Start ()
	{		
		for (int i = 0; i < aspectsList.Count; i++) {
			GameObject header = NGUITools.AddChild (tableAspects.gameObject, aspectHeaderPrefab);
			header.name = aspectsList [i].header;
			header.GetComponent<LabelName> ().SetName (aspectsList [i].header);

			for (int j = 0; j < aspectsList [i].aspectNames.Count; j++) {
				GameObject go = NGUITools.AddChild (tableAspects.gameObject, aspectPrefab);
				go.name = (j + 1).ToString () + " - " + aspectsList [i].aspectNames [j];
				aspects.Add (go.GetComponent<Aspect> ());
				int current = aspects.Count - 1;
				aspects [current].name = go.name;
				aspects [current].indicator = indicators [current];
				aspects [current].labelName.text = go.name;
				aspects [current].toggleApproved.group = CheckSpaceManager.instance.aspectSeedGroup;
				aspects [current].toggleRepproved.group = CheckSpaceManager.instance.aspectSeedGroup;
				aspects [current].toggleNA.group = CheckSpaceManager.instance.aspectSeedGroup;
				aspects [current].categoryObject = categoryObject;

				aspects [current].evidency = evidenceList.Where(ev => (ev != null && ev.indicador != null)?
				                                                (ev.indicador == indicators [current]) : false).ToList()[0];

			
				int ind = indicators [current].indicador;

				if (ind == 1) {
					aspects [current].toggleApproved.value = true;
				} else if (ind == 2) {
					aspects [current].toggleRepproved.value = true;
				} else {
					aspects [current].toggleNA.value = true;
				}
				CheckSpaceManager.instance.IncreaseSeedGroup ();
			}
		}
	}	

	public void UpdateComments()
	{

	}
}
