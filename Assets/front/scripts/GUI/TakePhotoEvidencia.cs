﻿using UnityEngine;
using System.Collections;
using System.Web;

public class TakePhotoEvidencia : MonoBehaviour
{
	public string photoName;
	public UITexture texture;
	public ImagemIndicador imagemIndicador;
	public string currentEvidenciaName;
	public PhotosWindow photosWindow;

	void Start ()
	{
		photosWindow = CheckSpaceManager.instance.photosWindow;
	}

	public void AddEventListeners ()
	{
		EtceteraAndroidManager.photoChooserSucceededEvent += OnPhotoSucceed;
		EtceteraAndroidManager.photoChooserCancelledEvent += OnPhotoCancel;
	}

	public void RemoveEventListeners ()
	{
		EtceteraAndroidManager.photoChooserSucceededEvent -= OnPhotoSucceed;
		EtceteraAndroidManager.photoChooserCancelledEvent -= OnPhotoCancel;
	}

	void OnClick ()
	{
		EtceteraAndroid.promptToTakePhoto (photoName);
	}

	public void OnPhotoSucceed (string pName)
	{
		EtceteraAndroid.scaleImageAtPath (pName, 0.5f);
		texture.mainTexture = EtceteraAndroid.textureFromFileAtPath (pName);
	
		Texture2D tex2d = (Texture2D)texture.mainTexture;
		byte[] imgBytes = tex2d.EncodeToJPG ();

		photosWindow.evidence.imagem = imgBytes;

		if (StaticData.OnlineVistoria) {
			string photoName = "Foto_Evidencia_";
			photoName += photosWindow.evidence.indicador.nome + "_" + photosWindow.evidence.indicador.grupo
			+ "_" + photosWindow.evidence.indicador.subGrupo;
			string encoding = HttpUtility.UrlEncode (photosWindow.evidence.imagem);

			photosWindow.evidence.imagemUrl = CheckSpaceManager.instance.server.saveImage ("Vistoria_" + StaticData.vistoriaAtual.id.ToString () + ";" + photoName + ";" + encoding);
			Debug.Log ("image saved in the server at " + photosWindow.evidence.imagemUrl);
		}

	}

	public void OnPhotoCancel ()
	{

	}

	public void OnDestroy ()
	{
		RemoveEventListeners ();
	}
	
}
