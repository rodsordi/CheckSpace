﻿using UnityEngine;
using System.Collections;

public static class StaticStrings
{

	public static string Sujo = "sujo";
	public static string Desalinhado = "desalinhado";
	public static string ComFolga = "com folga";
	public static string Quebrado = "quebrado";
	public static string Trincado = "trincado";
	public static string Riscado = "riscado";
	public static string Manchado = "manchado";
	public static string Oxidado = "com mancha de oxidaçao";
	public static string Desnivelado = "desnivelado";
	public static string Alto = "alto";
	public static string Baixo = "baixo";
	public static string FalhaRejunte = "falha no rejunte";
	public static string SemRejunte = "sem rejunte";
	public static string ManchaRejunte = "rejunte manchado";
	public static string MalFixado = "mal fixado";
	public static string NaoFixado = "nao fixado";

}
