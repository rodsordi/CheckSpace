﻿using UnityEngine;
using System.Collections;

public class SwipeChecker : MonoBehaviour {

	bool touching = false;
	float deltaX = 0;
	float startingPosX = 0;
	float lastPosX = 0;
	float deltaY = 0;
	float startingPosY = 0;
	float lastPosY = 0;

	public float swipeAmount = -200;
	public CheckSpaceManager checkSpaceManager;

	void Start () {
	
	}
	

	void Update () 
	{
		if(Input.GetMouseButtonDown(0))
		{
			startingPosX = Input.mousePosition.x;
			startingPosY = Input.mousePosition.y;
			touching = true;
		}
		else if(Input.GetMouseButton(0))
		{
			lastPosX = Input.mousePosition.x;
			lastPosY = Input.mousePosition.y;
		}
		else
		{
			if(touching)
			{
				deltaX = startingPosX - lastPosX;
				deltaY = Mathf.Abs((int) startingPosY - lastPosY);

				if(deltaX < swipeAmount && deltaY < 50)
					checkSpaceManager.ShowMenu();
			
				touching = false;
			}
		}
	}
}
