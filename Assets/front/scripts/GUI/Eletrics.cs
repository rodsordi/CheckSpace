﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Eletrics : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject rowPrefabI;
	public GameObject rowPrefabII;
	public GameObject rowPrefabIII;
	public GameObject rowPrefabIV;
	public GameObject rowPrefabV;
	
	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	public Ambient selectedAmbients;

	UIToggle toggleVerify;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.Eletrics;

		serviceTags = new string[] {"Torneira vazando", "Nao abre", "Pouco fluxo de água", "Duchas vazando / registro não abre /", "registro não fecha / saída com plug", "Pouco fluxo de água", "Registro não abre / Registro não fecha / ", "Registro longe da parede",
			"Sem acabamento", "Pressão baixa", "Cifão vazando / flexível vazando", "Quebrada / Suja / mancha de oxidação", "Sem caimento", 
			"Mal Fixada", "Riscada", "Trincada", "Manchada", "Desalinhada", "Nao fixada",
			"Sem escoamento / entopido", "Riscadas / manchadas / trincadas", "não fixadas / mal fiadas", "com desnível / baixa/ alta", "vazando / baixo fluxo", "riscada / amassada/ manchada", "sem tampa", "solta", "suja / manchada / trincada", "vazando", "apresenta vazamento no apartamento inferior",
			"Sujo", "Quebrado", "Riscado", "Manchado", "Suja",
			"Desnivelada", "Alta", "Baixa", "Falha no Rejunte", "Sem Rejunte", 
			"Mal Fixada", "Riscada", "Trincada", "Manchada", "Desalinhada", "Nao fixada"
		};
	}

	public override void UpdateComments ()
	{
		string comment = "";

		for (int i = 0; i < rows.Count; i++) {

			foreach (var item in rows [i].GetComponent <AspectRow> ().indicators) {
				if (item.indicador == 2) {
					if (comment != "")
						comment += ", ";
					comment += item.nome;
				}
			}
		}
			
		CheckSpaceManager.instance.selectedAmbient.eletricsInformation.service.tag = comment;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		rows.Clear ();
		
		ForegroundMenus.instace.SetBottomTitle ("Instalações Elétricas");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = category;

		GameObject header2 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header2.GetComponent<LabelName> ().SetName ("I - Espelhos");
		
		GameObject goI = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabI);
		goI.name = CheckSpaceManager.instance.selectedAmbient.name + " Espelhos";
		
		GameObject header3 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header3.GetComponent<LabelName> ().SetName ("II - Cablagem");
		
		GameObject goII = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabII);
		goII.name = CheckSpaceManager.instance.selectedAmbient.name + " Cablagem";
		
		GameObject header4 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header4.GetComponent<LabelName> ().SetName ("III - Interruptores");
		
		GameObject goIII = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabIII);
		goIII.name = CheckSpaceManager.instance.selectedAmbient.name + " Interruptores";
		
		GameObject header5 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header5.GetComponent<LabelName> ().SetName ("IV - Quadros de Distribuição");
		
		GameObject goIV = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabIV);
		goIV.name = CheckSpaceManager.instance.selectedAmbient.name + " Quadros de Distribuição";
		
		GameObject header6 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header6.GetComponent<LabelName> ().SetName ("V - Pontos de Iluminação");
		
		GameObject goV = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabV);
		goV.name = CheckSpaceManager.instance.selectedAmbient.name + " Pontos de Iluminação";

		rows.Add (goI);
		rows.Add (goII);
		rows.Add (goIII);
		rows.Add (goIV);
		rows.Add (goV);

		for (int i = 0; i < rows.Count; i++) {
			rows [i].GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.eletricsInformation.indicatorsList [i];
			rows [i].GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.eletricsInformation.evidencias [i];
			rows [i].GetComponent <AspectRow> ().ambiente = CheckSpaceManager.instance.selectedAmbient.name;
			rows [i].GetComponent <AspectRow> ().servico = CheckSpaceConstants.CheckSpaceCategories.Eletrics.ToString ();
			rows [i].GetComponent <AspectRow> ().categoryObject = this;
		}

		serviceTags = CheckSpaceManager.instance.selectedAmbient.eletricsInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.eletricsInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.eletricsInformation.verified = toggleVerify.value;
		Debug.Log (CheckSpaceManager.instance.selectedAmbient.eletricsInformation.verified);
	}
}
