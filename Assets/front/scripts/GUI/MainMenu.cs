using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour
{
	public List<string> categories = new List<string> ();
	public List<CheckSpaceConstants.CheckSpaceCategories> categoryTypes = new List<CheckSpaceConstants.CheckSpaceCategories> ();
	public GameObject categoryButtonPrefab;
	public UITable tableCategories;
	public UIScrollView scrollMainMenu;

	void Start ()
	{
		//RecreateMenu ();
	}

	public void RecreateMenu ()
	{
		for (int i = 0; i < tableCategories.transform.childCount; i++) {
			Destroy (tableCategories.transform.GetChild (i).gameObject);
		}

		int count = 0;
		int maxItems = 1;

		if (CheckSpaceManager.instance.createdAmbients.Count > 0)
			maxItems = categories.Count;

		for (int i = 0; i < maxItems; i++) {				
			GameObject go = NGUITools.AddChild (tableCategories.gameObject, categoryButtonPrefab);
			go.name = categories [i];
			go.GetComponent<CategoryButton> ().category = categoryTypes [count];
			count++;
			if (VerifyAmbientsCategory (categoryTypes [i])) {
				if (i == 0)
					go.GetComponent<CategoryButton> ().spriteVerified.color = Color.white;
				else
					go.GetComponent<CategoryButton> ().spriteVerified.color = Color.green;
			} else {
				if (i == 0)
					go.GetComponent<CategoryButton> ().spriteVerified.color = Color.white;
				else
					go.GetComponent<CategoryButton> ().spriteVerified.color = Color.red;
			}
		}

		tableCategories.repositionNow = true;

		scrollMainMenu.ResetPosition ();
	}

	/*public bool AllVerified ()
	{
		int maxItems = 1;

		if (CheckSpaceManager.instance.createdAmbients.Count > 0)
			maxItems = categories.Count;
		if (maxItems > 1) {
			for (int i = 1; i < maxItems; i++) {
				
			}
		} else
			return false;
	}*/

	public bool VerifyAmbientsCategory (CheckSpaceConstants.CheckSpaceCategories category)
	{
		switch (category) {

		case CheckSpaceConstants.CheckSpaceCategories.Accoustic:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].accousticInformation.verified)
					return false;
			}

			return true;

			break;

		case CheckSpaceConstants.CheckSpaceCategories.Ceramic:

			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].ceramicInformation.verified)
					return false;
			}

			return true;

			break;

		case CheckSpaceConstants.CheckSpaceCategories.CeramicWall:

			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].ceramicWallsInformation.verified)
					return false;
			}

			return true;

			break;

		case CheckSpaceConstants.CheckSpaceCategories.Dimensions:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].dimensionsInformation.verified)
					return false;
			}

			return true;

			break;

		case CheckSpaceConstants.CheckSpaceCategories.Eletrics:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].eletricsInformation.verified)
					return false;
			}

			return true;

			break;

		case CheckSpaceConstants.CheckSpaceCategories.Frames:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].framesInformation.verified)
					return false;
			}

			return true;
		
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Hidraulics:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].hidraulicsInformation.verified)
					return false;
			}

			return true;
		
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingCeiling:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].paintingCeilingInformation.verified)
					return false;
			}

			return true;

			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingWall:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].paintingWallInformation.verified)
					return false;
			}
				
			return true;
			
			break;

		case CheckSpaceConstants.CheckSpaceCategories.WoodenFloor:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].woodenFloorInformation.verified)
					return false;
			}

			return true;
		
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Sills:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].sillsInformation.verified)
					return false;
			}

			return true;

			break;

		case CheckSpaceConstants.CheckSpaceCategories.Peitoris:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].peitorisInformation.verified)
					return false;
			}
			
			return true;
			
			break;

		case CheckSpaceConstants.CheckSpaceCategories.TentosBaguetes:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].tentosBaguetesInformation.verified)
					return false;
			}
			
			return true;
			
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalCeiling:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].functionalCeilingInformation.verified)
					return false;
			}
			
			return true;
			
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalWall:
			for (int i = 0; i < CheckSpaceManager.instance.createdAmbients.Count; i++) {
				if (!CheckSpaceManager.instance.createdAmbients [i].functionalWallInformation.verified)
					return false;
			}
			
			return true;
			
			break;

		}

		return false;
	}
}
