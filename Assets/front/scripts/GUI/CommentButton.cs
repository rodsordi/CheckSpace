using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommentButton : MonoBehaviour
{
	public CheckSpaceConstants.CheckSpaceCategories category;
	

	CommentsWindowSmall commentPanel;

	void Start ()
	{
		commentPanel = CheckSpaceManager.instance.commentsWindowSmall;
	}

	void OnClick ()
	{

		commentPanel.category = category;

		switch (category) {

		case CheckSpaceConstants.CheckSpaceCategories.Ceramic:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.ceramicInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.ceramicInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.CeramicWall:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.service.tag);
			break;
					
		case CheckSpaceConstants.CheckSpaceCategories.Eletrics:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.eletricsInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.eletricsInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Frames:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.framesInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.framesInformation.service.tag);
			break;
			
		case CheckSpaceConstants.CheckSpaceCategories.Hidraulics:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.hidraulicsInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.hidraulicsInformation.service.tag);
			break;
			
		case CheckSpaceConstants.CheckSpaceCategories.PaintingCeiling:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingWall:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.paintingWallInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.paintingWallInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Sills:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.sillsInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.sillsInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Peitoris:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.peitorisInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.peitorisInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.TentosBaguetes:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.tentosBaguetesInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.tentosBaguetesInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.WoodenFloor:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.woodenFloorInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalCeiling:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.functionalCeilingInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.functionalCeilingInformation.service.tag);
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalWall:
			commentPanel.SetComment (CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.service.comentario, CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.service.tag);
			break;

		}
	}




}
