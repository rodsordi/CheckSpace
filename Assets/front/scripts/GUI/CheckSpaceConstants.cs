﻿using UnityEngine;
using System.Collections;

public static class CheckSpaceConstants {
	public enum CheckSpaceCategories
	{
		Home,
		Accoustic,
		Dimensions,
		Ceramic,
		CeramicWall,
		Sills,
		Peitoris,
		TentosBaguetes,
		Frames,
		WoodenFloor,
		PaintingCeiling,
		PaintingWall,
		FunctionalCeiling,
		FunctionalWall,
		Eletrics,
		Hidraulics,
	}


	public const string AccousticVerified = "AccousticVerifiedVID";
	public const string CeramicVerified = "CeramicVerifiedVID";
	public const string DimensionVerified = "DimensionVerifiedVID";
	public const string EletricsVerified = "EletricsVerifiedVID";
	public const string FramesVerified = "FramesVerifiedVID";
	public const string HidraulicsVerified = "HidraulicsVerifiedVID";
	public const string PaintingWallVerified = "PaintingWallVerifiedVID";
	public const string PaintingCeilingVerified = "PaintingCeilingVerifiedVID";
	public const string SillsVerified = "SillsVerifiedVID";
	public const string PeitorisVerified = "PeitorisVerifiedVID";
	public const string TentosBaguetesVerified = "TentosBaguetesVerifiedVID";
	public const string WoodenFloorVerified = "WoodeFloorVerifiedVID";
	public const string FunctionalWallVerified = "FunctionalWallVerifiedVID";
	public const string FunctionalCeilingVerified = "FunctionalCeilingVerifiedVID";

}
