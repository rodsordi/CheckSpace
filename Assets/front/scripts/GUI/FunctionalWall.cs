﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FunctionalWall : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject rowPrefabI;
	
	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	public Ambient selectedAmbients;

	UIToggle toggleVerify;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.FunctionalWall;
	}

	public override void UpdateComments ()
	{
		string comment = "";
		
		for (int i = 0; i < rows.Count; i++) {
			
			foreach (var item in rows [i].GetComponent <AspectRow> ().indicators) {
				if (item.indicador == 2) {
					if (comment != "")
						comment += ", ";
					comment += item.nome;
				}
			}
		}

		CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.service.tag = comment;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		rows.Clear ();
		
		ForegroundMenus.instace.SetBottomTitle ("Funcional Parede");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = CheckSpaceConstants.CheckSpaceCategories.FunctionalWall;

		GameObject header2 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header2.GetComponent<LabelName> ().SetName ("IV - Funcional Parede");
		
		GameObject goI = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabI);
		goI.name = CheckSpaceManager.instance.selectedAmbient.name + " Funcionamento";

		rows.Add (goI);


		for (int i = 0; i < rows.Count; i++) {
			rows [i].GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.indicatorsList [i];
			rows [i].GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.evidencias [i];
			rows [i].GetComponent <AspectRow> ().ambiente = CheckSpaceManager.instance.selectedAmbient.name;
			rows [i].GetComponent <AspectRow> ().servico = Servicos.FUNCIONAL_PAREDE.ToString ();
			rows [i].GetComponent <AspectRow> ().categoryObject = this;
		}

		serviceTags = CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.functionalWallInformation.verified = toggleVerify.value;
	}
}
