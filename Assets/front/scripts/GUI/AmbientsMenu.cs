using UnityEngine;
using System.Collections;

public class AmbientsMenu : MonoBehaviour
{

	public GameObject ambientButtonPrefab;
	public UITable tableAmbients;
	public OverviewReportScreen overViewScreen;
	//public GameObject ambientForegroundMenu;
	public string currentTitle;

	public void FillAmbientList ()
	{
		tableAmbients.sorting = UITable.Sorting.None;
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		foreach (var item in CheckSpaceManager.instance.createdAmbients) {
			GameObject go = NGUITools.AddChild (tableAmbients.gameObject, ambientButtonPrefab);
			go.name = item.name;
			AmbientButton ambButton = go.GetComponent<AmbientButton> ();

			ambButton.category = CheckSpaceManager.instance.selectedCategory;
			ambButton.ambient = item;

			if (CheckAmbientCategoryVerified (ambButton.ambient, ambButton.category))
				ambButton.spriteVistoria.color = Color.green;
			else
				ambButton.spriteVistoria.color = Color.red;
		}

		if (CheckSpaceManager.instance.selectedCategory == CheckSpaceConstants.CheckSpaceCategories.Dimensions) {
			GameObject lbl = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("AreaTotalLabel"));
			lbl.GetComponent <UILabel> ().text = "Área total: " + CheckSpaceManager.instance.totalArea.ToString ();
		}

		tableAmbients.repositionNow = true;
		Invoke ("ResetPos", 0.1f);
	}

	void ResetPos ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void HideAll ()
	{
		gameObject.SetActive (false);
	}

	public void ShowAll ()
	{
		CheckSpaceManager.instance.HideAll ();
		gameObject.SetActive (true);
		FillAmbientList ();
		ForegroundMenus.instace.SetMenuTitle (currentTitle);
		ForegroundMenus.instace.ShowBottomTitle (false);
		ResetPos ();
	}

	public bool CheckAmbientCategoryVerified (Ambient ambient, CheckSpaceConstants.CheckSpaceCategories category)
	{
		switch (category) {

		case CheckSpaceConstants.CheckSpaceCategories.Accoustic:
			return ambient.accousticInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Ceramic:
			return ambient.ceramicInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.CeramicWall:
			return ambient.ceramicWallsInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Dimensions:
			return ambient.dimensionsInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Eletrics:
			return ambient.eletricsInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Frames:
			return ambient.framesInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Hidraulics:
			return ambient.hidraulicsInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingCeiling:
			return ambient.paintingCeilingInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingWall:
			return ambient.paintingWallInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.WoodenFloor:
			return ambient.woodenFloorInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Sills:
			return ambient.sillsInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Peitoris:
			return ambient.peitorisInformation.verified;
			break;

		case CheckSpaceConstants.CheckSpaceCategories.TentosBaguetes:
			return ambient.tentosBaguetesInformation.verified;
			break;


		case CheckSpaceConstants.CheckSpaceCategories.FunctionalCeiling:
			return ambient.functionalCeilingInformation.verified;
			break;


		case CheckSpaceConstants.CheckSpaceCategories.FunctionalWall:
			return ambient.functionalWallInformation.verified;
			break;

		}

		return false;
	}
}
