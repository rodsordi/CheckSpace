﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Web;

public class PhotosWindow : MonoBehaviour
{
	public UILabel commentLabel;
	public UIInput commentInput;
	public UILabel tagsLabel;
	public UITable tableCommentCheckboxes;
	public List<string> availableComments;
	public GameObject prefabComments;

	public int commentCount = 0;
	public string completeComment;
	public string [] result;
	public EventDelegate callback;

	public UIScrollView scrollComments;
	public UIScrollView scrollTags;
	public UIScrollView scrollCheckboxes;
	public GameObject panelPhotoImage;
	public GameObject panelComments;
	public ImagemIndicador evidence;
	public UITexture currentPhoto;
	List<CommentCheckbox> commentCheckBoxesList;

	void Start ()
	{
		tagsLabel.text = string.Empty;
	}

	public void ClickPhotoButton()
	{
		panelPhotoImage.SetActive(true);
		panelComments.SetActive(false);
	}

	public void ClickCommentsButton()
	{
		panelPhotoImage.SetActive(false);
		panelComments.SetActive(true);
	}

	public void ClickCloseButton()
	{

		evidence.comentario = commentLabel.text;
		evidence.tag = tagsLabel.text;
		commentLabel.text = "";
		tagsLabel.text = "";
		ClickPhotoButton();
		currentPhoto.GetComponentInChildren<TakePhotoEvidencia>().RemoveEventListeners();
		gameObject.SetActive(false);

	}

	public void SetEvidence (ImagemIndicador currentEvidence)
	{
		evidence = currentEvidence;
		LoadPhotoTexture();
		commentLabel.text = evidence.comentario;
		tagsLabel.text = evidence.tag;
		currentPhoto.GetComponentInChildren<TakePhotoEvidencia>().AddEventListeners();
	}

	void LoadPhotoTexture()
	{
		if(StaticData.OnlineVistoria)
		{

			if(evidence.imagem != null)
			{
				Texture2D tex2d = new Texture2D(2,2);
				if(tex2d.LoadImage(evidence.imagem))
					currentPhoto.mainTexture = tex2d;
			}
			else if(!string.IsNullOrEmpty(evidence.imagemUrl))
			{
				string s = CheckSpaceManager.instance.server.getImage(evidence.imagemUrl);
				byte[] decode = HttpUtility.UrlDecodeToBytes(s);
				Texture2D t2d = new Texture2D(2,2);
				if(t2d.LoadImage (decode))
					currentPhoto.mainTexture = t2d;
			}
			else
				currentPhoto.mainTexture = null;
		}
		else
		{
			if(evidence.imagem != null)
			{
				Texture2D tex2d = new Texture2D(2,2);
				if(tex2d.LoadImage(evidence.imagem))
					currentPhoto.mainTexture = tex2d;
			}
			else
				currentPhoto.mainTexture = null;
		}
	}

	public void FillComments (string[] allComments)
	{
		ClickCommentsButton();

		foreach (var item in tableCommentCheckboxes.GetChildList()) {
			Destroy (item.gameObject);
		}

		scrollCheckboxes.ResetPosition();
		tableCommentCheckboxes.Reposition ();
		commentCheckBoxesList = new List<CommentCheckbox>();
		for (int i = 0; i < allComments.Length; i++) {
			GameObject checkbox = NGUITools.AddChild (tableCommentCheckboxes.gameObject, prefabComments);
			CommentCheckbox commentCheckbox = checkbox.GetComponent<CommentCheckbox> ();
			commentCheckBoxesList.Add(commentCheckbox);
			commentCheckbox.SetComment (allComments [i]);
			commentCheckbox.commentsWindow = this;
			commentCheckbox.SetToggleChange ();
		}

		tableCommentCheckboxes.Reposition ();
		scrollCheckboxes.ResetPosition();
		scrollComments.ResetPosition();
		scrollTags.ResetPosition();

		ClickPhotoButton();
	}


	public void LoadText()
	{
		commentLabel.text = "";
		commentInput.value = "";
		commentLabel.text = evidence.comentario;
		commentInput.value = commentLabel.text;
	}

	public void LoadTags()
	{
		tagsLabel.text = evidence.tag;
		completeComment = tagsLabel.text;
		string[] splitWords = tagsLabel.text.Split(',');
		commentCount = 0;

		for (int i = 0; i < splitWords.Length; i++) {
			if(splitWords[i].StartsWith(" "))
			splitWords[i] = splitWords[i].Substring(1);
		}

		foreach (var item in splitWords) {
			foreach (var box in commentCheckBoxesList) {
				if(item == box.comment)
				{
					box.toggle.value = true;
				}
			}
		}
	}

	public void Close ()
	{
		gameObject.SetActive (false);	
		result = new string[] {commentLabel.text, completeComment};
		callback.Execute();
		commentLabel.text = "";
		completeComment = "";
	}

	public void Open ()
	{
		gameObject.SetActive (true);
		if(evidence != null && evidence.imagem != null)
		{
			Texture2D tex = new Texture2D(2,2);
			if(tex.LoadImage(evidence.imagem))
			currentPhoto.mainTexture = tex;
		}
	}

	public void CommentToggleChange (CommentCheckbox checkBox)
	{
		if (checkBox.toggle.value)
			AddWord (checkBox.comment);
		else {
			if (commentCount > 0)
				RemoveWord (checkBox.comment);
		}
		tagsLabel.text = completeComment;
		evidence.tag = tagsLabel.text;
	}

	public void AddWord (string word)
	{
		if (commentCount > 0)
			completeComment += ", " + word;
		else
			completeComment = word;
		commentCount++;

		evidence.tag = completeComment;
	}

	public void RemoveWord (string word)
	{
		if (tagsLabel.text.Contains (word)) {
			string searchString;
			if (commentCount > 1)
				searchString = ", " + word;
			else
				searchString = word;

			int index = tagsLabel.text.IndexOf (searchString);

			if (index != -1) {
				completeComment = completeComment.Replace (searchString, string.Empty);
			} else {
				if (commentCount > 1)
					completeComment = completeComment.Remove (0, word.Length + 2);
				else
					completeComment = completeComment.Remove (0, word.Length);
			}
			commentCount--;

			evidence.tag = completeComment;
		}
	}
}
