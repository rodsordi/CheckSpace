﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Peitoris : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject rowPrefab;
	
	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	public Ambient selectedAmbients;

	UIToggle toggleVerify;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.Peitoris;
	}

	public override void UpdateComments ()
	{
		string comment = "";
		
		for (int i = 0; i < rows.Count; i++) {
			
			foreach (var item in rows [i].GetComponent <AspectRow> ().indicators) {
				if (item.indicador == 2) {
					if (comment != "")
						comment += ", ";
					comment += item.nome;
				}
			}
		}

		CheckSpaceManager.instance.selectedAmbient.peitorisInformation.service.tag = comment;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		rows.Clear ();
		
		ForegroundMenus.instace.SetBottomTitle ("Peitoris");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = category;

		GameObject goFloor = NGUITools.AddChild (tableAmbients.gameObject, rowPrefab);
		goFloor.name = CheckSpaceManager.instance.selectedAmbient.name + " floor";
	
		rows.Add (goFloor);

		for (int i = 0; i < rows.Count; i++) {
			rows [i].GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.peitorisInformation.indicatorsList [i];
			rows [i].GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.peitorisInformation.evidencias [i];
			rows [i].GetComponent <AspectRow> ().ambiente = CheckSpaceManager.instance.selectedAmbient.name;
			rows [i].GetComponent <AspectRow> ().servico = Servicos.PEITORIS.ToString ();
			rows [i].GetComponent <AspectRow> ().categoryObject = this;
		}

		serviceTags = CheckSpaceManager.instance.selectedAmbient.peitorisInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.peitorisInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.peitorisInformation.verified = toggleVerify.value;
		Debug.Log (CheckSpaceManager.instance.selectedAmbient.peitorisInformation.verified);
	}
}
