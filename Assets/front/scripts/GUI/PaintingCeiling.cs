using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PaintingCeiling : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject rowPrefabI;
	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	public Ambient selectedAmbients;

	UIToggle toggleVerify;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.PaintingCeiling;
	}

	public override void UpdateComments ()
	{
		string comment = "";
		
		for (int i = 0; i < rows.Count; i++) {
			
			foreach (var item in rows [i].GetComponent <AspectRow> ().indicators) {
				if (item.indicador == 2) {
					if (comment != "")
						comment += ", ";
					comment += item.nome;
				}
			}
		}

		CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.service.tag = comment;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		rows.Clear ();
		
		ForegroundMenus.instace.SetBottomTitle ("Pintura Teto");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = category;

		GameObject header2 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header2.GetComponent<LabelName> ().SetName ("II - Teto/Pintura");
		
		GameObject goI = NGUITools.AddChild (tableAmbients.gameObject, rowPrefabI);
		goI.name = CheckSpaceManager.instance.selectedAmbient.name + " Teto/ Pintura";

		rows.Add (goI);

		for (int i = 0; i < rows.Count; i++) {
			rows [i].GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.indicatorsList [i];
			rows [i].GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.evidencias [i];
			rows [i].GetComponent <AspectRow> ().ambiente = CheckSpaceManager.instance.selectedAmbient.name;
			rows [i].GetComponent <AspectRow> ().servico = Servicos.PINTURA_TETO.ToString ();
			rows [i].GetComponent <AspectRow> ().categoryObject = this;
		}

		serviceTags = CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.paintingCeilingInformation.verified = toggleVerify.value;
	}
}
