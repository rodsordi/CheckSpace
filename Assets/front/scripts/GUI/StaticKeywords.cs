﻿using UnityEngine;
using System.Collections;

public static class StaticKeywords {

	public static string Umidade = "Umidade junto ao ponto";
	public static string Infiltracao = "Infiltração nos Frontões";
	public static string UmidadeFuro = "Umidade no furo da bancada";
	public static string UmidadeShafts = "Umidade na base do Shafts";
	public static string UmidadeForroRalo = "Umidade no forro e ralos";
	public static string Ralo = "Ralo";
	public static string Loucas = "Louças";
	public static string Cubas = "Cubas";
	public static string Registro = "Registros";
	public static string Bancada = "Bancada";
	public static string CaixaAcoplada = "Caixa Acoplada";
	public static string Torneiras = "Torneiras";
	public static string Metais = "Metais";
	public static string Pedra = "Pedra";

}
