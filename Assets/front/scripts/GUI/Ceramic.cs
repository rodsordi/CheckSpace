﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ceramic : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject floorRowPrefab;
	public GameObject subfloorRowPrefab;

	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;

	UIToggle toggleVerify;
	GameObject goFloor;
	GameObject goSubfloor;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.Ceramic;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}

		rows.Clear ();

		ForegroundMenus.instace.SetBottomTitle ("Revestimento Piso");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = CheckSpaceConstants.CheckSpaceCategories.Ceramic;

		GameObject header2 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header2.GetComponent<LabelName> ().SetName ("I - Piso");

		goFloor = NGUITools.AddChild (tableAmbients.gameObject, floorRowPrefab);
		goFloor.name = CheckSpaceManager.instance.selectedAmbient.name + " floor";

		GameObject header3 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header3.GetComponent<LabelName> ().SetName ("II - Contrapiso");

		goSubfloor = NGUITools.AddChild (tableAmbients.gameObject, subfloorRowPrefab);
		goSubfloor.name = CheckSpaceManager.instance.selectedAmbient.name + " subfloor";

		rows.Add (goFloor);
		goFloor.GetComponent <AspectRow> ().categoryObject = this;
		goFloor.GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.ceramicInformation.indicatorsList [0];
		goFloor.GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.ceramicInformation.evidencias [0];

		rows.Add (goSubfloor);
		goSubfloor.GetComponent <AspectRow> ().categoryObject = this;
		goSubfloor.GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.ceramicInformation.indicatorsList [1];
		goSubfloor.GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.ceramicInformation.evidencias [1];

		serviceTags = CheckSpaceManager.instance.selectedAmbient.ceramicInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	public override void UpdateComments ()
	{
		string comment = "";
		foreach (var item in goFloor.GetComponent <AspectRow> ().indicators) {
			if (item.indicador == 2) {
				if (comment != "")
					comment += ", ";
				comment += item.nome;
			}
		}

		foreach (var item in goSubfloor.GetComponent <AspectRow> ().indicators) {
			if (item.indicador == 2) {
				if (comment != "")
					comment += ", ";
				comment += item.nome;
			}
		}
			
		CheckSpaceManager.instance.selectedAmbient.ceramicInformation.service.tag = comment;
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.ceramicInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.ceramicInformation.verified = toggleVerify.value;
	}

}
