﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;

public class CheckSpaceManager : MonoBehaviour
{

	public OverviewReportScreen relatorioVistoria;
	public CheckSpaceConstants.CheckSpaceCategories selectedCategory;
	public AmbientsMenu ambientMenu;
	public Dimensions dimensionsMenu;
	public AccousticTerm accousticMenu;

	public Ceramic ceramicMenu;
	public CeramicWall ceramicWallMenu;

	public Eletrics eletricsMenu;
	public Frames framesMenu;
	public PaintingWall paintingWallMenu;
	public PaintingCeiling paintingCeilingMenu;
	public Sills sillsMenu;
	public Peitoris peitorisMenu;
	public TentosBaguetes tentoBaguetesMenu;
	public WoodenFloor woodenFloorMenu;
	public Hidraulics hidraulicsMenu;
	public FunctionalCeiling functionalCeilingMenu;
	public FunctionalWall functionalWallMenu;
	public static CheckSpaceManager instance;
	public MainMenu menu;
	public CommentsWindow commentsWindow;
	public CommentsWindowSmall commentsWindowSmall;
	public PhotosWindow photosWindow;
	public GameObject menuButtons;

	public List<Ambient> createdAmbients = new List<Ambient> ();
	public Ambient selectedAmbient;
	public int aspectSeedGroup = 1;

	public VistoriaDAO vistoriaDAO = null;
	public ServicoDAO servicoDAO = null;
	public AmbienteDAO ambienteDAO = null;
	public IndicadorDAO indicadorDAO = null;
	public ImagemIndicadorDAO imagemIndicadorDAO = null;

	//public VistoriaWS.VistoriaWSService vistoriaWS;
	public ServerService server;
	public GameObject carregando;
	public GameObject salvando;

	public double totalArea = 0;
	public List<Indicador> allIndicator;
	public List<Ambiente> allAmbients;
	public List<Servico> allServices;
	public List<ImagemIndicador> allImageIndicators;
	public bool initialized = false;
	bool isSaveOnline = false;

	void Awake ()
	{
		initialized = false;
		System.DateTime time = System.DateTime.Now;
		instance = this;

		server = ServerHandler.server;

		if (!StaticData.OnlineVistoria) {
			vistoriaDAO = ServerHandler.vistoriaDAO;
			servicoDAO = ServerHandler.servicoDAO;
			ambienteDAO = ServerHandler.ambienteDAO;
			indicadorDAO = ServerHandler.indicadorDAO;
			imagemIndicadorDAO = ServerHandler.imagemIndicadorDAO;
			LoadOfflineData ();
		} else {
			LoadOnlineData ();
		}
	
	}

	public void SetSendingMessage ()
	{
		salvando.GetComponent<SaveBox> ().SetMessage ("Enviando...");	
	}

	public void SetSavingMessage ()
	{
		salvando.GetComponent<SaveBox> ().SetMessage ("Salvando...");	
	}


	public void LoadOnlineData ()
	{
		string indicadoresJson = server.findIndicadorByVistoria (StaticData.vistoriaAtual.id);
		if (!string.IsNullOrEmpty (indicadoresJson) && !indicadoresJson.StartsWith ("Exception"))
			allIndicator = JsonBehaviour.GetList<Indicador> (indicadoresJson);
		else {
			allIndicator = new List<Indicador> ();
			Debug.Log (indicadoresJson);
		}

		string ambientesJson = server.findAmbienteByVistoria (StaticData.vistoriaAtual.id);
		if (!string.IsNullOrEmpty (ambientesJson) && !ambientesJson.StartsWith ("Exception"))
			allAmbients = JsonBehaviour.GetList<Ambiente> (ambientesJson);
		else {
			allAmbients = new List<Ambiente> ();
			Debug.Log (ambientesJson);
		}

		string servicesJson = server.findServicoByVistoria (StaticData.vistoriaAtual.id);
		if (!string.IsNullOrEmpty (servicesJson) && !servicesJson.StartsWith ("Exception"))
			allServices = JsonBehaviour.GetList<Servico> (servicesJson);
		else {
			allServices = new List<Servico> ();
			Debug.Log (servicesJson);
		}

		string imagemIndicadoresJson = server.findImagemIndicadorByVistoria (StaticData.vistoriaAtual.id);
		if (!string.IsNullOrEmpty (imagemIndicadoresJson) && !imagemIndicadoresJson.StartsWith ("Exception"))
			allImageIndicators = JsonBehaviour.GetList<ImagemIndicador> (imagemIndicadoresJson);
		else {
			allImageIndicators = new List<ImagemIndicador> ();
		}

		foreach (var item in allAmbients) {
			item.vistoria = StaticData.vistoriaAtual;
		}

		foreach (var item in allServices) {
			item.ambiente = FindOnlineAmbientById (item.ambiente.id);
		}

		foreach (var item in allIndicator) {
			item.servico = FindOnlineServiceById (item.servico.id);
		}

		foreach (var item in allImageIndicators) {
			item.indicador = FindOnlineIndicatorById (item.indicador.id);
		}
	}

	public void LoadOfflineData ()
	{
		allIndicator = indicadorDAO.LoadAllIncludingDeleted ();
		allAmbients = ambienteDAO.LoadAllIncludingDeleted ();
		allServices = servicoDAO.LoadAllIncludingDeleted ();
		allImageIndicators = imagemIndicadorDAO.LoadAllIncludingDeleted ();
	}

	void Start ()
	{
		ShowHome ();
	}


	public List<Indicador> FilterIndicator (string nomeServico, string ambientName)
	{
		return CheckSpaceManager.instance.allIndicator.Where (i => 
		                                                     (i.servico != null &&
		i.servico.ambiente != null &&
		i.servico.ambiente.vistoria != null) ? 
		(i.servico.ambiente.vistoria.OID == StaticData.vistoriaAtual.OID
		&& i.servico.ambiente.nome == ambientName
		&& i.servico.nome == nomeServico) : false).ToList ();
	}

	public List<ImagemIndicador> FilterImageIndicator (string nomeServico, string ambientName)
	{
		return CheckSpaceManager.instance.allImageIndicators.Where (i => 
		 (i.indicador != null &&
		i.indicador.servico != null &&
		i.indicador.servico.ambiente != null &&
		i.indicador.servico.ambiente.vistoria != null) ? 
       	(i.indicador.servico.ambiente.vistoria.OID == StaticData.vistoriaAtual.OID
		&& i.indicador.servico.ambiente.nome == ambientName
		&& i.indicador.servico.nome == nomeServico) : false).ToList ();
	}

	public List<Ambiente> FilterAmbients (string nomeAmbiente)
	{
		return CheckSpaceManager.instance.allAmbients.Where
			(i => (i != null &&
		i.vistoria != null) ? 
			 (i.vistoria.OID == StaticData.vistoriaAtual.OID
		&& i.nome == nomeAmbiente) : false).ToList ();
	}

	public List<Servico> FilterServicos (string nomeAmbiente)
	{
		return CheckSpaceManager.instance.allServices.Where
			(i => (i != null &&
		i.ambiente != null &&
		i.ambiente.vistoria != null) ? 
			 (i.ambiente.vistoria.OID == StaticData.vistoriaAtual.OID
		&& i.ambiente.nome == nomeAmbiente) : false).ToList ();
	}
	
	//Online filters
	
	
	public List<Indicador> FindOnlineIndicators (string nomeAmbiente, string nomeServico)
	{
		List<Indicador> indicadores = allIndicator.Where
			(i => (i != null &&
		                              i.servico != null &&
		                              i.servico.ambiente != null &&
		                              i.servico.ambiente.vistoria != null) ? 
			 (i.servico.ambiente.nome == nomeAmbiente
		                              && i.servico.nome == nomeServico) : false).ToList ();

		if (indicadores != null && indicadores.Count > 0)
			return indicadores;
		else
			return null;
	}

	public List<ImagemIndicador> FindOnlineImageIndicators (string nomeAmbiente, string nomeServico)
	{
		List<ImagemIndicador> indicadores = allImageIndicators.Where
			(i => (i != null &&
		                                    i.indicador != null &&
		                                    i.indicador.servico != null &&
		                                    i.indicador.servico.ambiente != null &&
		                                    i.indicador.servico.ambiente.vistoria != null) ? 
			 (i.indicador.servico.ambiente.nome == nomeAmbiente
		                                    && i.indicador.servico.nome == nomeServico) : false).ToList ();
		
		if (indicadores != null && indicadores.Count > 0)
			return indicadores;
		else
			return null;
	}

	public Indicador FindOnlineIndicatorById (int indicatorId)
	{
		List<Indicador> indicadores = allIndicator.Where
			(i => (i != null &&
		                              i.servico != null &&
		                              i.servico.ambiente != null &&
		                              i.servico.ambiente.vistoria != null) ? 
			 (i.id == indicatorId) : false).ToList ();
		
		if (indicadores != null && indicadores.Count > 0)
			return indicadores [0];
		else
			return null;
	}

	public Ambiente FindOnlineAmbient (string ambientName)
	{
		List<Ambiente> ambients = allAmbients.Where
			(i => (i != null &&
		                          i.vistoria != null) ? 
			(i.nome == ambientName) : false).ToList ();

		if (ambients != null && ambients.Count > 0)
			return ambients [0];
		else
			return null;
	}

	public Ambiente FindOnlineAmbientById (int ambientId)
	{
		List<Ambiente> ambients = allAmbients.Where
			(i => (i != null &&
		                          i.vistoria != null) ? 
			 (i.id == ambientId) : false).ToList ();

		if (ambients != null && ambients.Count > 0)
			return ambients [0];
		else
			return null;
	}

	public Servico FindOnlineServicesByAmbientId (string serviceName, int ambientId)
	{
		List<Servico> servicos = allServices.Where
			(i => (i != null &&
		                         i.ambiente != null &&
		                         i.ambiente.vistoria != null) ? 
			 (i.nome == serviceName && i.ambiente.id == ambientId) : false).ToList ();
		
		if (servicos != null && servicos.Count > 0)
			return servicos [0];
		else
			return null;
	}

	public Servico FindOnlineServiceById (int serviceId)
	{
		List<Servico> servicos = allServices.Where
		(i => (i != null &&
		                         i.ambiente != null &&
		                         i.ambiente.vistoria != null) ? 
		 (i.id == serviceId) : false).ToList ();

		if (servicos != null && servicos.Count > 0)
			return servicos [0];
		else
			return null;
	}

	public List<ImagemIndicador> FindOnlineImageIndicator (int indicatorId)
	{
		List<ImagemIndicador> imagemIndicadores = allImageIndicators.Where
			(i => (i != null &&
		                                          i.indicador != null &&
		                                          i.indicador.servico != null) ? 
			 (i.indicador.id == indicatorId) : false).ToList ();
		
		if (imagemIndicadores != null && imagemIndicadores.Count > 0)
			return imagemIndicadores;
		else
			return null;
	}

	public void SaveData ()
	{
		if (StaticData.OnlineVistoria)
		{
			isSaveOnline = true;
			SaveVistoriaNet ();
		}
		else
		{
			isSaveOnline = false;
			SaveVistoriaLocal ();
		}
	}

	public IEnumerator ShowSavingPopupNet ()
	{
		if(isSaveOnline)
			SetSavingMessage ();
		else
			SetSendingMessage ();

		salvando.SetActive (true);
		isSaveOnline = false;
		yield return null;
		FinishNetSave ();
	}

	public IEnumerator ShowSavingPopupLocal ()
	{
		SetSavingMessage ();
		salvando.SetActive (true);
		yield return null;
		FinishLocalSave ();
	}

	public void SaveVistoriaLocal ()
	{
		isSaveOnline = false;
		StartCoroutine (ShowSavingPopupLocal ());
	}

	void FinishLocalSave ()
	{
		vistoriaDAO.SaveOrUpdate (StaticData.vistoriaAtual);
		foreach (var item in createdAmbients) {
			item.SaveAllLocal ();
		}
		salvando.SetActive (false);
		PopupsManager.Instance.ShowPopupMessage ("Confirmação", "A vistoria foi salva no banco local com sucesso.");
	}

	public void SaveVistoriaNet ()
	{
		StartCoroutine (ShowSavingPopupNet ());
	}

	void FinishNetSave ()
	{
		if (ConnectionManager.IsConnected (false)) {
			string jsonVistoria = null;
			string jsonAmbiente = null;
			string jsonServico = null;
			string id = null;
			
			jsonVistoria = StaticData.vistoriaAtual.GetJson ();
			id = server.saveOrUpdateVistoria (jsonVistoria);
			if (id != null && id.StartsWith ("Exception")) {
				PopupsManager.Instance.ShowPopupMessage ("Erro no envio", "Não foi possível salvar a vistoria. " + jsonVistoria);
				salvando.SetActive (false);
				return;
			}
			
			StaticData.vistoriaAtual.id = int.Parse (id);

			if (StaticData.vistoriaAtual.imagem != null) {
				string photoName = "photoPredio";
				photoName += StaticData.vistoriaAtual.id + "_" + StaticData.vistoriaAtual.proprietario;
				string encoding = HttpUtility.UrlEncode (StaticData.vistoriaAtual.imagem);
				
				string url = CheckSpaceManager.instance.server.saveImage (photoName + ";" + encoding);
				if (url != null && !url.StartsWith ("Exception")) {
					StaticData.vistoriaAtual.imagemPredioUrl = url;
				} else {
					Debug.Log (url);
				}
			}

			jsonVistoria = StaticData.vistoriaAtual.GetJson ();
			id = server.saveOrUpdateVistoria (jsonVistoria);
			if (id != null && id.StartsWith ("Exception")) {
				PopupsManager.Instance.ShowPopupMessage ("Erro no envio", "Não foi possível salvar a vistoria. " + jsonVistoria);
				salvando.SetActive (false);
				return;
			}

			foreach (var item in createdAmbients) {
				if (!item.SaveAllNet ()) {
					PopupsManager.Instance.ShowPopupMessage ("Erro no envio", "Não foi possível salvar os ambientes. " + jsonVistoria);
					salvando.SetActive (false);
					return;
				}
			}

			if (!StaticData.OnlineVistoria) {
				DeleteVistorias ();
				StaticData.OnlineVistoria = true;
			}
			PopupsManager.Instance.ShowPopupMessage ("Confirmação", "A vistoria foi salva no banco online com sucesso.");
		} else {
			PopupsManager.Instance.ShowPopupMessage ("Erro de conexao", "É preciso conexão com a Internet para enviar os dados.");
		}
		
		salvando.SetActive (false);
	}

	public void UpdateAreaTotal ()
	{
		totalArea = 0;

		foreach (var item in createdAmbients) {
			totalArea += item.ambient.area;
		}

		relatorioVistoria.metragem.text = totalArea.ToString ();
		StaticData.vistoriaAtual.metragem = totalArea;
		StaticData.vistoriaAtual.metrosQuadrados = totalArea;
	}

	public void HideAll ()
	{
		relatorioVistoria.HideAll ();
		menuButtons.SetActive (false);
		dimensionsMenu.gameObject.SetActive (false);
		accousticMenu.gameObject.SetActive (false);
		ceramicMenu.gameObject.SetActive (false);
		ceramicWallMenu.gameObject.SetActive (false);
		eletricsMenu.gameObject.SetActive (false);
		framesMenu.gameObject.SetActive (false);
		paintingWallMenu.gameObject.SetActive (false);
		paintingCeilingMenu.gameObject.SetActive (false);
		sillsMenu.gameObject.SetActive (false);
		peitorisMenu.gameObject.SetActive (false);
		tentoBaguetesMenu.gameObject.SetActive (false);
		woodenFloorMenu.gameObject.SetActive (false);
		functionalWallMenu.gameObject.SetActive (false);
		functionalCeilingMenu.gameObject.SetActive (false);
		hidraulicsMenu.gameObject.SetActive (false);
		ambientMenu.HideAll ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (false);
	}

	public void ShowDimensions ()
	{
		dimensionsMenu.gameObject.SetActive (true);
		dimensionsMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowFrames ()
	{
		framesMenu.gameObject.SetActive (true);
		framesMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowFunctionalWall ()
	{
		functionalWallMenu.gameObject.SetActive (true);
		functionalWallMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowFunctionalCeiling ()
	{
		functionalCeilingMenu.gameObject.SetActive (true);
		functionalCeilingMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowAccoustic ()
	{
		accousticMenu.gameObject.SetActive (true);
		accousticMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowCeramic ()
	{
		ceramicMenu.gameObject.SetActive (true);
		ceramicMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowCeramicWall ()
	{
		ceramicWallMenu.gameObject.SetActive (true);
		ceramicWallMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowEletrics ()
	{
		eletricsMenu.gameObject.SetActive (true);
		eletricsMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowPaintingCeiling ()
	{
		paintingCeilingMenu.gameObject.SetActive (true);
		paintingCeilingMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowPaintingWall ()
	{
		paintingWallMenu.gameObject.SetActive (true);
		paintingWallMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowSills ()
	{
		sillsMenu.gameObject.SetActive (true);
		sillsMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowPeitoris ()
	{
		peitorisMenu.gameObject.SetActive (true);
		peitorisMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}


	public void ShowTentosBaguetes ()
	{
		tentoBaguetesMenu.gameObject.SetActive (true);
		tentoBaguetesMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}


	public void ShowWoodenFloor ()
	{
		woodenFloorMenu.gameObject.SetActive (true);
		woodenFloorMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowHidraulics ()
	{
		hidraulicsMenu.gameObject.SetActive (true);
		hidraulicsMenu.UpdateAmbientList ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
	}

	public void ShowHome ()
	{
		HideAll ();
		relatorioVistoria.ShowReportsHome ();
		relatorioVistoria.ShowForegroundMenu ();
		HorizontalScroller.Instance.MoveToMiddleScrollPosition ();
		ForegroundMenus.instace.SetMenuTitle ("Menu Principal");
		ForegroundMenus.instace.ShowMenuArrow (true);
		ForegroundMenus.instace.ShowAmbientsArrow (false);
		UpdateAreaTotal ();
	}

	public void ShowMenu ()
	{
		HideAll ();
		relatorioVistoria.HideForegroundMenu ();
		relatorioVistoria.HideAll ();
		menu.RecreateMenu ();
		menuButtons.SetActive (true);
		HorizontalScroller.Instance.ResetScrollPosition ();
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (false);
		ForegroundMenus.instace.SetMenuTitle ("Menu Principal");
	}

	public void ShowAmbientMenu ()
	{
		ambientMenu.ShowAll ();
		ForegroundMenus.instace.ShowMenuArrow (true);
		ForegroundMenus.instace.ShowAmbientsArrow (false);
		
	}

	public void IncreaseSeedGroup ()
	{
		aspectSeedGroup++;
	}

	public void CreateAmbient (string name)
	{
		Ambient ambient = new Ambient (name);
		createdAmbients.Add (ambient);
		UpdateAreaTotal ();
	}

	public void RemoveAmbient (string name)
	{
		for (int i = 0; i < createdAmbients.Count; i++) {
			if (createdAmbients [i].name == name)
				createdAmbients.Remove (createdAmbients [i]);
		} 		
	}

	void OnDestroy ()
	{
		instance = null;
	}

	public void Quit ()
	{
		StaticData.IsRelog = true;
		carregando.SetActive (true);
		Application.LoadLevel ("Login");
	}

	public void DeleteVistorias ()
	{
		
		List<Vistoria> vists = ServerHandler.vistoriaDAO.LoadAllIncludingDeleted ();
		
		if (vists != null && vists.Count > 0) {
			Vistoria vistoriaToDelete = vists [0];
		
			ServerHandler.imagemIndicadorDAO.DeleteFile ();
			ServerHandler.indicadorDAO.DeleteFile ();
			ServerHandler.servicoDAO.DeleteFile ();
			ServerHandler.ambienteDAO.DeleteFile ();
			ServerHandler.vistoriaDAO.DeleteFile ();
		}	
	}
}
