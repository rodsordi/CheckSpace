﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Web;

public class OverviewReportScreen : MonoBehaviour
{
	public UITable tableMapeamento;

	public GameObject foregroundButtons;
	public GameObject buildingPhoto;
	public GameObject buildingDrawing;
	public GameObject newAmbientPopup;
	public UILabel newAmbientName;

	public List<string> ambientsToCreate = new List<string> ();
	public List<string> checkedAmbients = new List<string> ();
	GameObject checkBoxPrefab;

	public UILabel cliente;
	public UILabel bloco;
	public UILabel dataVistoria;
	public UILabel unidade;
	public UILabel inicioVistoria;
	public UILabel terminoVistoria;
	public UILabel metragem;
	public UILabel vistoriador;
	public UILabel empreendimento;
	public UILabel construtora;
	public UILabel incorporadora;
	bool listeningEvents = false;

	List<AmbientCheckbox> ambientCheckboxes = new List<AmbientCheckbox> ();

	void Start ()
	{
		checkBoxPrefab = Resources.Load<GameObject> ("CheckboxAmbiente");
		foreach (var item in ambientsToCreate) {
			GameObject go = NGUITools.AddChild (tableMapeamento.gameObject, checkBoxPrefab);
			go.name = item;
			go.GetComponent<AmbientCheckbox> ().overviewScreen = this;
			go.GetComponent<AmbientCheckbox> ().SetName (item);
			ambientCheckboxes.Add (go.GetComponent<AmbientCheckbox> ());
		}

		tableMapeamento.repositionNow = true;

		if (StaticData.vistoriaAtual != null)
		{
			PopulateData ();
			CheckSpaceManager.instance.StartCoroutine(WaitFrames());
		}
	}

	IEnumerator WaitFrames()
	{
		yield return null;
		yield return null;
		CheckSpaceManager.instance.initialized = true;
	}

	public void PopulateData ()
	{
		cliente.text = StaticData.vistoriaAtual.proprietario;
		bloco.text = StaticData.vistoriaAtual.bloco;
		dataVistoria.text = StaticData.vistoriaAtual.dataVistoria.ToString ("dd/MM/yyyy");
		unidade.text = StaticData.vistoriaAtual.unidade;

		inicioVistoria.text = StaticData.vistoriaAtual.inicioVistoria.ToString ("HH:mm");
		terminoVistoria.text = StaticData.vistoriaAtual.terminoVistoria.ToString ("HH:mm");
		metragem.text = StaticData.vistoriaAtual.metragem.ToString ();

		empreendimento.text = StaticData.vistoriaAtual.empreendimento;
		construtora.text = StaticData.vistoriaAtual.construtora;
		incorporadora.text = StaticData.vistoriaAtual.incorporadora;

		if(StaticData.OnlineVistoria)
		{
			string json = CheckSpaceManager.instance.server.findAllVistoriador();
			List<Vistoriador> listaVistoriadores = JsonBehaviour.GetList<Vistoriador> (json);
			foreach (var item in listaVistoriadores) {
				if(item.id == StaticData.vistoriaAtual.vistoriador.id)
				{
					StaticData.vistoriaAtual.vistoriador = item;
					vistoriador.text = item.nome;
				}
			}
		}
		else
		{
			if(StaticData.vistoriaAtual.vistoriador != null)
			{
				vistoriador.text = StaticData.vistoriaAtual.vistoriador.nome;
			}
			else
				Debug.Log("vistoriador null");
		}
		if (StaticData.OnlineVistoria) {
			ServerService server = CheckSpaceManager.instance.server;
			string ambientes = server.getNomeAmbientesByVistoria (StaticData.vistoriaAtual.id);

			string[] ambientesSplit = ambientes.Split (';');
			foreach (var item in ambientesSplit) {
				if (ambientsToCreate.Contains (item)) {
						foreach (var chk in ambientCheckboxes) {

						if (chk.name == item) {						
							chk.gameObject.GetComponent<UIToggle> ().value = true;
						}
					}
				}
			}
		} else {
			IndicadorDAO indicadorDAO = new IndicadorDAO();
			List<string> ambientesSplit = new List<string>();
			try
			{
				ambientesSplit = indicadorDAO.getNomeAmbientesByVistoria(StaticData.vistoriaAtual);
			}
			catch{
				ambientesSplit = new List<string>();
			}

			foreach (var item in ambientesSplit) {
				Debug.Log(item);
				if (!string.IsNullOrEmpty(item))
				{
					if(ambientsToCreate.Contains (item)) {
						foreach (var chk in ambientCheckboxes) {
							if (chk.name == item) {
								chk.gameObject.GetComponent<UIToggle> ().value = true;
							}
						}
					}
					else
					{
						AddNewAmbientOption(item);
					}
				}
				else
				{
					Debug.Log("Ambient name was null or empty");
				}
			}
		}
	}

	public void ShowBuildingPhoto ()
	{
		buildingPhoto.SetActive (true);
		listeningEvents = true;
		buildingPhoto.GetComponentInChildren<TakePhoto>().AddEventListeners();

		Debug.Log(StaticData.vistoriaAtual.imagemPredioUrl);
		if(StaticData.OnlineVistoria)
		{
			if(StaticData.vistoriaAtual.imagem != null)
			{
				Texture2D tex2d = new Texture2D(2,2);
				if(tex2d.LoadImage(StaticData.vistoriaAtual.imagem))
					buildingPhoto.GetComponent<UITexture>().mainTexture = tex2d;
			}
			else if(!string.IsNullOrEmpty(StaticData.vistoriaAtual.imagemPredioUrl))
			{
				string s = CheckSpaceManager.instance.server.getImage(StaticData.vistoriaAtual.imagemPredioUrl);
				byte[] decode = HttpUtility.UrlDecodeToBytes(s);
				Texture2D t2d = new Texture2D(2,2);
				if(t2d.LoadImage (decode))
				buildingPhoto.GetComponent<UITexture>().mainTexture = t2d;
			}
			else
				buildingPhoto.GetComponent<UITexture>().mainTexture = null;
		}
		else
		{
			if(StaticData.vistoriaAtual.imagem != null)
			{
				Texture2D tex2d = new Texture2D(2,2);
				if(tex2d.LoadImage(StaticData.vistoriaAtual.imagem))
				buildingPhoto.GetComponent<UITexture>().mainTexture = tex2d;
			}
		}
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void ShowBuildingDrawing ()
	{
		buildingDrawing.SetActive (true);
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void ShowForegroundMenu ()
	{
		foregroundButtons.SetActive (true);
	}

	public void HideForegroundMenu ()
	{
		foregroundButtons.SetActive (false);
	}

	public void HideAll ()
	{
		if(listeningEvents)
		{
			buildingPhoto.SetActive(true);
			buildingPhoto.GetComponentInChildren<TakePhoto>().RemoveEventListeners();
			listeningEvents = false;
		}

		buildingPhoto.SetActive (false);
		buildingDrawing.SetActive (false);
		gameObject.SetActive (false);
	}

	public void ShowReportsHome ()
	{	
		gameObject.SetActive (true);
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void AddCheckedAmbient (string ambientName)
	{
		if (!checkedAmbients.Contains (ambientName)) {
			checkedAmbients.Add (ambientName);
			CheckSpaceManager.instance.CreateAmbient (ambientName);
		}
	}

	public void RemoveCheckedAmbient (string ambientName)
	{
		if (checkedAmbients.Contains (ambientName)) {
			checkedAmbients.Remove (ambientName);
			CheckSpaceManager.instance.RemoveAmbient (ambientName);
		}
	}

	public void AddNewAmbientOption (string name)
	{
		GameObject go = NGUITools.AddChild (tableMapeamento.gameObject, checkBoxPrefab);
		go.name = name;
		go.GetComponent<AmbientCheckbox> ().overviewScreen = this;
		go.GetComponent<AmbientCheckbox> ().SetName (name);
		tableMapeamento.Reposition ();
	}

	public void ShowNewAmbientPopup ()
	{
		newAmbientPopup.SetActive (true);
	}

	public void ConfirmAmbient ()
	{
		if (newAmbientName.text != "") {
			AddNewAmbientOption (newAmbientName.text);
			newAmbientName.text = "";	
			newAmbientPopup.SetActive (false);
		}
		
	}

	public void CancelAmbient ()
	{
		newAmbientName.text = "";	
		newAmbientPopup.SetActive (false);
	}

	public void ClienteChange ()
	{		
		StaticData.vistoriaAtual.proprietario = cliente.text;
	}

	public void UnidadeChange ()
	{
		StaticData.vistoriaAtual.unidade = unidade.text;
	}

	public void EmpreendimentoChange ()
	{
		StaticData.vistoriaAtual.empreendimento = empreendimento.text;
	}

	public void ConstrutoraChange ()
	{
		StaticData.vistoriaAtual.construtora = construtora.text;
	}

	public void IncorporadoraChange ()
	{
		StaticData.vistoriaAtual.incorporadora = incorporadora.text;
	}

	public void MetragemChange ()
	{
		StaticData.vistoriaAtual.metragem = double.Parse (metragem.text);
	}

	public void BlocoChange ()
	{
		StaticData.vistoriaAtual.bloco = bloco.text;
	}

	public void DataVistoriaChange ()
	{
		StaticData.vistoriaAtual.dataVistoria = System.DateTime.ParseExact (dataVistoria.text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
	}

	public void InicioChange ()
	{
		StaticData.vistoriaAtual.inicioVistoria = System.DateTime.ParseExact (inicioVistoria.text, "HH:mm", System.Globalization.CultureInfo.InvariantCulture);
	}

	public void TerminoChange ()
	{
		StaticData.vistoriaAtual.terminoVistoria = System.DateTime.ParseExact (terminoVistoria.text, "HH:mm", System.Globalization.CultureInfo.InvariantCulture);
	}
}
