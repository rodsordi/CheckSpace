﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class DimensionRow : MonoBehaviour
{

	public double rightFoot;
	public double areaMeters;

	public UILabel labelRightFoot;
	public UILabel labelArea;

	public void RightFootChanged ()
	{
		double.TryParse (labelRightFoot.text, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out rightFoot);
		CheckSpaceManager.instance.selectedAmbient.ambient.peDireito = rightFoot;
	}

	public void AreaChanged ()
	{
		double.TryParse (labelArea.text, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out areaMeters);
		CheckSpaceManager.instance.selectedAmbient.ambient.area = areaMeters;
		CheckSpaceManager.instance.UpdateAreaTotal ();
	}
}
