﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EvidenciasFotograficas : MonoBehaviour 
{
	public string ambient, service;
	public GameObject evidenciaRowPrefab;
	public GameObject addEvidenciaBtnPrefab;
	List<ImagemIndicador> fotosEvidencia;
	List<EvidenciaRow> evidencias = new List<EvidenciaRow>();
	public UITable tableEvidencias;
	public UIButton btnAddEvidencia;
	public UITable parentTable;

	public void Start()
	{
		if(!StaticData.OnlineVistoria)
		{
			ImagemIndicadorDAO imagemIndicadorDAO = new ImagemIndicadorDAO();
			fotosEvidencia = imagemIndicadorDAO.LoadByVistoriaAmbienteServico(StaticData.vistoriaAtual, ambient, service);
			Debug.Log(fotosEvidencia);
		}
		else
		{
			//CheckSpaceManager.instance.server.saveOrUpdateImagemIndicador();

		}
	}


	public void DestroyRows()
	{
		if(tableEvidencias == null)
			tableEvidencias = GetComponent<UITable>();

		foreach (var item in tableEvidencias.GetChildList()) {
			Destroy (item.gameObject);
		}

		if(evidencias != null)
			evidencias.Clear();
		else
			Debug.Log("null evidencias");
	}

	public void BuildRows()
	{
		DestroyRows();
		GameObject gobtn = NGUITools.AddChild(tableEvidencias.gameObject, Resources.Load<GameObject>("ButtonAddEvidencia"));
		btnAddEvidencia = gobtn.GetComponent<UIButton>();
		btnAddEvidencia.onClick.Add(new EventDelegate(this,"CreateRow"));

		if(fotosEvidencia != null)
		{
			foreach(var item in fotosEvidencia)
			{
				GameObject go = NGUITools.AddChild(tableEvidencias.gameObject, evidenciaRowPrefab);
				go.GetComponent<EvidenciaRow>();
				evidencias.Add(go.GetComponent<EvidenciaRow>());
			}
		}

		tableEvidencias.Reposition();
		parentTable.Reposition();
	}

	public void CreateRow()
	{
		if(fotosEvidencia == null)
			fotosEvidencia = new List<ImagemIndicador>();

		fotosEvidencia.Add(new ImagemIndicador());

		GameObject go = NGUITools.AddChild(tableEvidencias.gameObject, evidenciaRowPrefab);
		evidencias.Add(go.GetComponent<EvidenciaRow>());

		tableEvidencias.Reposition();
		parentTable.Reposition();
	}
}
