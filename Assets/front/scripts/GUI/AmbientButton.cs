﻿using UnityEngine;
using System.Collections;

public class AmbientButton : MonoBehaviour
{

	public string spriteName;
	public CheckSpaceConstants.CheckSpaceCategories category;
	public Ambient ambient;

	public UILabel labelName;
	public UISprite spriteVistoria;

	void Start ()
	{
		labelName.text = gameObject.name;

	}

	public void OnClick ()
	{
		CheckSpaceManager.instance.HideAll ();
		CheckSpaceManager.instance.selectedAmbient = ambient;
		ForegroundMenus.instace.SetMenuTitle (ambient.name);
		ForegroundMenus.instace.ShowMenuArrow (false);
		ForegroundMenus.instace.ShowAmbientsArrow (true);
		ForegroundMenus.instace.ShowBottomTitle (true);
		
		switch (category) {

		case CheckSpaceConstants.CheckSpaceCategories.Accoustic:
			CheckSpaceManager.instance.ShowAccoustic ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Ceramic:
			CheckSpaceManager.instance.ShowCeramic ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.CeramicWall:
			CheckSpaceManager.instance.ShowCeramicWall ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Dimensions:
			CheckSpaceManager.instance.ShowDimensions ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Eletrics:
			CheckSpaceManager.instance.ShowEletrics ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Frames:
			CheckSpaceManager.instance.ShowFrames ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Hidraulics:
			CheckSpaceManager.instance.ShowHidraulics ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingCeiling:
			CheckSpaceManager.instance.ShowPaintingCeiling ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.PaintingWall:
			CheckSpaceManager.instance.ShowPaintingWall ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.WoodenFloor:
			CheckSpaceManager.instance.ShowWoodenFloor ();
			break;
			
		case CheckSpaceConstants.CheckSpaceCategories.Sills:
			CheckSpaceManager.instance.ShowSills ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.Peitoris:
			CheckSpaceManager.instance.ShowPeitoris ();
			break;
		
		case CheckSpaceConstants.CheckSpaceCategories.TentosBaguetes:
			CheckSpaceManager.instance.ShowTentosBaguetes ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalWall:
			CheckSpaceManager.instance.ShowFunctionalWall ();
			break;

		case CheckSpaceConstants.CheckSpaceCategories.FunctionalCeiling:
			CheckSpaceManager.instance.ShowFunctionalCeiling ();
			break;

		}
	}
}
