﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CeramicWall : GUICategory
{

	public GameObject tableHeaderPrefab;
	public GameObject tileRowPrefab;

	public UITable tableAmbients;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;

	UIToggle toggleVerify;
	GameObject goTiles;

	void Awake ()
	{
		category = CheckSpaceConstants.CheckSpaceCategories.CeramicWall;
	}

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}

		rows.Clear ();

		ForegroundMenus.instace.SetBottomTitle ("Revestimento Parede");
		CreateVerificationToggle ();

		GameObject commentBtn = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load<GameObject> ("CommentsButtonNew"));
		commentBtn.GetComponent<CommentButton> ().category = CheckSpaceConstants.CheckSpaceCategories.CeramicWall;

		GameObject header4 = NGUITools.AddChild (tableAmbients.gameObject, tableHeaderPrefab);
		header4.GetComponent<LabelName> ().SetName ("III - Azulejos");

		goTiles = NGUITools.AddChild (tableAmbients.gameObject, tileRowPrefab);
		goTiles.name = CheckSpaceManager.instance.selectedAmbient.name + " tiles";

		rows.Add (goTiles);
		goTiles.GetComponent <AspectRow> ().categoryObject = this;
		goTiles.GetComponent <AspectRow> ().indicators = CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.indicatorsList [0];
		goTiles.GetComponent <AspectRow> ().evidenceList = CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.evidencias [0];
		serviceTags = CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.serviceTags;

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	public override void UpdateComments ()
	{
		string comment = "";

		foreach (var item in goTiles.GetComponent <AspectRow> ().indicators) {
			if (item.indicador == 2) {
				if (comment != "")
					comment += ", ";
				comment += item.nome;
			}
		}

		CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.service.tag = comment;
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.ceramicWallsInformation.verified = toggleVerify.value;
	}

}
