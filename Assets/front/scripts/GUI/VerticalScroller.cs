﻿using UnityEngine;
using System.Collections;

public class VerticalScroller : MonoBehaviour {

	public static VerticalScroller Instance;
	UIScrollView scrollView;

	void Start () {
		Instance = this;
		scrollView = GetComponent<UIScrollView>();
	}

	public void ResetScrollPosition()
	{
		scrollView.ResetPosition();
	}

	void OnDestroy()
	{
		Instance = null;
	}

}
