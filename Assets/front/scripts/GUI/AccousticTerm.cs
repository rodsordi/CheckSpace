﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AccousticTerm : MonoBehaviour
{

	public GameObject tableHeaderPrefab;
	public GameObject rowPrefab;
	
	public UITable tableAmbients;
	public OverviewReportScreen overViewScreen;
	
	public List<GameObject> rows = new List<GameObject> ();
	GameObject footer;
	public Ambient selectedAmbients;
	UIToggle toggleVerify;

	public void UpdateAmbientList ()
	{
		foreach (var item in tableAmbients.GetChildList()) {
			Destroy (item.gameObject);
		}
		
		ForegroundMenus.instace.SetBottomTitle ("Conforto Térmico e Acústico");
		CreateVerificationToggle ();

		GameObject go = NGUITools.AddChild (tableAmbients.gameObject, rowPrefab);
		go.name = CheckSpaceManager.instance.selectedAmbient.name;

		AccousticRow aRow = go.GetComponent<AccousticRow> ();
		aRow.labelOpenAccoustic.text = CheckSpaceManager.instance.selectedAmbient.ambient.acusticoAberto.ToString ();
		aRow.labelClosedAccoustic.text = CheckSpaceManager.instance.selectedAmbient.ambient.acusticoFechado.ToString ();
		aRow.labelOpenTermic.text = CheckSpaceManager.instance.selectedAmbient.ambient.termoAberto.ToString ();
		aRow.labelClosedTermicc.text = CheckSpaceManager.instance.selectedAmbient.ambient.termoFechado.ToString ();

		if (CheckSpaceManager.instance.selectedAmbient.ambient.acusticoAbertoComRes == "Res") {
			aRow.toggleOpenRes.value = true;
			aRow.toggleOpenCom.value = false;
		} else {
			aRow.toggleOpenRes.value = false;
			aRow.toggleOpenCom.value = true;
		}

		if (CheckSpaceManager.instance.selectedAmbient.ambient.acusticoFechadoComRes == "Res") {
			aRow.toggleClosedRes.value = true;
			aRow.toggleClosedCom.value = false;
		} else {
			aRow.toggleClosedRes.value = false;
			aRow.toggleClosedCom.value = true;
		}

		rows.Add (go);

		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		Invoke ("Reposition", 0.05f);
		
	}

	void Reposition ()
	{
		VerticalScroller.Instance.ResetScrollPosition ();
		tableAmbients.repositionNow = true;
		tableAmbients.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void CreateVerificationToggle ()
	{
		GameObject checkVerify = NGUITools.AddChild (tableAmbients.gameObject, Resources.Load <GameObject> ("CheckboxVerified"));
		toggleVerify = checkVerify.GetComponent <UIToggle> ();
		toggleVerify.onChange.Add (new EventDelegate (this, "SetAmbientVerified"));
		toggleVerify.value = CheckSpaceManager.instance.selectedAmbient.accousticInformation.verified;
	}

	public void SetAmbientVerified ()
	{
		CheckSpaceManager.instance.selectedAmbient.accousticInformation.verified = toggleVerify.value;
	}
}
