﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class StaticData
{
	public static List<Vistoria> vistorias;
	public static Vistoria vistoriaAtual;
	public static Vistoriador vistoriadorAtual;
	public static bool OnlineVistoria = false;
	public static bool IsRelog = false;
}
