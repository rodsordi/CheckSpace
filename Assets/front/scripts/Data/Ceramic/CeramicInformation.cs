﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CeramicInformation:BaseInformation
{

	public Floor ceramicFloor;
	public Subfloor ceramicSubFloor;
	public Tile ceramicTile;

	//readonly int[] indicatorsCount = { 18, 2, 14 };
	
	public CeramicInformation (string ambientName)
	{		
		serviceTags = new string[] {"Sujo", "Ondulações", "Manchado", "Riscado", "Fissuras", "Tricado", "Lascado",
			"Diferença de tonalidade no piso", "Fora de esquadro", "Sem caimento", "Piso com relevo", "Juntas desiguais", 
			"Diferença de tonalidade no rejunte", "Falta de rejunte", "Peça solta", "Peça oca", "Fora de nivel", "Contrapiso fofo"};

		ambient = ambientName;
		service.nome = Servicos.REVESTIMENTO_PISO.ToString();
		prefabNames = new string[] { "CeramicFloorRow", "CeramicSubFloorRow" };
		InitializeIndicators();
	}

	public void InitializeIndicators()
	{
		indicatorsList = new List<List<Indicador>> ();
		evidencias = new List<List<ImagemIndicador>>();
		for (int i = 0; i < prefabNames.Length; i++) {
			
			GameObject baseGO = Resources.Load <GameObject> (prefabNames [i]);
			AspectRow aspectRow = baseGO.GetComponent <AspectRow> (); 

			indicatorsList.Add (new List<Indicador> ());
			evidencias.Add (new List<ImagemIndicador> ());
			int total = 0;

			for (int j = 0; j < aspectRow.aspectsList.Count; j++) {
				for (int k = 0; k < aspectRow.aspectsList [j].aspectNames.Count; k++) {
					indicatorsList [i].Add (new Indicador ());	
					indicatorsList [i] [total].indicador = 3;
					indicatorsList [i] [total].nome = aspectRow.aspectsList [j].aspectNames [k];
					indicatorsList [i] [total].servico = service;
					indicatorsList [i] [total].grupo =  aspectRow.aspectsList [j].group.Split(' ')[0];
					indicatorsList [i] [total].subGrupo = aspectRow.aspectsList [j].header.Split('-')[0];
					ImagemIndicador imgIndicator = new ImagemIndicador();
					imgIndicator.indicador = indicatorsList[i][total];
					evidencias [i].Add (new ImagemIndicador ());	
					evidencias [i] [total] = imgIndicator;
					total++;
				}
			}
		}
	}

	public override void ShowPhotosWindow(Indicador indic, ImagemIndicador imgIndicador)
	{
		CheckSpaceManager.instance.photosWindow.FillComments(serviceTags);
		CheckSpaceManager.instance.photosWindow.Open();

	}
}
