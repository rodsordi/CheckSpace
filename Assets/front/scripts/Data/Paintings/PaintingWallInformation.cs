﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PaintingWallInformation:BaseInformation
{
	public PaintingWallInformation (string ambientName)
	{
		ambient = ambientName;

		serviceTags = new string[]  {"Sujo", "Ondulado", "Manchado", "Riscado", "Fissurado", "Trincado", "Diferença de tonalidade",
			"Com manchas de oxidação", "Com escamações", "Com manchas de umidade"};

		service.nome = Servicos.PINTURA_PAREDE.ToString();
		prefabNames = new string[] {"PaintingRowII"};
		InitializeIndicators();
	}

	public void InitializeIndicators()
	{
		indicatorsList = new List<List<Indicador>> ();
		evidencias = new List<List<ImagemIndicador>>();

		for (int i = 0; i < prefabNames.Length; i++) {
			
			GameObject baseGO = Resources.Load <GameObject> (prefabNames [i]);
			AspectRow aspectRow = baseGO.GetComponent <AspectRow> (); 
			
			indicatorsList.Add (new List<Indicador> ());
			evidencias.Add (new List<ImagemIndicador> ());

			int total = 0;
			
			for (int j = 0; j < aspectRow.aspectsList.Count; j++) {
				for (int k = 0; k < aspectRow.aspectsList [j].aspectNames.Count; k++) {
					
					indicatorsList [i].Add (new Indicador ());	
					indicatorsList [i] [total].indicador = 3;
					indicatorsList [i] [total].nome = aspectRow.aspectsList [j].aspectNames [k];
					indicatorsList [i] [total].grupo = aspectRow.aspectsList [j].group.Split(' ')[0];
					indicatorsList [i] [total].subGrupo = aspectRow.aspectsList [j].header.Split('-')[0];

					ImagemIndicador imgIndicator = new ImagemIndicador();
					imgIndicator.indicador = indicatorsList[i][total];
					evidencias [i].Add (new ImagemIndicador ());	
					evidencias [i] [total] = imgIndicator;
					total++;
				}
			}
		}
	}

	public override void ShowPhotosWindow(Indicador indic, ImagemIndicador imgIndicador)
	{
		CheckSpaceManager.instance.photosWindow.FillComments(serviceTags);
		CheckSpaceManager.instance.photosWindow.Open();
	}
}
