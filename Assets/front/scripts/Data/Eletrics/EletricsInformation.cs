﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EletricsInformation : BaseInformation
{
//readonly int[] indicatorsCount = { 5, 5, 4, 7, 4 };
	
	public EletricsInformation (string ambientName)
	{		
		serviceTags = new string[] {"Fora do Prumo", "Desalinhado", "Fora de altura padrão", "Sujo", "Manchado", "Quebrado", "Solto", "Não encaixa", "Sujo", "Sem isolamento",
			"Emenda ruim", "Sem fase", "Sem Terra", "Sem Neutro", "Sem Retorno", "Rabicho curto", 
			"Não funciona", "Não funciona", "Não funciona", "Não funciona",
			"Voltagem inferior", "Quadro", "Desmontando","Quebrado", "Sem tampa",
			"Sujo", "Mal fixado", "Desalinhado", "Sem etiqueta de identificação", "Sem identificação",
			"Tensão inferior", "Sem tensão", "Sujo", "Tubos obstruídos", "Caixas obstruídas", "Não fixado", "Mal fixado", "Não funciona"};
		
		ambient = ambientName;
		service.nome = Servicos.ELETRICA.ToString();
		prefabNames = new string[] { "EletricsRowI", "EletricsRowII", "EletricsRowIII", "EletricsRowIV", "EletricsRowV" };
		InitializeIndicators();
	}

	public void InitializeIndicators()
	{
		indicatorsList = new List<List<Indicador>> ();
		evidencias = new List<List<ImagemIndicador>>();

		for (int i = 0; i < prefabNames.Length; i++) {
			
			GameObject baseGO = Resources.Load <GameObject> (prefabNames [i]);
			AspectRow aspectRow = baseGO.GetComponent <AspectRow> (); 
			
			indicatorsList.Add (new List<Indicador> ());
			evidencias.Add (new List<ImagemIndicador> ());

			int total = 0;
			
			for (int j = 0; j < aspectRow.aspectsList.Count; j++) {
				for (int k = 0; k < aspectRow.aspectsList [j].aspectNames.Count; k++) {
					
					indicatorsList [i].Add (new Indicador ());	
					indicatorsList [i] [total].indicador = 3;
					indicatorsList [i] [total].nome = aspectRow.aspectsList [j].aspectNames [k];
					indicatorsList [i] [total].grupo = aspectRow.aspectsList [j].group.Split(' ')[0];
					indicatorsList [i] [total].subGrupo = aspectRow.aspectsList [j].header.Split('-')[0];

					ImagemIndicador imgIndicator = new ImagemIndicador();
					imgIndicator.indicador = indicatorsList[i][total];
					evidencias [i].Add (new ImagemIndicador ());	
					evidencias [i] [total] = imgIndicator;
					total++;
				}
			}
		}
	}

	public override void ShowPhotosWindow(Indicador indic, ImagemIndicador imgIndicador)
	{
		CheckSpaceManager.instance.photosWindow.FillComments(serviceTags);
		CheckSpaceManager.instance.photosWindow.Open();
	}
}
