﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Web;

public class Ambient
{
	public int id = 0;
	public int buildingId = 0;
	public string name = "";

	public DimensionsInformation dimensionsInformation;
	public AccousticInformation accousticInformation;
	public CeramicInformation ceramicInformation;
	public CeramicWallsInformation ceramicWallsInformation;
	public EletricsInformation eletricsInformation;
	public HidraulicsInformation hidraulicsInformation;
	public FramesInformation framesInformation;
	public PaintingCeilingInformation paintingCeilingInformation;
	public PaintingWallInformation paintingWallInformation;
	public WoodenFloorInformation woodenFloorInformation;
	public SillsInformation sillsInformation;
	public PeitorisInformation peitorisInformation;
	public TentosBaguetesInformation tentosBaguetesInformation;
	public FunctionalWallInformation functionalWallInformation;
	public FunctionalCeilingInformation functionalCeilingInformation;

	public Ambiente ambient;

	public Ambient (string ambName)
	{
		name = ambName;
		ambient = new Ambiente ();

		ambient.vistoria = StaticData.vistoriaAtual;
		ambient.nome = name;
		ambient.peDireito = 0;
		ambient.area = 0;
		ambient.acusticoAbertoComRes = "Res";
		ambient.acusticoFechadoComRes = "Res";

		dimensionsInformation = new DimensionsInformation ();
		accousticInformation = new AccousticInformation ();
		ceramicInformation = new CeramicInformation (name);
		ceramicWallsInformation = new CeramicWallsInformation (name);
		eletricsInformation = new EletricsInformation (name);
		hidraulicsInformation = new HidraulicsInformation (name);
		framesInformation = new FramesInformation (name);
		paintingCeilingInformation = new PaintingCeilingInformation (name);
		paintingWallInformation = new PaintingWallInformation (name);
		woodenFloorInformation = new WoodenFloorInformation (name);
		sillsInformation = new SillsInformation (name);
		peitorisInformation = new PeitorisInformation (name);
		tentosBaguetesInformation = new TentosBaguetesInformation (name);
		functionalWallInformation = new FunctionalWallInformation (name);
		functionalCeilingInformation = new FunctionalCeilingInformation (name);

		if (!CheckSpaceManager.instance.initialized) {
			if (StaticData.OnlineVistoria) {
				LoadAllNet ();
			} else {
				CheckSpaceManager.instance.StartCoroutine (LoadAllLocal ());
			}
		}
	}

	public void FillIndicadoresNet (BaseInformation categoryInformation, string service)
	{
		List<Indicador> listaIndicadores = CheckSpaceManager.instance.FindOnlineIndicators (name, service);
		if (listaIndicadores != null && listaIndicadores.Count > 0) {
			for (int i = 0; i < categoryInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < categoryInformation.indicatorsList [i].Count; j++) {

					categoryInformation.indicatorsList [i] [j] = listaIndicadores.Where
						(ind => (ind != null &&
					ind.servico != null &&
					ind.servico.ambiente != null &&
					ind.servico.ambiente.vistoria != null) ? 
						 (ind.nome == categoryInformation.indicatorsList [i] [j].nome &&
					ind.grupo == categoryInformation.indicatorsList [i] [j].grupo &&
					ind.subGrupo == categoryInformation.indicatorsList [i] [j].subGrupo) : false).ToList () [0];
				}
			}
		}
	}

	public void FillImageIndicadoresNet (BaseInformation categoryInformation, string service)
	{
		List<ImagemIndicador> listaIndicadores = CheckSpaceManager.instance.FindOnlineImageIndicators (name, service);
		if (listaIndicadores != null && listaIndicadores.Count > 0) {
			for (int i = 0; i < categoryInformation.evidencias.Count; i++) {
				for (int j = 0; j < categoryInformation.evidencias [i].Count; j++) {

					categoryInformation.evidencias [i] [j] = listaIndicadores.Where
						(ind => (ind != null &&
					ind.indicador != null) ? 
						 (ind.indicador.nome == categoryInformation.indicatorsList [i] [j].nome &&
					ind.indicador.grupo == categoryInformation.indicatorsList [i] [j].grupo &&
					ind.indicador.subGrupo == categoryInformation.indicatorsList [i] [j].subGrupo) : false).ToList () [0];

				}
			}
		}
	}

	public void LoadAllNet ()
	{
		ambient = CheckSpaceManager.instance.FindOnlineAmbient (name);

		LoadOnlineServices ();
		FillIndicadoresNet (ceramicInformation, Servicos.REVESTIMENTO_PISO.ToString ());
		FillIndicadoresNet (ceramicWallsInformation, Servicos.REVESTIMENTO_PAREDE.ToString ());
		FillIndicadoresNet (eletricsInformation, Servicos.ELETRICA.ToString ());
		FillIndicadoresNet (framesInformation, Servicos.ESQUADRIAS.ToString ());
		FillIndicadoresNet (hidraulicsInformation, Servicos.HIDRAULICA.ToString ());
		FillIndicadoresNet (paintingCeilingInformation, Servicos.PINTURA_TETO.ToString ());
		FillIndicadoresNet (paintingWallInformation, Servicos.PINTURA_PAREDE.ToString ());
		FillIndicadoresNet (sillsInformation, Servicos.SOLEIRAS.ToString ());
		FillIndicadoresNet (peitorisInformation, Servicos.PEITORIS.ToString ());
		FillIndicadoresNet (tentosBaguetesInformation, Servicos.TENTOS_BAGUETES.ToString ());
		FillIndicadoresNet (woodenFloorInformation, Servicos.PISO_MADEIRA.ToString ());
		FillIndicadoresNet (functionalCeilingInformation, Servicos.FUNCIONAL_TETO.ToString ());
		FillIndicadoresNet (functionalWallInformation, Servicos.FUNCIONAL_PAREDE.ToString ());

		FillImageIndicadoresNet (ceramicInformation, Servicos.REVESTIMENTO_PISO.ToString ());
		FillImageIndicadoresNet (ceramicWallsInformation, Servicos.REVESTIMENTO_PAREDE.ToString ());
		FillImageIndicadoresNet (eletricsInformation, Servicos.ELETRICA.ToString ());
		FillImageIndicadoresNet (framesInformation, Servicos.ESQUADRIAS.ToString ());
		FillImageIndicadoresNet (hidraulicsInformation, Servicos.HIDRAULICA.ToString ());
		FillImageIndicadoresNet (paintingCeilingInformation, Servicos.PINTURA_TETO.ToString ());
		FillImageIndicadoresNet (paintingWallInformation, Servicos.PINTURA_PAREDE.ToString ());
		FillImageIndicadoresNet (sillsInformation, Servicos.SOLEIRAS.ToString ());
		FillImageIndicadoresNet (peitorisInformation, Servicos.PEITORIS.ToString ());
		FillImageIndicadoresNet (tentosBaguetesInformation, Servicos.TENTOS_BAGUETES.ToString ());
		FillImageIndicadoresNet (woodenFloorInformation, Servicos.PISO_MADEIRA.ToString ());
		FillImageIndicadoresNet (functionalCeilingInformation, Servicos.FUNCIONAL_TETO.ToString ());
		FillImageIndicadoresNet (functionalWallInformation, Servicos.FUNCIONAL_PAREDE.ToString ());
	}

	public void LoadOnlineServices ()
	{
		eletricsInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.ELETRICA.ToString (), ambient.id);
		framesInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.ESQUADRIAS.ToString (), ambient.id);
		functionalWallInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.FUNCIONAL_PAREDE.ToString (), ambient.id);
		functionalCeilingInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.FUNCIONAL_TETO.ToString (), ambient.id);
		hidraulicsInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.HIDRAULICA.ToString (), ambient.id);	
		peitorisInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.PEITORIS.ToString (), ambient.id);
		paintingWallInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.PINTURA_PAREDE.ToString (), ambient.id);
		paintingCeilingInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.PINTURA_TETO.ToString (), ambient.id);
		woodenFloorInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.PISO_MADEIRA.ToString (), ambient.id);
		ceramicWallsInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.REVESTIMENTO_PAREDE.ToString (), ambient.id);
		ceramicInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.REVESTIMENTO_PISO.ToString (), ambient.id);
		sillsInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.SOLEIRAS.ToString (), ambient.id);
		tentosBaguetesInformation.service = CheckSpaceManager.instance.FindOnlineServicesByAmbientId (Servicos.TENTOS_BAGUETES.ToString (), ambient.id);
	}

	public void SaveLocalIndicators (BaseInformation categoryInformation)
	{

		categoryInformation.service.ambiente = ambient;
		CheckSpaceManager.instance.servicoDAO.SaveOrUpdate (categoryInformation.service);

		List<Indicador> newList = new List<Indicador> ();

		for (int i = 0; i < categoryInformation.indicatorsList.Count; i++) {
			for (int j = 0; j < categoryInformation.indicatorsList [i].Count; j++) {
				categoryInformation.indicatorsList [i] [j].servico = categoryInformation.service;
				newList.Add (categoryInformation.indicatorsList [i] [j]);
			}
		}
		
		CheckSpaceManager.instance.indicadorDAO.SaveList (newList);
	}

	public void SaveLocalImageIndicators (BaseInformation categoryInformation)
	{
		List<ImagemIndicador> newList = new List<ImagemIndicador> ();
		
		for (int i = 0; i < categoryInformation.evidencias.Count; i++) {
			for (int j = 0; j < categoryInformation.evidencias [i].Count; j++) {
				newList.Add (categoryInformation.evidencias [i] [j]);
			}
		}
		
		CheckSpaceManager.instance.imagemIndicadorDAO.SaveList (newList);
	}

	public void SaveAllLocal ()
	{
		ambient.vistoria = StaticData.vistoriaAtual;

		CheckSpaceManager.instance.ambienteDAO.SaveOrUpdate (ambient);

		SaveLocalIndicators (ceramicInformation);
		SaveLocalIndicators (ceramicWallsInformation);
		SaveLocalIndicators (eletricsInformation);
		SaveLocalIndicators (framesInformation);
		SaveLocalIndicators (hidraulicsInformation);
		SaveLocalIndicators (paintingCeilingInformation);
		SaveLocalIndicators (paintingWallInformation);
		SaveLocalIndicators (sillsInformation);
		SaveLocalIndicators (peitorisInformation);
		SaveLocalIndicators (tentosBaguetesInformation);
		SaveLocalIndicators (woodenFloorInformation);
		SaveLocalIndicators (functionalWallInformation);
		SaveLocalIndicators (functionalCeilingInformation);

		SaveLocalImageIndicators (ceramicInformation);
		SaveLocalImageIndicators (ceramicWallsInformation);
		SaveLocalImageIndicators (eletricsInformation);
		SaveLocalImageIndicators (framesInformation);
		SaveLocalImageIndicators (hidraulicsInformation);
		SaveLocalImageIndicators (paintingCeilingInformation);
		SaveLocalImageIndicators (paintingWallInformation);
		SaveLocalImageIndicators (sillsInformation);
		SaveLocalImageIndicators (peitorisInformation);
		SaveLocalImageIndicators (tentosBaguetesInformation);
		SaveLocalImageIndicators (woodenFloorInformation);
		SaveLocalImageIndicators (functionalWallInformation);
		SaveLocalImageIndicators (functionalCeilingInformation);
	}

	public void SaveIndicatorsFromCategory (BaseInformation categoryInformation)
	{
		List<Indicador> newList = new List<Indicador> ();

		for (int i = 0; i < categoryInformation.indicatorsList.Count; i++) {
			for (int j = 0; j < categoryInformation.indicatorsList [i].Count; j++) {
				categoryInformation.indicatorsList [i] [j].servico = categoryInformation.service;
				newList.Add (categoryInformation.indicatorsList [i] [j]);
			}
		}

		string jsonIndicadores = JsonBehaviour.GetJsonFromList(newList);

		string jsonWeb = CheckSpaceManager.instance.server.saveIndicadorList (jsonIndicadores);

		if(!string.IsNullOrEmpty(jsonWeb) && !jsonWeb.StartsWith("Exception"))
		{
			newList = JsonBehaviour.GetList<Indicador>(jsonWeb);
	
			for (int i = 0; i < categoryInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < categoryInformation.indicatorsList [i].Count; j++) {

					categoryInformation.indicatorsList [i] [j].id 
						= newList.Where(ind => (ind != null && ind.servico != null)?
		                (ind.nome == categoryInformation.indicatorsList[i][j].nome &&
						 ind.grupo == categoryInformation.indicatorsList[i][j].grupo &&
						 ind.subGrupo == categoryInformation.indicatorsList[i][j].subGrupo && 
						 ind.indicador == categoryInformation.indicatorsList[i][j].indicador)  
                      :false).ToList()[0].id;

				}
			}
		}
		else
		{
			Debug.Log("grupo nulo em " + categoryInformation.service.nome);
		}

		/*for (int z = 0; z < newList.Count; z++) {
			string st = CheckSpaceManager.instance.server.saveOrUpdateIndicador (newList [z].GetJson ());
			if (!string.IsNullOrEmpty (st) && !st.StartsWith ("Exception")) {
				int newId = int.Parse (st);
				newList [z].id = newId;
			} else {
				Debug.Log(st);
			}
		}*/
	}

	public void SaveImageIndicatorsFromCategory (BaseInformation categoryInformation)
	{
		List<ImagemIndicador> newList = new List<ImagemIndicador> ();
		
		for (int i = 0; i < categoryInformation.evidencias.Count; i++) {
			for (int j = 0; j < categoryInformation.evidencias [i].Count; j++) {

				newList.Add (categoryInformation.evidencias [i] [j]);
			}
		}
		string jsonImagemIndicadores = JsonBehaviour.GetJsonFromList(newList);

		string jsonWeb = CheckSpaceManager.instance.server.saveImagemIndicadorList (jsonImagemIndicadores);

		if(!string.IsNullOrEmpty (jsonWeb) && !jsonWeb.StartsWith("Exception"))
		{
			newList = JsonBehaviour.GetList<ImagemIndicador>(jsonWeb);

			for (int i = 0; i < categoryInformation.evidencias.Count; i++) {
				for (int j = 0; j < categoryInformation.evidencias [i].Count; j++)
				{
					categoryInformation.evidencias [i][j].id = newList.Where(ind => (ind !=null && ind.indicador != null)?
					                                                      (ind.indicador.id == categoryInformation.evidencias[i][j].indicador.id)  :false).ToList()[0].id;

				}
			}

			for (int i = 0; i < categoryInformation.evidencias.Count; i++) {
				for (int j = 0; j < categoryInformation.evidencias [i].Count; j++)
				{
					if (categoryInformation.evidencias [i][j] != null && categoryInformation.evidencias [i][j].imagem != null) {
						string photoName = "Foto_Evidencia_";
						photoName += categoryInformation.evidencias [i][j].indicador.servico.ambiente.nome + "_" + categoryInformation.evidencias [i][j].indicador.servico.nome + "_" + categoryInformation.evidencias [i][j].indicador.nome + "_" + categoryInformation.evidencias [i][j].indicador.grupo
							+ "_" + categoryInformation.evidencias [i][j].indicador.subGrupo;
						string encoding = HttpUtility.UrlEncode (categoryInformation.evidencias [i][j].imagem);
						
						categoryInformation.evidencias [i][j].imagemUrl = CheckSpaceManager.instance.server.saveImage ("Vistoria_" + StaticData.vistoriaAtual.id.ToString () + ";" + photoName + ";" + encoding);

					}
				}
			}

			newList = new List<ImagemIndicador> ();

			for (int i = 0; i < categoryInformation.evidencias.Count; i++) {
				for (int j = 0; j < categoryInformation.evidencias [i].Count; j++) {
					
					newList.Add (categoryInformation.evidencias [i] [j]);
				}
			}

			string jsonImagemIndicadoresUpdate = JsonBehaviour.GetJsonFromList(newList);
			string jsonWebUpdate = CheckSpaceManager.instance.server.saveImagemIndicadorList (jsonImagemIndicadoresUpdate);

			if(!string.IsNullOrEmpty (jsonWebUpdate) && !jsonWebUpdate.StartsWith("Exception"))
			{
				for (int a = 0; a < categoryInformation.evidencias.Count; a++) {
					for (int j = 0; j < categoryInformation.evidencias [a].Count; j++)
					{

						categoryInformation.evidencias [a][j].imagemUrl = newList.Where(ind => (ind !=null && ind.indicador != null)?
						                                                         (ind.indicador == categoryInformation.evidencias[a][j].indicador)  :false).ToList()[0].imagemUrl;
					}
				}
			}
			else
			{
				Debug.Log("Erro no update dos imagem indicadores " + jsonWebUpdate);
			}
		}
		else
		{
			Debug.Log("erro nos imagemindicadores antes de salvar foto " + jsonWeb);
		}
	}

	public bool SaveAllNet ()
	{

		string jsonAmbient = ambient.GetJson ();

		string result = CheckSpaceManager.instance.server.saveOrUpdateAmbiente (jsonAmbient);

		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar ambiente " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}

		ambient.id = int.Parse (result);

		ceramicInformation.service.ambiente = ambient;
		string jsonService = ceramicInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);

		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		ceramicInformation.service.id = int.Parse (result);

		ceramicWallsInformation.service.ambiente = ambient;
		jsonService = ceramicWallsInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		ceramicWallsInformation.service.id = int.Parse (result);

		eletricsInformation.service.ambiente = ambient;
		jsonService = eletricsInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);

		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}

		eletricsInformation.service.id = int.Parse (result);

		framesInformation.service.ambiente = ambient;
		;
		jsonService = framesInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		framesInformation.service.id = int.Parse (result);

		hidraulicsInformation.service.ambiente = ambient;
		jsonService = hidraulicsInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		hidraulicsInformation.service.id = int.Parse (result);

		paintingCeilingInformation.service.ambiente = ambient;
		jsonService = paintingCeilingInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		paintingCeilingInformation.service.id = int.Parse (result);

		paintingWallInformation.service.ambiente = ambient;
		jsonService = paintingWallInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		paintingWallInformation.service.id = int.Parse (result);

		sillsInformation.service.ambiente = ambient;
		jsonService = sillsInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		sillsInformation.service.id = int.Parse (result);

		peitorisInformation.service.ambiente = ambient;
		jsonService = peitorisInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		peitorisInformation.service.id = int.Parse (result);

		tentosBaguetesInformation.service.ambiente = ambient;
		jsonService = tentosBaguetesInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		tentosBaguetesInformation.service.id = int.Parse (result);

		woodenFloorInformation.service.ambiente = ambient;
		jsonService = woodenFloorInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		woodenFloorInformation.service.id = int.Parse (result);

		functionalCeilingInformation.service.ambiente = ambient;
		jsonService = functionalCeilingInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		functionalCeilingInformation.service.id = int.Parse (result);

		functionalWallInformation.service.ambiente = ambient;
		jsonService = functionalWallInformation.service.GetJson ();
		result = CheckSpaceManager.instance.server.saveOrUpdateServico (jsonService);
		
		if (result != null && result.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro ao salvar serviço. " + result, "Não foi possível salvar a vistoria. ");
			return false;
		}
		
		functionalWallInformation.service.id = int.Parse (result);

		SaveIndicatorsFromCategory (ceramicInformation);
		SaveIndicatorsFromCategory (ceramicWallsInformation);
		SaveIndicatorsFromCategory (eletricsInformation);
		SaveIndicatorsFromCategory (framesInformation);
		SaveIndicatorsFromCategory (hidraulicsInformation);
		SaveIndicatorsFromCategory (paintingCeilingInformation);
		SaveIndicatorsFromCategory (paintingWallInformation);
		SaveIndicatorsFromCategory (sillsInformation);
		SaveIndicatorsFromCategory (peitorisInformation);
		SaveIndicatorsFromCategory (tentosBaguetesInformation);
		SaveIndicatorsFromCategory (woodenFloorInformation);
		SaveIndicatorsFromCategory (functionalWallInformation);
		SaveIndicatorsFromCategory (functionalCeilingInformation);

		SaveImageIndicatorsFromCategory (ceramicInformation);
		SaveImageIndicatorsFromCategory (ceramicWallsInformation);
		SaveImageIndicatorsFromCategory (eletricsInformation);
		SaveImageIndicatorsFromCategory (framesInformation);
		SaveImageIndicatorsFromCategory (hidraulicsInformation);
		SaveImageIndicatorsFromCategory (paintingCeilingInformation);
		SaveImageIndicatorsFromCategory (paintingWallInformation);
		SaveImageIndicatorsFromCategory (sillsInformation);
		SaveImageIndicatorsFromCategory (peitorisInformation);
		SaveImageIndicatorsFromCategory (tentosBaguetesInformation);
		SaveImageIndicatorsFromCategory (woodenFloorInformation);
		SaveImageIndicatorsFromCategory (functionalWallInformation);
		SaveImageIndicatorsFromCategory (functionalCeilingInformation);

		return true;
		
	}

	public IEnumerator LoadAllLocal ()
	{		
		List<Ambiente> ambientes = CheckSpaceManager.instance.FilterAmbients (ambient.nome);

		if (ambientes.Count > 0) {
			ambient = ambientes [0];
		} else
			yield break;

		List<Servico> servicos = CheckSpaceManager.instance.FilterServicos (ambient.nome);

		foreach (var item in servicos) {
			if (item.nome == Servicos.ELETRICA.ToString ())
				eletricsInformation.service = item;
			else if (item.nome == Servicos.ESQUADRIAS.ToString ())
				framesInformation.service = item;
			else if (item.nome == Servicos.FUNCIONAL_PAREDE.ToString ())
				functionalWallInformation.service = item;
			else if (item.nome == Servicos.FUNCIONAL_TETO.ToString ())
				functionalCeilingInformation.service = item;
			else if (item.nome == Servicos.HIDRAULICA.ToString ())
				hidraulicsInformation.service = item;
			else if (item.nome == Servicos.PEITORIS.ToString ())
				peitorisInformation.service = item;
			else if (item.nome == Servicos.PINTURA_PAREDE.ToString ())
				paintingWallInformation.service = item;
			else if (item.nome == Servicos.PINTURA_TETO.ToString ())
				paintingCeilingInformation.service = item;
			else if (item.nome == Servicos.PISO_MADEIRA.ToString ())
				woodenFloorInformation.service = item;
			else if (item.nome == Servicos.REVESTIMENTO_PAREDE.ToString ())
				ceramicWallsInformation.service = item;
			else if (item.nome == Servicos.REVESTIMENTO_PISO.ToString ())
				ceramicInformation.service = item;
			else if (item.nome == Servicos.SOLEIRAS.ToString ())
				sillsInformation.service = item;
			else if (item.nome == Servicos.TENTOS_BAGUETES.ToString ())
				tentosBaguetesInformation.service = item;
			yield return new WaitForEndOfFrame ();
		}

		System.DateTime time = System.DateTime.Now;

		List<Indicador> ceramicIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.REVESTIMENTO_PISO.ToString (), name);

		if (ceramicIndicators != null && ceramicIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < ceramicInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < ceramicInformation.indicatorsList [i].Count; j++) {
					ceramicInformation.indicatorsList [i] [j] = ceramicIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> ceramicImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.REVESTIMENTO_PISO.ToString (), name);
		
		if (ceramicImageIndicators != null && ceramicImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < ceramicInformation.evidencias.Count; i++) {
				for (int j = 0; j < ceramicInformation.evidencias [i].Count; j++) {
					ceramicInformation.evidencias [i] [j] = ceramicImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}


		List<Indicador> ceramicWallIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.REVESTIMENTO_PAREDE.ToString (), name);

		if (ceramicWallIndicators != null && ceramicWallIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < ceramicWallsInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < ceramicWallsInformation.indicatorsList [i].Count; j++) {
					ceramicWallsInformation.indicatorsList [i] [j] = ceramicWallIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> ceramicWallImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.REVESTIMENTO_PAREDE.ToString (), name);
		
		if (ceramicWallImageIndicators != null && ceramicWallImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < ceramicWallsInformation.evidencias.Count; i++) {
				for (int j = 0; j < ceramicWallsInformation.evidencias [i].Count; j++) {
					ceramicWallsInformation.evidencias [i] [j] = ceramicWallImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> eletricsIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.ELETRICA.ToString (), name);
		
		if (eletricsIndicators != null && eletricsIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < eletricsInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < eletricsInformation.indicatorsList [i].Count; j++) {
					eletricsInformation.indicatorsList [i] [j] = eletricsIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> eletricsImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.ELETRICA.ToString (), name);
		
		if (eletricsImageIndicators != null && eletricsImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < eletricsInformation.evidencias.Count; i++) {
				for (int j = 0; j < eletricsInformation.evidencias [i].Count; j++) {
					eletricsInformation.evidencias [i] [j] = eletricsImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> framesIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.ESQUADRIAS.ToString (), name);
		
		if (framesIndicators != null && framesIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < framesInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < framesInformation.indicatorsList [i].Count; j++) {
					framesInformation.indicatorsList [i] [j] = framesIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> framesImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.ESQUADRIAS.ToString (), name);
		
		if (framesImageIndicators != null && framesImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < framesInformation.evidencias.Count; i++) {
				for (int j = 0; j < framesInformation.evidencias [i].Count; j++) {
					framesInformation.evidencias [i] [j] = framesImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> hidraulicsIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.HIDRAULICA.ToString (), name);
		
		if (hidraulicsIndicators != null && hidraulicsIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < hidraulicsInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < hidraulicsInformation.indicatorsList [i].Count; j++) {
					hidraulicsInformation.indicatorsList [i] [j] = hidraulicsIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> hidraulicsImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.HIDRAULICA.ToString (), name);
		
		if (hidraulicsImageIndicators != null && hidraulicsImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < hidraulicsInformation.evidencias.Count; i++) {
				for (int j = 0; j < hidraulicsInformation.evidencias [i].Count; j++) {
					hidraulicsInformation.evidencias [i] [j] = hidraulicsImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> paintingCeilingIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.PINTURA_TETO.ToString (), name);
		
		if (paintingCeilingIndicators != null && paintingCeilingIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < paintingCeilingInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < paintingCeilingInformation.indicatorsList [i].Count; j++) {
					paintingCeilingInformation.indicatorsList [i] [j] = paintingCeilingIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> paintingImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.PINTURA_TETO.ToString (), name);
		
		if (paintingImageIndicators != null && paintingImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < paintingCeilingInformation.evidencias.Count; i++) {
				for (int j = 0; j < paintingCeilingInformation.evidencias [i].Count; j++) {
					paintingCeilingInformation.evidencias [i] [j] = paintingImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> paintingWallIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.PINTURA_PAREDE.ToString (), name);
		
		if (paintingWallIndicators != null && paintingWallIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < paintingWallInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < paintingWallInformation.indicatorsList [i].Count; j++) {
					paintingWallInformation.indicatorsList [i] [j] = paintingWallIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> paintingWallImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.PINTURA_PAREDE.ToString (), name);
		
		if (paintingWallImageIndicators != null && paintingWallImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < paintingWallInformation.evidencias.Count; i++) {
				for (int j = 0; j < paintingWallInformation.evidencias [i].Count; j++) {
					paintingWallInformation.evidencias [i] [j] = paintingWallImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> sillsIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.SOLEIRAS.ToString (), name);
		
		if (sillsIndicators != null && sillsIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < sillsInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < sillsInformation.indicatorsList [i].Count; j++) {
					sillsInformation.indicatorsList [i] [j] = sillsIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> sillsImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.SOLEIRAS.ToString (), name);
		
		if (sillsImageIndicators != null && sillsImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < sillsInformation.evidencias.Count; i++) {
				for (int j = 0; j < sillsInformation.evidencias [i].Count; j++) {
					sillsInformation.evidencias [i] [j] = sillsImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> peitorisIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.PEITORIS.ToString (), name);
		
		if (peitorisIndicators != null && peitorisIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < peitorisInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < peitorisInformation.indicatorsList [i].Count; j++) {
					peitorisInformation.indicatorsList [i] [j] = peitorisIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> peitorisImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.PEITORIS.ToString (), name);
		
		if (peitorisImageIndicators != null && peitorisImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < peitorisInformation.evidencias.Count; i++) {
				for (int j = 0; j < peitorisInformation.evidencias [i].Count; j++) {
					peitorisInformation.evidencias [i] [j] = peitorisImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> tentosBaguetesIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.TENTOS_BAGUETES.ToString (), name);
		
		if (tentosBaguetesIndicators != null && tentosBaguetesIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < tentosBaguetesInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < tentosBaguetesInformation.indicatorsList [i].Count; j++) {
					tentosBaguetesInformation.indicatorsList [i] [j] = tentosBaguetesIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> tentosBaguetesImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.TENTOS_BAGUETES.ToString (), name);
		
		if (tentosBaguetesImageIndicators != null && tentosBaguetesImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < tentosBaguetesInformation.evidencias.Count; i++) {
				for (int j = 0; j < tentosBaguetesInformation.evidencias [i].Count; j++) {
					tentosBaguetesInformation.evidencias [i] [j] = tentosBaguetesImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> woodenFloorIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.PISO_MADEIRA.ToString (), name);
		
		if (woodenFloorIndicators != null && woodenFloorIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < woodenFloorInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < woodenFloorInformation.indicatorsList [i].Count; j++) {
					woodenFloorInformation.indicatorsList [i] [j] = woodenFloorIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> woodenFloorImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.PISO_MADEIRA.ToString (), name);
		
		if (woodenFloorImageIndicators != null && woodenFloorImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < woodenFloorInformation.evidencias.Count; i++) {
				for (int j = 0; j < woodenFloorInformation.evidencias [i].Count; j++) {
					woodenFloorInformation.evidencias [i] [j] = woodenFloorImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> funcionalTetoIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.FUNCIONAL_TETO.ToString (), name);
		
		if (funcionalTetoIndicators != null && funcionalTetoIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < functionalCeilingInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < functionalCeilingInformation.indicatorsList [i].Count; j++) {
					functionalCeilingInformation.indicatorsList [i] [j] = funcionalTetoIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> funcionalTetoImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.FUNCIONAL_TETO.ToString (), name);
		
		if (funcionalTetoImageIndicators != null && funcionalTetoImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < functionalCeilingInformation.evidencias.Count; i++) {
				for (int j = 0; j < functionalCeilingInformation.evidencias [i].Count; j++) {
					functionalCeilingInformation.evidencias [i] [j] = funcionalTetoImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<Indicador> funcionalWallIndicators = CheckSpaceManager.instance.FilterIndicator (Servicos.FUNCIONAL_PAREDE.ToString (), name);
		
		if (funcionalWallIndicators != null && funcionalWallIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < functionalWallInformation.indicatorsList.Count; i++) {
				for (int j = 0; j < functionalWallInformation.indicatorsList [i].Count; j++) {
					functionalWallInformation.indicatorsList [i] [j] = funcionalWallIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		List<ImagemIndicador> funcionalWallImageIndicators = CheckSpaceManager.instance.FilterImageIndicator (Servicos.FUNCIONAL_PAREDE.ToString (), name);
		
		if (funcionalWallImageIndicators != null && funcionalWallImageIndicators.Count > 0) {
			int count = 0;
			for (int i = 0; i < functionalWallInformation.evidencias.Count; i++) {
				for (int j = 0; j < functionalWallInformation.evidencias [i].Count; j++) {
					functionalWallInformation.evidencias [i] [j] = funcionalWallImageIndicators [count];
					count++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}
	}

	//Offline filters




}
