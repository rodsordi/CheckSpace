﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AspectList
{
	public string group;
	public string header;
	public List<string> aspectNames;
}
