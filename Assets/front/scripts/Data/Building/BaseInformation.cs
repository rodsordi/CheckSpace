﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseInformation  {
	public bool verified = false;
	public string ambient;
	public Vistoria vistoria;
	public List<List<Indicador>> indicatorsList;
	public string[] prefabNames;
	public List<List<ImagemIndicador>> evidencias;
	public int aspectRowCount = 0;
	public string comment = "";
	public Servico service;
	public string[] serviceTags;

	public BaseInformation()
	{
		vistoria = StaticData.vistoriaAtual;
		service = new Servico();
	}

	public virtual void ShowPhotosWindow(Indicador indic, ImagemIndicador imgIndicador)
	{
		CheckSpaceManager.instance.photosWindow.FillComments(serviceTags);
		CheckSpaceManager.instance.photosWindow.Open();
	}

}
