﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HidraulicsInformation:BaseInformation
{
	public HidraulicsInformation (string ambientName)
	{
		ambient = ambientName;

		serviceTags = new string[] {"Torneira vazando", "Não abre", "Não funciona", "Pouco fluxo de água", "Duchas vazando",
			"Registro não abre", "Registro não fecha", "Registro longe da parede", "Sem acabamento", "Pressao baixa",
			"Cifão vazando", "Flexível vazando", "Quebrada", "Suja", "Mancha de oxidação", "Sem caimento", 
			"Sem escoamento", "Entopido", "Riscadas", "Manchadas",
			"Trincadas", "Não fixadas", "Mal fiadas","Com desnível", "Baixa",
			"Alta", "Vazando", "Baixo fluxo", "Riscada", "Amassada",
			"Sem tampa", "Solta", "Suja", "Manchada", "Trincada", "Vazando", "Apresenta vazamento no apartamento inferior",
			"Sujo", "Desalinhado", "Com folga", "Quebrado", "Riscado", "Manchado", "Com mancha de oxidação",
			"Suja", "Desnivelada", "Alta", "Baixa", "Falha no rejunte", "Mal fixada", "Riscada", "Trincada", "Manchada", "Desalinhada", "Não fixada"};


		service.nome = Servicos.HIDRAULICA.ToString();
		prefabNames = new string[] {"HidraulicsRowI", "HidraulicsRowII", "HidraulicsRowIII"};
		InitializeIndicators();
	}

	public void InitializeIndicators()
	{
		indicatorsList = new List<List<Indicador>> ();
		evidencias = new List<List<ImagemIndicador>>();

		for (int i = 0; i < prefabNames.Length; i++) {
			
			GameObject baseGO = Resources.Load <GameObject> (prefabNames [i]);
			AspectRow aspectRow = baseGO.GetComponent <AspectRow> (); 
			
			indicatorsList.Add (new List<Indicador> ());
			evidencias.Add (new List<ImagemIndicador> ());

			int total = 0;
			
			for (int j = 0; j < aspectRow.aspectsList.Count; j++) {
				for (int k = 0; k < aspectRow.aspectsList [j].aspectNames.Count; k++) {
					
					indicatorsList [i].Add (new Indicador ());	
					indicatorsList [i] [total].indicador = 3;
					indicatorsList [i] [total].nome = aspectRow.aspectsList [j].aspectNames [k];
					indicatorsList [i] [total].grupo = aspectRow.aspectsList [j].group.Split(' ')[0];
					indicatorsList [i] [total].subGrupo = aspectRow.aspectsList [j].header.Split('-')[0];

					ImagemIndicador imgIndicator = new ImagemIndicador();
					imgIndicator.indicador = indicatorsList[i][total];
					evidencias [i].Add (new ImagemIndicador ());	
					evidencias [i] [total] = imgIndicator;
					total++;
				}
			}
		}
	}

	public override void ShowPhotosWindow(Indicador indic, ImagemIndicador imgIndicador)
	{
		CheckSpaceManager.instance.photosWindow.FillComments(serviceTags);
		CheckSpaceManager.instance.photosWindow.Open();
	}
}
