﻿using UnityEngine;
using System.Collections;

public class TimeField : MonoBehaviour
{
	public UILabel timeLabel;

	float width;
	float height;
	Rect drawRect;

	public EventDelegate fieldChange;

	Rect toScreenRect (Rect rect)
	{
		Vector2 lt = new Vector2 (rect.x, rect.y);
		Vector2 br = lt + new Vector2 (rect.width, rect.height);

		lt = GUIUtility.GUIToScreenPoint (lt);
		br = GUIUtility.GUIToScreenPoint (br);

		return new Rect (lt.x, lt.y, br.x - lt.x, br.y - lt.y);
	}

	void Start ()
	{
		width = Screen.width / 2;
		height = Screen.width / 14;
		drawRect = new Rect ((Screen.width - width) / 2, height, width, height);

	}

	public void OnClick ()
	{
		NativePicker.Instance.ShowTimePicker (toScreenRect (drawRect), (long val) => {
			timeLabel.text = NativePicker.ConvertToDateTime (val).ToString ("HH:mm");
			fieldChange.Execute ();
		}, () => {
			timeLabel.text = "";
		});
	}
}
