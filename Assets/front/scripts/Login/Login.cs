﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Login : MonoBehaviour
{

	public string username;
	public string senha;
	public UILabel lblUser;
	public UILabel lblSenha;

	public UILabel lblregisterUser;
	public UILabel lblregisterName;
	public UILabel lblregisterPass;

	public UITable loginTable;
	public UITable registerTable;
	public UITable vistoriasTable;

	public UITable loginButtonsTable;
	public UITable registerButtonsTable;
	public UITable vistoriasButtonsTable;

	public UIButton deleteButton;
	public UIButton newButton;

	Vistoriador vistoriador;
	List<Vistoria> vistorias;

	public GameObject vistoriasPrefab;

	public Vistoria selectedVistoria;

	public UILabel titleLabel;
	bool isConnected = false;
	public bool offlineMode = false;

	public GameObject carregando;
	ServerService server;
	bool isAtLoginScreen = false;

	void Start ()
	{
		if(StaticData.IsRelog)
		{
			if(ServerHandler.server != null)
				server = ServerHandler.server ;
			else
			{
				ServerHandler.server = new ServerService ();
				server = ServerHandler.server ;
			}

			Relog();
		}
		else
		{
			ShowLoginScreen ();	
			isConnected = ConnectionManager.IsConnected (false);

			if (isConnected && !offlineMode) {
				if(ServerHandler.server == null)
					ServerHandler.server = new ServerService ();

				server = ServerHandler.server ;
				Invoke("DownloadVistoriadores" , 0.4f);
			}
			else
			{
				List<Vistoriador> list = ServerHandler.vistoriadorDAO.LoadAllIncludingDeleted();
				if(list.Count == 0)
				{
					Vistoriador vistoriadorPadrao = new Vistoriador();
					vistoriadorPadrao.nome = "Master";
					vistoriadorPadrao.usuario = "admin";
					vistoriadorPadrao.senha = "1234";
					ServerHandler.vistoriadorDAO.SaveOrUpdate(vistoriadorPadrao);
				}
			}
		}
	}

	public void DownloadVistoriadores()
	{
		carregando.SetActive(true);
		string json = ServerHandler.server.findAllVistoriador ();

		if (json != null && json.StartsWith ("Exception")) {
			PopupsManager.Instance.ShowPopupMessage ("Erro do Servidor", "Não foi possível carregar usuários cadastrados.");
			Invoke("DownloadVistoriadores" , 1.5f);
			return;
		}

		List<Vistoriador> listaVistoriadores = JsonBehaviour.GetList<Vistoriador> (json);
		
		ServerHandler.vistoriadorDAO.SaveList (listaVistoriadores);

		carregando.SetActive(false);




	}
	
	public void Relog()
	{
		username = ServerHandler.user;
		senha = ServerHandler.pass;

		carregando.SetActive(true);
		string json = "";
		
		isConnected = ConnectionManager.IsConnected (false);

		if (isConnected && !offlineMode) {
			json = server.login (username + "_" + senha);
			
			if (json != null && json.StartsWith ("Exception")) {
				PopupsManager.Instance.ShowPopupMessage ("Erro do Servidor", "Não foi possível fazer login. Tente novamente mais tarde.");
				return;
			}
			
			if (!string.IsNullOrEmpty (json)) {
				vistoriador = new Vistoriador (json);
				json = server.findVistoriaByVistoriador (vistoriador.id);
				vistorias = JsonBehaviour.GetList<Vistoria> (json);
				
				vistoriador = ServerHandler.vistoriadorDAO.LoadById(vistoriador.id);
			} else {
				PopupsManager.Instance.ShowPopupMessage ("Erro no Login", "Usuário ou senha incorretos.");
			}
			
		} else {
			vistoriador = ServerHandler.vistoriadorDAO.login (username, senha);
		}
		
		if (vistoriador != null) {
			
			StaticData.vistoriadorAtual = vistoriador;
			List<Vistoria> ultimaVistoria = new VistoriaDAO ().LoadByVistoriador (vistoriador);
			
			if (ultimaVistoria != null && ultimaVistoria.Count > 0) {
				GameObject row = NGUITools.AddChild (vistoriasTable.gameObject, vistoriasPrefab);
				row.GetComponent <VistoriaRow> ().vistoria = ultimaVistoria[0];
				row.GetComponent <VistoriaRow> ().online = false;
				newButton.enabled = false;
				deleteButton.enabled = true;
				//sendButton.enabled = true;
			} else {
				newButton.enabled = true;
				deleteButton.enabled = false;
				//sendButton.enabled = false;
			}
			
			if (vistorias != null && vistorias.Count > 0) {
				
				foreach (var item in vistorias) {
					GameObject row = NGUITools.AddChild (vistoriasTable.gameObject, vistoriasPrefab);
					row.GetComponent <VistoriaRow> ().vistoria = item;
					row.GetComponent <VistoriaRow> ().online = true;
				}
				
				
			}
			
			ShowVistoriasScreen ();
			carregando.SetActive(false);
			StaticData.IsRelog = false;
		}
		else
		{
			carregando.SetActive(false);
			PopupsManager.Instance.ShowPopupMessage ("Erro no Login", "Usuário ou senha incorretos.");
		}

	}

	public void OnLoginClick ()
	{
		username = lblUser.text;
		senha = lblSenha.text;
		string json = "";

		isConnected = ConnectionManager.IsConnected (false) && !offlineMode;

		if (isConnected) {
			json = server.login (username + "_" + senha);

			if (json != null && json.StartsWith ("Exception")) {
				carregando.SetActive(false);
				PopupsManager.Instance.ShowPopupMessage ("Erro do Servidor", "Não foi possível fazer login. Tente novamente mais tarde.");
				return;
			}

			if (!string.IsNullOrEmpty (json)) {
				vistoriador = new Vistoriador (json);
				json = server.findVistoriaByVistoriador (vistoriador.id);
				vistorias = JsonBehaviour.GetList<Vistoria> (json);
				vistoriador = ServerHandler.vistoriadorDAO.LoadById(vistoriador.id);
				carregando.SetActive(true);
			} else {
				carregando.SetActive(false);
				PopupsManager.Instance.ShowPopupMessage ("Erro no Login", "Usuário ou senha incorretos.");
				return;
			}

		} else {
			vistoriador = ServerHandler.vistoriadorDAO.login (username, senha);
		}

		if (vistoriador != null) {
			carregando.SetActive(true);

			ServerHandler.user = username;
			ServerHandler.pass = senha;

			StaticData.vistoriadorAtual = vistoriador;

			List<Vistoria> ultimaVistoria = new VistoriaDAO ().LoadByVistoriador (vistoriador);
			for(int i = 0; i < vistoriasTable.transform.childCount; i++)
			{
				Destroy (vistoriasTable.transform.GetChild(i).gameObject);
			}

			if (ultimaVistoria != null && ultimaVistoria.Count > 0) {
				GameObject row = NGUITools.AddChild (vistoriasTable.gameObject, vistoriasPrefab);
				row.GetComponent <VistoriaRow> ().vistoria = ultimaVistoria[0];
				row.GetComponent <VistoriaRow> ().online = false;
				newButton.enabled = false;
				deleteButton.enabled = true;
//				sendButton.enabled = true;
			} else {
				newButton.enabled = true;
				deleteButton.enabled = false;
				//sendButton.enabled = false;
			}
		

			if (vistorias != null && vistorias.Count > 0) {
				foreach (var item in vistorias) {
					GameObject row = NGUITools.AddChild (vistoriasTable.gameObject, vistoriasPrefab);
					row.GetComponent <VistoriaRow> ().vistoria = item;
					row.GetComponent <VistoriaRow> ().online = true;
				}


			}

			ShowVistoriasScreen ();
			carregando.SetActive(false);
		}
		else
		{
			PopupsManager.Instance.ShowPopupMessage ("Erro no Login", "Usuário ou senha incorretos.");
		}

	}

	public void OnQuitClick()
	{
		if(isAtLoginScreen)
			Application.Quit();
		else
			ShowLoginScreen();
	}

	public void OnRegisterClick ()
	{
		string user = lblregisterUser.text;
		string pass = lblregisterPass.text;
		string person = lblregisterName.text;
		string json = null;
		string id = null;

		//Cadastrar Vistoriador

		Vistoriador novoVistoriador = new Vistoriador ();
		novoVistoriador.id = 0;
		novoVistoriador.nome = person;
		novoVistoriador.usuario = user;
		novoVistoriador.senha = pass;

		json = novoVistoriador.GetJson ();

		if (isConnected) {
			id = server.saveOrUpdateVistoriador (json);
			novoVistoriador.id = int.Parse (id);
		}

		ServerHandler.vistoriadorDAO.SaveOrUpdate (novoVistoriador);
		print (novoVistoriador);
		ShowLoginScreen ();
	}

	public void OnNewClick ()
	{
		Vistoria vistoria = new Vistoria ();

		vistoria.id = 0;
		vistoria.proprietario = "";
		vistoria.empreendimento = "";
		vistoria.valor = 0d;
		vistoria.endereco = "";
		vistoria.unidade = "";
		vistoria.bloco = "";
		vistoria.dataCompra = DateTime.Now;
		vistoria.metragem = 0.0f;
		vistoria.padrao = "";
		vistoria.dataVistoria = DateTime.Now;
		vistoria.horarioAgendado = DateTime.Now;
		vistoria.inicioVistoria = DateTime.Now;
		vistoria.incorporadora = "";
		vistoria.terminoVistoria = DateTime.Now;
		vistoria.construtora = "";
		vistoria.clima = "";
		vistoria.imagemPredioUrl = null;
		vistoria.vistoriador = vistoriador;
		vistoria.imagem = null;

		VistoriaDAO vistoriaDAO = new VistoriaDAO ();
		StaticData.vistoriaAtual = vistoria;
		StaticData.OnlineVistoria = false;
		Application.LoadLevel ("RelatorioVistoria");
	}

	public void OnSendClick ()
	{		
		string json = StaticData.vistoriaAtual.GetJson ();

		if (isConnected) {
			string success = server.saveOrUpdateVistoria (json);
			if (!success.StartsWith ("Exception"))
				DeleteVistorias ();
			else {
				PopupsManager.Instance.ShowPopupMessage ("Erro", "Não foi possível salvar o registro no banco");
			}
		} else {
			PopupsManager.Instance.ShowPopupMessage ("Erro", "Sem conexão com a Internet");
		}

	}

	public void OnDeleteClick ()
	{		
		DeleteVistorias ();
	}

	public void DeleteVistorias ()
	{

		List<Vistoria> vists = ServerHandler.vistoriaDAO.LoadAllIncludingDeleted();

		if(vists != null && vists.Count > 0)
		{
		Vistoria vistoriaToDelete = vists[0];

			foreach (var item in vistoriasTable.GetChildList ()) {
			if (item.GetComponent<VistoriaRow> ().vistoria.id == vistoriaToDelete.id) {
				ServerHandler.imagemIndicadorDAO.DeleteFile();
				ServerHandler.indicadorDAO.DeleteFile();
				ServerHandler.servicoDAO.DeleteFile();
				ServerHandler.ambienteDAO.DeleteFile();
				ServerHandler.vistoriaDAO.DeleteFile ();

				Destroy (item.gameObject);
				ShowVistoriaButtons ();
				newButton.enabled = true;
				deleteButton.enabled = false;
				//sendButton.enabled = false;
			}
		}
			vistoriasTable.Reposition ();
		}
	
	}

	public void ShowRegisterScreen ()
	{
		loginTable.gameObject.SetActive (false);
		registerTable.gameObject.SetActive (true);
		vistoriasTable.gameObject.SetActive (false);
		ShowRegisterButtons ();
		SetScreenTitle ("Cadastrar usuário");
		isAtLoginScreen = false;
		registerTable.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void ShowLoginScreen ()
	{
		loginTable.gameObject.SetActive (true);
		registerTable.gameObject.SetActive (false);
		vistoriasTable.gameObject.SetActive (false);
		ShowLoginButtons ();
		SetScreenTitle ("Login");
		isAtLoginScreen = true;

		loginTable.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();
	}

	public void ShowVistoriasScreen ()
	{
		loginTable.gameObject.SetActive (false);
		registerTable.gameObject.SetActive (false);
		vistoriasTable.gameObject.SetActive (true);
		ShowVistoriaButtons ();
		SetScreenTitle ("Lista de Vistorias");
		isAtLoginScreen = false;
		vistoriasTable.Reposition ();
		VerticalScroller.Instance.ResetScrollPosition ();

	}


	public void ShowRegisterButtons ()
	{
		loginButtonsTable.gameObject.SetActive (false);
		registerButtonsTable.gameObject.SetActive (true);
		vistoriasButtonsTable.gameObject.SetActive (false);
	}

	public void ShowLoginButtons ()
	{
		loginButtonsTable.gameObject.SetActive (true);
		registerButtonsTable.gameObject.SetActive (false);
		vistoriasButtonsTable.gameObject.SetActive (false);
	}

	public void ShowVistoriaButtons ()
	{
		loginButtonsTable.gameObject.SetActive (false);
		registerButtonsTable.gameObject.SetActive (false);
		vistoriasButtonsTable.gameObject.SetActive (true);
	}

	public void SetScreenTitle (string title)
	{
		titleLabel.text = title;
	}



}
