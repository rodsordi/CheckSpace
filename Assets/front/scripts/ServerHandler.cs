﻿using UnityEngine;
using System.Collections;

public static class ServerHandler 
{
	public static ServerService server;
	public static VistoriadorDAO vistoriadorDAO = new VistoriadorDAO ();
	public static VistoriaDAO vistoriaDAO = new VistoriaDAO ();
	public static ServicoDAO servicoDAO = new ServicoDAO();
	public static AmbienteDAO ambienteDAO = new AmbienteDAO();
	public static IndicadorDAO indicadorDAO = new IndicadorDAO();
	public static ImagemIndicadorDAO imagemIndicadorDAO = new ImagemIndicadorDAO();
	public static string user = "";
	public static string pass = "";
}
