﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/*using VistoriadorWS;
using VistoriaWS;
using DimensaoWS;
using IndicadorWS;
using TermoAcusticoWS;
using ImagemIndicadorWS;*/

public class WebServiceTests : MonoBehaviour {

	void Start () {
		testAmbienteJason ();
	}

	public void testAmbienteJason () {
		Ambiente ambiente = new Ambiente ();
		ambiente.id = 0;
		ambiente.nome = "nome";
		ambiente.peDireito = 2f;
		ambiente.area = 2f;
		ambiente.acusticoAberto = 2f;
		ambiente.acusticoFechado = 2f;
		ambiente.termoAberto = 2f;
		ambiente.termoFechado = 2f;
		ambiente.acusticoAbertoComRes = "test";
		ambiente.acusticoFechadoComRes = "test";
		ambiente.termoAbertoComRes = "test";
		ambiente.termoFechadoComRes = "test";
		ambiente.vistoria = new Vistoria () {id = 3};
		print (ambiente.GetJson ());
	}

	public void testFindIndicador () {
		ServerService server = new ServerService ();
		string key = "3_Sala_CERAMICA";
		string json = server.findIndicadorByVistoriaAndAmbienteAndServico (key);
		print (json);
	}
	
	/*VistoriadorWSService vistoriadorWS = new VistoriadorWSService ();
	VistoriaWSService vistoriaWS = new VistoriaWSService ();
	IndicadorWSService indicadorWS = new IndicadorWSService ();
	ImagemIndicadorWSService imagemIndicadorWS = new ImagemIndicadorWSService ();
	DimensaoWSService dimensaoWS = new DimensaoWSService ();
	TermoAcusticoWSService termoAcusticoWS = new TermoAcusticoWSService ();*/

	/*public ServerService server = new ServerService ();
	
	VistoriadorDAO vistoriadorDAO = new VistoriadorDAO ();
	VistoriaDAO vistoriaDAO = new VistoriaDAO ();
	IndicadorDAO indicadorDAO = new IndicadorDAO ();
	ImagemIndicadorDAO imagemIndicadorDAO = new ImagemIndicadorDAO ();
	DimensaoDAO dimensaoDAO = new DimensaoDAO ();
	TermoAcusticoDAO termoAcusticoDAO = new TermoAcusticoDAO ();
	
	
	// Use this for initialization
	void Start () {
		testGroupBy ();
		//testJson ();
		//testSave ();
		//testLogin ();
		//testSync ();
	}

	public void testGroupBy () {

		/*Vistoria v = new VistoriaDAO ().LoadLast ();
		List<string> result = new DimensaoDAO ().getNomeAmbientesByVistoria (v);
		print ("getNomeAmbientesByVistoria " + v.OID);
		foreach (string s in result)
			print (s);

		//new DimensaoDAO ().SaveOrUpdate (new Dimensao (0, 1f, 1f, "Cozinha", "", v));

		List<Dimensao> dList = new DimensaoDAO ().LoadAll ();
		print ("LoadAll");
		foreach (Dimensao d in dList)
			print (d.OID + " " + d.ambiente);*/
	/*}

	private void testJson () {
		/*string result = indicadorWS.getNomeAmbientesByVistoria (11);
		print (result);*/

	/*}
	
	private void testSync () {
		Vistoriador vistoriador = new Vistoriador ();
		vistoriador.id = 1;
		vistoriador.nome = "Rorigo";
		vistoriador.usuario = "rodsordi";
		vistoriador.senha = "1234";
		
		new Synchronizer (true).Synchronize (vistoriador, SyncCompleted);
	}
	
	private void SyncCompleted (SynchronizerCompletedEventArgs args) {
		if (args.error == null) {
			print (args.result);
		} else {
			print (args.error);
		}
	}
	
	private void testLogin () {
		String login = "rodsordi_1234";
		string json = server.login (login);
		print (json);
		Vistoriador vistoriador = new Vistoriador (json);
		print (vistoriador);
		
		
	}
	
	private void testSave () {
		string json = null;
		string id = null;
		
		//Vistoriador
		
		Vistoriador vistoriador = new Vistoriador ();
		vistoriador.id = 0;
		vistoriador.nome = "Rorigo";
		vistoriador.usuario = "rodsordi";
		vistoriador.senha = "1234";
		
		json = vistoriador.GetJson ();
		id = vistoriadorWS.save (json);
		vistoriador.id = int.Parse (id);
		vistoriadorDAO.SaveOrUpdate (vistoriador);
		print (vistoriador);
		
		//Vistoria
		
		Vistoria vistoria = new Vistoria ();
		vistoria.id = 0;
		vistoria.proprietario = "proprietario";
		vistoria.empreendimento = "empreendimento";
		vistoria.valor = 1000.50d;
		vistoria.endereco = "endereco";
		vistoria.unidade = "unidade";
		vistoria.bloco = "bloco";
		vistoria.dataCompra = DateTime.Now;
		vistoria.metragem = 1.5f;
		vistoria.padrao = "padrao";
		vistoria.dataVistoria = DateTime.Now;
		vistoria.horarioAgendado = DateTime.Now;
		vistoria.inicioVistoria = DateTime.Now;
		vistoria.incorporadora = "incorporadora";
		vistoria.terminoVistoria = DateTime.Now;
		vistoria.construtora = "construtora";
		vistoria.clima = "clima";
		vistoria.imagemPredioUrl = null;
		vistoria.vistoriador = vistoriador;
		vistoria.imagem = null;
		
		json = vistoria.GetJson ();
		id = vistoriaWS.save (json);
		vistoria.id = int.Parse (id);
		vistoriaDAO.SaveOrUpdate (vistoria);
		print (vistoria);
		
		//Equipamento
		
		/*Equipamento equipamento = new Equipamento ();
		equipamento.id = 0;
		equipamento.name = "name";
		equipamento.url = null;
		equipamento.vistoria = vistoria;
		equipamento.imagem = null;
		
		json = equipamento.GetJson ();
		id = equipamentoWS.save (json);
		equipamento.id = int.Parse (id);
		equipamentoDAO.SaveOrUpdate (equipamento);
		print (equipamento);*/
		
		//Indicador
		
		/*Indicador indicador = new Indicador ();
		indicador.id = 0;
		indicador.nome = "nome";
		indicador.ambiente = Ambiente.Sala.ToString ();
		indicador.servico = Servico.CERAMICA.ToString ();
		indicador.grupo = Grupo.I.ToString ();
		indicador.subGrupo = SubGrupo.A.ToString ();
		indicador.indicador = 1;
		indicador.vistoria = vistoria;
		
		json = indicador.GetJson ();
		id = indicadorWS.save (json);
		indicador.id = int.Parse (id);
		indicadorDAO.SaveOrUpdate (indicador);
		print (indicador);
		
		//ImagemIndicador
		
		ImagemIndicador imagemIndicador = new ImagemIndicador ();
		imagemIndicador.id = 0;
		imagemIndicador.imagemUrl = null;
		imagemIndicador.comentario = "comentario";
		imagemIndicador.indicador = indicador;
		imagemIndicador.imagem = null;
		
		json = imagemIndicador.GetJson ();
		id = imagemIndicadorWS.save (json);
		imagemIndicador.id = int.Parse (id);
		imagemIndicadorDAO.SaveOrUpdate (imagemIndicador);
		print (imagemIndicador);
		
		//Dimensao
		
		Dimensao dimensao = new Dimensao ();
		dimensao.id = 0;
		dimensao.peDireito = 1.5d;
		dimensao.area = 1.5d;
		dimensao.ambiente = Ambiente.Sala.ToString ();
		dimensao.comentario = "comentario";
		dimensao.vistoria = vistoria;
		
		json = dimensao.GetJson ();
		id = dimensaoWS.save (json);
		dimensao.id = int.Parse (id);
		dimensaoDAO.SaveOrUpdate (dimensao);
		print (dimensao);
		
		//ImagemDimensao
		
		/*ImagemDimensao imagemDimensao = new ImagemDimensao ();
		imagemDimensao.id = 0;
		imagemDimensao.imagemUrl = null;
		imagemDimensao.comentario = "comentario";
		imagemDimensao.dimensao = dimensao;
		imagemDimensao.imagem = null;
		
		json = imagemDimensao.GetJson ();
		id = imagemDimensaoWS.save (json);
		imagemDimensao.id = int.Parse (id);
		imagemDimensaoDAO.SaveOrUpdate (imagemDimensao);
		print (imagemDimensao);*/
		
		//TermoAcustico
		
		/*TermoAcustico termoAcustico = new TermoAcustico ();
		termoAcustico.id = 0;
		termoAcustico.acusticoAberto = 1.5d;
		termoAcustico.acusticoFechado = 1.5d;
		termoAcustico.termoAberto = 1.5d;
		termoAcustico.termoFechado = 1.5d;
		termoAcustico.ambiente = Ambiente.Sala.ToString ();
		termoAcustico.vistoria = vistoria;
		
		json = termoAcustico.GetJson ();
		id = termoAcusticoWS.save (json);
		termoAcustico.id = int.Parse (id);
		termoAcusticoDAO.SaveOrUpdate (termoAcustico);
		print (termoAcustico);
		
		//ImagemTermoAcustico
		
		/*ImagemTermoAcustico imagemTermoAcustico = new ImagemTermoAcustico ();
		imagemTermoAcustico.id = 0;
		imagemTermoAcustico.imagemUrl = null;
		imagemTermoAcustico.comentario = "comentario";
		imagemTermoAcustico.termoAcustico = termoAcustico;
		imagemTermoAcustico.imagem = null;
		
		json = imagemTermoAcustico.GetJson ();
		id = imagemTermoAcusticoWS.save (json);
		imagemTermoAcustico.id = int.Parse (id);
		imagemTermoAcusticoDAO.SaveOrUpdate (imagemTermoAcustico);
		print (imagemTermoAcustico);*/
	//}
}
